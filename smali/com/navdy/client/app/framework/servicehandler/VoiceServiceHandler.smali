.class public Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
.super Ljava/lang/Object;
.source "VoiceServiceHandler.java"


# static fields
.field private static final GOOGLE_APP_PACKAGE:Ljava/lang/String; = "com.google.android.googlequicksearchbox"

.field private static final VOICE_SEARCH_MAX_RESULTS:I = 0xa

.field private static instance:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private alreadyRetried:Z

.field private handler:Landroid/os/Handler;

.field private hfpSoundId:I

.field private hfpSoundPool:Landroid/media/SoundPool;

.field private isReadyForSpeech:Z

.field private listeningOverBt:Z

.field mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recognitionListener:Landroid/speech/RecognitionListener;

.field private final recognizerIntent:Landroid/content/Intent;

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private sr:Landroid/speech/SpeechRecognizer;

.field private userCanceledVoiceSearch:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 141
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 146
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->instance:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const/16 v7, 0x7d0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 153
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundId:I

    .line 155
    iput-boolean v5, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->isReadyForSpeech:Z

    .line 156
    iput-boolean v5, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->alreadyRetried:Z

    .line 158
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->userCanceledVoiceSearch:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 327
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognitionListener:Landroid/speech/RecognitionListener;

    .line 179
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handler:Landroid/os/Handler;

    .line 180
    new-instance v1, Landroid/media/SoundPool;

    invoke-direct {v1, v6, v5, v5}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundPool:Landroid/media/SoundPool;

    .line 182
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundPool:Landroid/media/SoundPool;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070010

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundId:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :goto_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 190
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    .line 191
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    const-string v2, "android.speech.extra.LANGUAGE_MODEL"

    const-string v3, "free_form"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    const-string v2, "calling_package"

    .line 194
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    .line 193
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    const-string v2, "android.speech.extra.PARTIAL_RESULTS"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 196
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    const-string v2, "android.speech.extra.PROMPT"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 197
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    const-string v2, "android.speech.extra.MAX_RESULTS"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 198
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    const-string v2, "android.speech.extras.SPEECH_INPUT_MINIMUM_LENGTH_MILLIS"

    const/16 v3, 0x1b58

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 199
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    const-string v2, "android.speech.extras.SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 200
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    const-string v2, "android.speech.extras.SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 203
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "re":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "RuntimeException loading the sound "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->listeningOverBt:Z

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->listeningOverBt:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundId:I

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->onStopListening()V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->onVoiceSearchComplete()V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # I

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleError(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleVoiceCommand(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->logAnonymousAnalytics(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sayNoResultFound(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendDestinationsToHud(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendToHud(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundPool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/speech/SpeechRecognizer;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sr:Landroid/speech/SpeechRecognizer;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Landroid/speech/SpeechRecognizer;)Landroid/speech/SpeechRecognizer;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Landroid/speech/SpeechRecognizer;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sr:Landroid/speech/SpeechRecognizer;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/speech/RecognitionListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognitionListener:Landroid/speech/RecognitionListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->recognizerIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->isReadyForSpeech:Z

    return v0
.end method

.method static synthetic access$702(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->isReadyForSpeech:Z

    return p1
.end method

.method static synthetic access$800(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->userCanceledVoiceSearch:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->alreadyRetried:Z

    return v0
.end method

.method static synthetic access$902(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->alreadyRetried:Z

    return p1
.end method

.method public static declared-synchronized getInstance()Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    .locals 2

    .prologue
    .line 169
    const-class v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->instance:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->instance:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .line 172
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->instance:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleError(I)V
    .locals 6
    .param p1, "errorPrompt"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 720
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 721
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 722
    .local v1, "errorString":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v1, v4, v5}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 723
    new-instance v3, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 724
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_WORDS_RECOGNIZED:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 725
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->listeningOverBt:Z

    .line 726
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v3

    .line 727
    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v2

    .line 728
    .local v2, "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    invoke-static {v2}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 729
    return-void
.end method

.method private handleGoogleNowRequest(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)V
    .locals 7
    .param p1, "audioRouter"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 228
    sget-object v5, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Handle Google now request"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 230
    :try_start_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->cancelTtsAndClearQueue()V

    .line 231
    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 232
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v5, 0x10000000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 233
    const-string v5, "com.google.android.googlequicksearchbox"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 235
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 236
    .local v2, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 237
    :cond_0
    sget-object v5, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Google now is not installed"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 238
    new-instance v5, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;

    sget-object v6, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-direct {v5, v6}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;-><init>(Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;)V

    invoke-static {v5}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 251
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :goto_0
    return-void

    .line 241
    .restart local v0    # "context":Landroid/content/Context;
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_1
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 242
    new-instance v5, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;

    sget-object v6, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_LISTENING:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-direct {v5, v6}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;-><init>(Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;)V

    invoke-static {v5}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 243
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :catch_0
    move-exception v3

    .line 244
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to start Google Now"

    invoke-virtual {v5, v6, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 246
    :try_start_1
    new-instance v5, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;

    sget-object v6, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-direct {v5, v6}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;-><init>(Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;)V

    invoke-static {v5}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 247
    :catch_1
    move-exception v4

    .line 248
    .local v4, "th":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to tell the HUD that we couldn\'t start Google Now"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private handleHomeAndWork(Ljava/lang/String;)Z
    .locals 6
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 737
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 740
    .local v0, "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->isHome(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 741
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getHome()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    .line 742
    .local v1, "homeDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v1, :cond_0

    .line 743
    invoke-direct {p0, v1, p1, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendDestinationToHud(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    .end local v1    # "homeDestination":Lcom/navdy/client/app/framework/models/Destination;
    :goto_0
    return v4

    .line 745
    .restart local v1    # "homeDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_0
    const v5, 0x7f080250

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 746
    .local v2, "title":Ljava/lang/String;
    invoke-direct {p0, p1, v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendNoResultsFoundResponse(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 751
    .end local v1    # "homeDestination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "title":Ljava/lang/String;
    :cond_1
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->isWork(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 752
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getWork()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v3

    .line 753
    .local v3, "workDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v3, :cond_2

    .line 754
    invoke-direct {p0, v3, p1, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendDestinationToHud(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 756
    :cond_2
    const v5, 0x7f080251

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 757
    .restart local v2    # "title":Ljava/lang/String;
    invoke-direct {p0, p1, v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendNoResultsFoundResponse(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 761
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "workDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private handleSpecialCommands(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Z
    .locals 12
    .param p1, "queryParam"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "audioRouter"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 774
    const v0, 0x7f070004

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->isOneOfTheseVoiceCommands(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 776
    .local v6, "context":Landroid/content/Context;
    const v0, 0x7f08024f

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 777
    .local v8, "iCanDoThis":Ljava/lang/String;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p2, v8, v0, v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 778
    const/4 v0, 0x1

    .line 856
    .end local v6    # "context":Landroid/content/Context;
    .end local v8    # "iCanDoThis":Ljava/lang/String;
    :goto_0
    return v0

    .line 781
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "use_experimental_hud_voice_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 783
    const/4 v0, 0x0

    goto :goto_0

    .line 788
    :cond_1
    const v0, 0x7f090010

    const v1, 0x7f080518

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleThisSimpleCommand(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;II)Z

    move-result v0

    if-nez v0, :cond_2

    const v0, 0x7f090020

    const v1, 0x7f08051a

    .line 789
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleThisSimpleCommand(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;II)Z

    move-result v0

    if-nez v0, :cond_2

    const v0, 0x7f090021

    const v1, 0x7f080516

    .line 790
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleThisSimpleCommand(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 791
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 794
    :cond_3
    const v3, 0x7f09000d

    const v4, 0x7f080519

    new-instance v5, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$4;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$4;-><init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleThisCommand(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;IILjava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 801
    const/4 v0, 0x1

    goto :goto_0

    .line 804
    :cond_4
    const/16 v0, 0x11

    new-array v11, v0, [Lcom/navdy/service/library/events/ui/Screen;

    const/4 v0, 0x0

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/4 v0, 0x1

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/4 v0, 0x2

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/4 v0, 0x3

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/4 v0, 0x4

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/4 v0, 0x5

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/4 v0, 0x6

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECOMMENDED_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/4 v0, 0x7

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_CONTACTS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0x8

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECENT_CALLS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0x9

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0xa

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MEDIA_BROWSER:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0xb

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OPTIONS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0xc

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SETTINGS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0xd

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0xe

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0xf

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    const/16 v0, 0x10

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SYSTEM_INFO:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v1, v11, v0

    .line 824
    .local v11, "screens":[Lcom/navdy/service/library/events/ui/Screen;
    const/16 v0, 0x11

    new-array v10, v0, [I

    fill-array-data v10, :array_0

    .line 844
    .local v10, "screenCommands":[I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v0, v10

    if-ge v7, v0, :cond_6

    array-length v0, v11

    if-ge v7, v0, :cond_6

    .line 845
    aget-object v9, v11, v7

    .line 846
    .local v9, "screen":Lcom/navdy/service/library/events/ui/Screen;
    aget v3, v10, v7

    const v4, 0x7f080515

    new-instance v5, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$5;

    invoke-direct {v5, p0, v9}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$5;-><init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Lcom/navdy/service/library/events/ui/Screen;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleThisCommand(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;IILjava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 853
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 844
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 856
    .end local v9    # "screen":Lcom/navdy/service/library/events/ui/Screen;
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 824
    :array_0
    .array-data 4
        0x7f090012
        0x7f090017
        0x7f090019
        0x7f09000f
        0x7f09000e
        0x7f090015
        0x7f09001c
        0x7f090014
        0x7f09001d
        0x7f09001a
        0x7f090018
        0x7f09001b
        0x7f09001e
        0x7f090011
        0x7f090013
        0x7f090016
        0x7f09001f
    .end array-data
.end method

.method private handleThisCommand(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;IILjava/lang/Runnable;)Z
    .locals 8
    .param p1, "queryParam"    # Ljava/lang/String;
    .param p2, "audioRouter"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .param p3, "array"    # I
        .annotation build Landroid/support/annotation/ArrayRes;
        .end annotation
    .end param
    .param p4, "answer"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p5, "runnable"    # Ljava/lang/Runnable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 864
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 865
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 866
    .local v3, "whatCanYouDo":[Ljava/lang/String;
    array-length v6, v3

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v2, v3, v5

    .line 867
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 868
    invoke-virtual {v0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 869
    .local v1, "noMatch":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {p2, v1, v5, v4}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 870
    if-eqz p5, :cond_0

    .line 871
    invoke-interface {p5}, Ljava/lang/Runnable;->run()V

    .line 873
    :cond_0
    const/4 v4, 0x1

    .line 876
    .end local v1    # "noMatch":Ljava/lang/String;
    .end local v2    # "s":Ljava/lang/String;
    :cond_1
    return v4

    .line 866
    .restart local v2    # "s":Ljava/lang/String;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private handleThisSimpleCommand(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;II)Z
    .locals 6
    .param p1, "queryParam"    # Ljava/lang/String;
    .param p2, "audioRouter"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .param p3, "array"    # I
        .annotation build Landroid/support/annotation/ArrayRes;
        .end annotation
    .end param
    .param p4, "answer"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 860
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleThisCommand(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;IILjava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method private handleVoiceCommand(Ljava/lang/String;)V
    .locals 10
    .param p1, "voiceCommand"    # Ljava/lang/String;

    .prologue
    const v9, 0x7f0802d9

    .line 619
    sget-object v6, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Handling the command: \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 621
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 622
    const v6, 0x7f080309

    invoke-direct {p0, v6}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleError(I)V

    .line 684
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 627
    .local v0, "context":Landroid/content/Context;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 628
    .local v1, "navdy":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 630
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 633
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 634
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 636
    const/4 v6, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 639
    :cond_3
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-direct {p0, p1, v6}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleSpecialCommands(Ljava/lang/String;Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 640
    new-instance v4, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    .line 641
    .local v4, "responseBuilder":Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    sget-object v6, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 642
    invoke-virtual {v4, p1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->recognizedWords(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 643
    invoke-virtual {v4, p1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->prefix(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 644
    invoke-virtual {v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v6

    invoke-static {v6}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0

    .line 650
    .end local v4    # "responseBuilder":Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    :cond_4
    const v6, 0x7f07000b

    invoke-static {v6, p1}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->getMatchingVoiceCommand(ILjava/lang/String;)Lcom/navdy/client/app/framework/util/VoiceCommand;

    move-result-object v5

    .line 651
    .local v5, "vc":Lcom/navdy/client/app/framework/util/VoiceCommand;
    if-eqz v5, :cond_5

    iget-object v6, v5, Lcom/navdy/client/app/framework/util/VoiceCommand;->query:Ljava/lang/String;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 652
    :cond_5
    const/4 v2, 0x0

    .line 653
    .local v2, "prefix":Ljava/lang/String;
    move-object v3, p1

    .line 658
    .local v3, "query":Ljava/lang/String;
    :goto_1
    sget-object v6, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cleaned up voice query is: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 660
    invoke-direct {p0, v3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleHomeAndWork(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 664
    new-instance v6, Lcom/navdy/client/app/framework/search/NavdySearch;

    new-instance v7, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;

    invoke-direct {v7, p0, v3, v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;-><init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v7}, Lcom/navdy/client/app/framework/search/NavdySearch;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;)V

    .line 683
    invoke-virtual {v6, v3}, Lcom/navdy/client/app/framework/search/NavdySearch;->runSearch(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 655
    .end local v2    # "prefix":Ljava/lang/String;
    .end local v3    # "query":Ljava/lang/String;
    :cond_6
    iget-object v2, v5, Lcom/navdy/client/app/framework/util/VoiceCommand;->prefix:Ljava/lang/String;

    .line 656
    .restart local v2    # "prefix":Ljava/lang/String;
    iget-object v3, v5, Lcom/navdy/client/app/framework/util/VoiceCommand;->query:Ljava/lang/String;

    .restart local v3    # "query":Ljava/lang/String;
    goto :goto_1
.end method

.method private handleVoiceSearchRequest()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 260
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->cancelTtsAndClearQueue()V

    .line 263
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "use_experimental_hud_voice_enabled"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 265
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "hud_voice_used_before"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 268
    .local v1, "hasUsedHudVoiceBefore":Z
    if-nez v1, :cond_0

    .line 269
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "hud_voice_used_before"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 270
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 271
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 272
    .local v0, "context":Landroid/content/Context;
    const v6, 0x7f0801f1

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 273
    .local v2, "helloThere":Ljava/lang/String;
    const v6, 0x7f08024f

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 274
    .local v3, "iCanDoThis":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 276
    new-instance v5, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    .line 277
    .local v5, "responseBuilder":Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    sget-object v6, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 278
    sget-object v6, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 279
    invoke-virtual {v5}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v6

    invoke-static {v6}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 280
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->onVoiceSearchComplete()V

    .line 322
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "hasUsedHudVoiceBefore":Z
    .end local v2    # "helloThere":Ljava/lang/String;
    .end local v3    # "iCanDoThis":Ljava/lang/String;
    .end local v5    # "responseBuilder":Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    :goto_0
    return-void

    .line 286
    :cond_0
    new-instance v6, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_STARTING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 287
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v6

    .line 288
    invoke-virtual {v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v4

    .line 289
    .local v4, "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    invoke-static {v4}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 290
    iput-boolean v9, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->alreadyRetried:Z

    .line 292
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    new-instance v7, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;

    invoke-direct {v7, p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setVoiceSearchSetupListener(Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;)V

    .line 319
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->startVoiceSearchSession()V

    .line 321
    const-string v6, "Navigate_Using_Hud_Voice_Search"

    invoke-static {v6}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private logAnonymousAnalytics(Ljava/lang/String;)V
    .locals 10
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 688
    const-string v0, "p"

    .line 694
    .local v0, "build":Ljava/lang/String;
    :try_start_0
    const-string v7, "UTF-8"

    invoke-static {p1, v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 699
    .local v2, "encodedQuery":Ljava/lang/String;
    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://s3-us-west-2.amazonaws.com/navdy-analytics/search/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/android/voice_search.txt?q="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 702
    .local v4, "url":Ljava/lang/String;
    const/4 v3, 0x0

    .line 706
    .local v3, "inputStream":Ljava/io/InputStream;
    :try_start_1
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 707
    .local v6, "urlObject":Ljava/net/URL;
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;

    .line 708
    .local v5, "urlConnection":Ljava/net/HttpURLConnection;
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 712
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 714
    .end local v5    # "urlConnection":Ljava/net/HttpURLConnection;
    .end local v6    # "urlObject":Ljava/net/URL;
    :goto_1
    return-void

    .line 695
    .end local v2    # "encodedQuery":Ljava/lang/String;
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .end local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 696
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 697
    move-object v2, p1

    .restart local v2    # "encodedQuery":Ljava/lang/String;
    goto :goto_0

    .line 709
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 710
    .local v1, "e":Ljava/lang/Throwable;
    :try_start_2
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "logAnonymousAnalytics: throwable e: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 712
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v7

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v7
.end method

.method private onStopListening()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 594
    iput-boolean v5, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->isReadyForSpeech:Z

    .line 595
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->listeningOverBt:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 596
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->hfpSoundId:I

    const/4 v4, 0x1

    move v3, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sr:Landroid/speech/SpeechRecognizer;

    if-eqz v0, :cond_1

    .line 601
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sr:Landroid/speech/SpeechRecognizer;

    invoke-virtual {v0}, Landroid/speech/SpeechRecognizer;->stopListening()V

    .line 602
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sr:Landroid/speech/SpeechRecognizer;

    invoke-virtual {v0}, Landroid/speech/SpeechRecognizer;->cancel()V

    .line 603
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sr:Landroid/speech/SpeechRecognizer;

    invoke-virtual {v0}, Landroid/speech/SpeechRecognizer;->destroy()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 607
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sr:Landroid/speech/SpeechRecognizer;

    .line 609
    :cond_1
    return-void

    .line 604
    :catch_0
    move-exception v7

    .line 605
    .local v7, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to close the speech recognizer."

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private onVoiceSearchComplete()V
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->stopVoiceSearchSession()V

    .line 613
    return-void
.end method

.method private sayNoResultFound(Ljava/lang/String;)V
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 887
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 889
    .local v0, "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 890
    const v2, 0x7f080304

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 895
    .local v1, "title":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, p1, v1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendNoResultsFoundResponse(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    return-void

    .line 892
    .end local v1    # "title":Ljava/lang/String;
    :cond_0
    const v2, 0x7f080305

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "title":Ljava/lang/String;
    goto :goto_0
.end method

.method private sendDestinationToHud(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "prefix"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 918
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 919
    .local v0, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz p1, :cond_0

    .line 920
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 922
    :cond_0
    invoke-direct {p0, v0, p2, p3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendDestinationsToHud(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    return-void
.end method

.method private sendDestinationsToHud(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "prefix"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 933
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 934
    .local v1, "size":I
    :goto_0
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Processing "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " destination"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-le v1, v6, :cond_2

    const-string v3, "s"

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " to be sent to the HUD: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 936
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 937
    :cond_0
    invoke-direct {p0, p2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sayNoResultFound(Ljava/lang/String;)V

    .line 967
    :goto_2
    return-void

    .end local v1    # "size":I
    :cond_1
    move v1, v2

    .line 933
    goto :goto_0

    .line 934
    .restart local v1    # "size":I
    :cond_2
    const-string v3, ""

    goto :goto_1

    .line 941
    :cond_3
    if-ne v1, v6, :cond_4

    .line 943
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 945
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$6;-><init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto :goto_2

    .line 965
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sendToHud(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private sendNoResultsFoundResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 899
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v2, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 900
    new-instance v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 901
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 902
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->listeningOverBt:Z

    .line 903
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    .line 904
    invoke-virtual {v1, p1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->recognizedWords(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    .line 905
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v0

    .line 906
    .local v0, "response":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 907
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->onVoiceSearchComplete()V

    .line 908
    return-void
.end method

.method private sendToHud(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p2, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "prefix"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    const/16 v11, 0xa

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 977
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 978
    :cond_0
    invoke-direct {p0, p2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sayNoResultFound(Ljava/lang/String;)V

    .line 1028
    :goto_0
    return-void

    .line 983
    :cond_1
    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v8, "hud_search_result_list_capable"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 986
    .local v1, "hudIsCapableOfSearchResultList":Z
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le v7, v10, :cond_2

    if-eqz v1, :cond_2

    .line 987
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 988
    .local v0, "context":Landroid/content/Context;
    const v7, 0x7f08031f

    new-array v8, v10, [Ljava/lang/Object;

    aput-object p2, v8, v6

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 989
    .local v5, "title":Ljava/lang/String;
    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v8, 0x0

    invoke-virtual {v7, v5, v8, v6}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 994
    .end local v0    # "context":Landroid/content/Context;
    .end local v5    # "title":Ljava/lang/String;
    :cond_2
    invoke-static {p1}, Lcom/navdy/client/app/framework/models/Destination;->convertDestinationsToProto(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    .line 995
    .local v2, "protoDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 996
    .local v4, "size":I
    :goto_1
    sget-object v8, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sending "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " protobuf destination"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-le v4, v10, :cond_7

    const-string v7, "s"

    :goto_2
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " to the HUD: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 998
    if-le v4, v11, :cond_3

    .line 999
    invoke-interface {v2, v6, v11}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 1000
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 1004
    :cond_3
    new-instance v3, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    .line 1005
    .local v3, "responseBuilder":Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    sget-object v7, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v3, v7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 1006
    invoke-virtual {v3, p2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->recognizedWords(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 1007
    invoke-virtual {v3, p3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->prefix(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 1008
    if-eqz v1, :cond_4

    .line 1009
    invoke-virtual {v3, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->results(Ljava/util/List;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 1011
    :cond_4
    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v7

    invoke-static {v7}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 1023
    if-nez v1, :cond_5

    .line 1024
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "The HUD we are connected to can not handle search result lists so routing to the 1st result."

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1025
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v7

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v7, v6, v10}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;Z)V

    .line 1027
    :cond_5
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->onVoiceSearchComplete()V

    goto/16 :goto_0

    .end local v3    # "responseBuilder":Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .end local v4    # "size":I
    :cond_6
    move v4, v6

    .line 995
    goto :goto_1

    .line 996
    .restart local v4    # "size":I
    :cond_7
    const-string v7, ""

    goto :goto_2
.end method


# virtual methods
.method public onVoiceAssistRequest(Lcom/navdy/service/library/events/audio/VoiceAssistRequest;)V
    .locals 1
    .param p1, "voiceAssistRequest"    # Lcom/navdy/service/library/events/audio/VoiceAssistRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleGoogleNowRequest(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)V

    .line 208
    return-void
.end method

.method public onVoiceSearchRequest(Lcom/navdy/service/library/events/audio/VoiceSearchRequest;)V
    .locals 2
    .param p1, "voiceSearchRequest"    # Lcom/navdy/service/library/events/audio/VoiceSearchRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 212
    if-eqz p1, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/VoiceSearchRequest;->end:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Receiving request to cancel Voice search from the HUD"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->userCanceledVoiceSearch:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 215
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->onStopListening()V

    .line 216
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->onVoiceSearchComplete()V

    .line 221
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->userCanceledVoiceSearch:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 219
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleVoiceSearchRequest()V

    goto :goto_0
.end method
