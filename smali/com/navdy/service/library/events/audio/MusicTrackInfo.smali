.class public final Lcom/navdy/service/library/events/audio/MusicTrackInfo;
.super Lcom/squareup/wire/Message;
.source "MusicTrackInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ALBUM:Ljava/lang/String; = ""

.field public static final DEFAULT_AUTHOR:Ljava/lang/String; = ""

.field public static final DEFAULT_COLLECTIONID:Ljava/lang/String; = ""

.field public static final DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final DEFAULT_CURRENTPOSITION:Ljava/lang/Integer;

.field public static final DEFAULT_DATASOURCE:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public static final DEFAULT_DURATION:Ljava/lang/Integer;

.field public static final DEFAULT_INDEX:Ljava/lang/Long;

.field public static final DEFAULT_ISNEXTALLOWED:Ljava/lang/Boolean;

.field public static final DEFAULT_ISONLINESTREAM:Ljava/lang/Boolean;

.field public static final DEFAULT_ISPREVIOUSALLOWED:Ljava/lang/Boolean;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PLAYBACKSTATE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final DEFAULT_PLAYCOUNT:Ljava/lang/Integer;

.field public static final DEFAULT_REPEATMODE:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

.field public static final DEFAULT_SHUFFLEMODE:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

.field public static final DEFAULT_TRACKID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final album:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final author:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xe
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xc
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final currentPosition:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final duration:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final index:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final isNextAllowed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final isOnlineStream:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final isPreviousAllowed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final playCount:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x10
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x12
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x11
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final trackId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xf
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_PLAYBACKSTATE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 19
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_INDEX:Ljava/lang/Long;

    .line 23
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_DURATION:Ljava/lang/Integer;

    .line 24
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_CURRENTPOSITION:Ljava/lang/Integer;

    .line 25
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_ISPREVIOUSALLOWED:Ljava/lang/Boolean;

    .line 26
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_ISNEXTALLOWED:Ljava/lang/Boolean;

    .line 27
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_DATASOURCE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 28
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_ISONLINESTREAM:Ljava/lang/Boolean;

    .line 29
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 30
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 33
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_PLAYCOUNT:Ljava/lang/Integer;

    .line 34
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_SHUFFLEMODE:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 35
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicRepeatMode;->MUSIC_REPEAT_MODE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->DEFAULT_REPEATMODE:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicPlaybackState;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/MusicDataSource;Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/MusicCollectionSource;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/navdy/service/library/events/audio/MusicShuffleMode;Lcom/navdy/service/library/events/audio/MusicRepeatMode;)V
    .locals 1
    .param p1, "playbackState"    # Lcom/navdy/service/library/events/audio/MusicPlaybackState;
    .param p2, "index"    # Ljava/lang/Long;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "author"    # Ljava/lang/String;
    .param p5, "album"    # Ljava/lang/String;
    .param p6, "duration"    # Ljava/lang/Integer;
    .param p7, "currentPosition"    # Ljava/lang/Integer;
    .param p8, "isPreviousAllowed"    # Ljava/lang/Boolean;
    .param p9, "isNextAllowed"    # Ljava/lang/Boolean;
    .param p10, "dataSource"    # Lcom/navdy/service/library/events/audio/MusicDataSource;
    .param p11, "isOnlineStream"    # Ljava/lang/Boolean;
    .param p12, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .param p13, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .param p14, "collectionId"    # Ljava/lang/String;
    .param p15, "trackId"    # Ljava/lang/String;
    .param p16, "playCount"    # Ljava/lang/Integer;
    .param p17, "shuffleMode"    # Lcom/navdy/service/library/events/audio/MusicShuffleMode;
    .param p18, "repeatMode"    # Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 143
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 144
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    .line 145
    iput-object p3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    .line 146
    iput-object p4, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    .line 147
    iput-object p5, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    .line 148
    iput-object p6, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    .line 149
    iput-object p7, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    .line 150
    iput-object p8, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    .line 151
    iput-object p9, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    .line 152
    iput-object p10, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 153
    iput-object p11, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isOnlineStream:Ljava/lang/Boolean;

    .line 154
    iput-object p12, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 155
    iput-object p13, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 156
    iput-object p14, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionId:Ljava/lang/String;

    .line 157
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    .line 158
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playCount:Ljava/lang/Integer;

    .line 159
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 160
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .line 161
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;)V
    .locals 20
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    .prologue
    .line 164
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->index:Ljava/lang/Long;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->duration:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->currentPosition:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isOnlineStream:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playCount:Ljava/lang/Integer;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    move-object/from16 v19, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v19}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;-><init>(Lcom/navdy/service/library/events/audio/MusicPlaybackState;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/MusicDataSource;Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/MusicCollectionSource;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/navdy/service/library/events/audio/MusicShuffleMode;Lcom/navdy/service/library/events/audio/MusicRepeatMode;)V

    .line 165
    invoke-virtual/range {p0 .. p1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 166
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;Lcom/navdy/service/library/events/audio/MusicTrackInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 170
    if-ne p1, p0, :cond_1

    .line 190
    :cond_0
    :goto_0
    return v1

    .line 171
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 172
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 173
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    .line 174
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    .line 175
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    .line 176
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    .line 177
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    .line 178
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    .line 179
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    .line 180
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    .line 181
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 182
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isOnlineStream:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isOnlineStream:Ljava/lang/Boolean;

    .line 183
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 184
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 185
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionId:Ljava/lang/String;

    .line 186
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    .line 187
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playCount:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playCount:Ljava/lang/Integer;

    .line 188
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 189
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .line 190
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 195
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->hashCode:I

    .line 196
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 197
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->hashCode()I

    move-result v0

    .line 198
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 199
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 200
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 201
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 202
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 203
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 204
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v3, v2

    .line 205
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v3, v2

    .line 206
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicDataSource;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v3, v2

    .line 207
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isOnlineStream:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isOnlineStream:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v3, v2

    .line 208
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->hashCode()I

    move-result v2

    :goto_b
    add-int v0, v3, v2

    .line 209
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->hashCode()I

    move-result v2

    :goto_c
    add-int v0, v3, v2

    .line 210
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionId:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_d
    add-int v0, v3, v2

    .line 211
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_e
    add-int v0, v3, v2

    .line 212
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playCount:Ljava/lang/Integer;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_f
    add-int v0, v3, v2

    .line 213
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->hashCode()I

    move-result v2

    :goto_10
    add-int v0, v3, v2

    .line 214
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicRepeatMode;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 215
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->hashCode:I

    .line 217
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 197
    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 198
    goto/16 :goto_1

    :cond_4
    move v2, v1

    .line 199
    goto/16 :goto_2

    :cond_5
    move v2, v1

    .line 200
    goto/16 :goto_3

    :cond_6
    move v2, v1

    .line 201
    goto/16 :goto_4

    :cond_7
    move v2, v1

    .line 202
    goto/16 :goto_5

    :cond_8
    move v2, v1

    .line 203
    goto/16 :goto_6

    :cond_9
    move v2, v1

    .line 204
    goto/16 :goto_7

    :cond_a
    move v2, v1

    .line 205
    goto/16 :goto_8

    :cond_b
    move v2, v1

    .line 206
    goto/16 :goto_9

    :cond_c
    move v2, v1

    .line 207
    goto/16 :goto_a

    :cond_d
    move v2, v1

    .line 208
    goto :goto_b

    :cond_e
    move v2, v1

    .line 209
    goto :goto_c

    :cond_f
    move v2, v1

    .line 210
    goto :goto_d

    :cond_10
    move v2, v1

    .line 211
    goto :goto_e

    :cond_11
    move v2, v1

    .line 212
    goto :goto_f

    :cond_12
    move v2, v1

    .line 213
    goto :goto_10
.end method
