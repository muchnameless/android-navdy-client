.class public Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;
.super Ljava/lang/Object;
.source "DefaultJWTClaimsVerifier.java"

# interfaces
.implements Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;
.implements Lcom/nimbusds/jwt/proc/ClockSkewAware;


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field public static final DEFAULT_MAX_CLOCK_SKEW_SECONDS:I = 0x3c

.field private static final EXPIRED_JWT_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;

.field private static final JWT_BEFORE_USE_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;


# instance fields
.field private maxClockSkew:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/nimbusds/jwt/proc/BadJWTException;

    const-string v1, "Expired JWT"

    invoke-direct {v0, v1}, Lcom/nimbusds/jwt/proc/BadJWTException;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->EXPIRED_JWT_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;

    .line 51
    new-instance v0, Lcom/nimbusds/jwt/proc/BadJWTException;

    const-string v1, "JWT before use time"

    invoke-direct {v0, v1}, Lcom/nimbusds/jwt/proc/BadJWTException;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->JWT_BEFORE_USE_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/16 v0, 0x3c

    iput v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->maxClockSkew:I

    .line 30
    return-void
.end method


# virtual methods
.method public getMaxClockSkew()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->maxClockSkew:I

    return v0
.end method

.method public setMaxClockSkew(I)V
    .locals 0
    .param p1, "maxClockSkewSeconds"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->maxClockSkew:I

    .line 69
    return-void
.end method

.method public verify(Lcom/nimbusds/jwt/JWTClaimsSet;)V
    .locals 6
    .param p1, "claimsSet"    # Lcom/nimbusds/jwt/JWTClaimsSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jwt/proc/BadJWTException;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 78
    .local v2, "now":Ljava/util/Date;
    invoke-virtual {p1}, Lcom/nimbusds/jwt/JWTClaimsSet;->getExpirationTime()Ljava/util/Date;

    move-result-object v0

    .line 80
    .local v0, "exp":Ljava/util/Date;
    if-eqz v0, :cond_0

    .line 82
    iget v3, p0, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->maxClockSkew:I

    int-to-long v4, v3

    invoke-static {v0, v2, v4, v5}, Lcom/nimbusds/jose/util/DateUtils;->isAfter(Ljava/util/Date;Ljava/util/Date;J)Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    sget-object v3, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->EXPIRED_JWT_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;

    throw v3

    .line 87
    :cond_0
    invoke-virtual {p1}, Lcom/nimbusds/jwt/JWTClaimsSet;->getNotBeforeTime()Ljava/util/Date;

    move-result-object v1

    .line 89
    .local v1, "nbf":Ljava/util/Date;
    if-eqz v1, :cond_1

    .line 91
    iget v3, p0, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->maxClockSkew:I

    int-to-long v4, v3

    invoke-static {v1, v2, v4, v5}, Lcom/nimbusds/jose/util/DateUtils;->isBefore(Ljava/util/Date;Ljava/util/Date;J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 92
    sget-object v3, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;->JWT_BEFORE_USE_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;

    throw v3

    .line 95
    :cond_1
    return-void
.end method
