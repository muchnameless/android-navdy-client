.class public Lcom/google/maps/ElevationApi;
.super Ljava/lang/Object;
.source "ElevationApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/maps/ElevationApi$MultiResponse;,
        Lcom/google/maps/ElevationApi$SingularResponse;
    }
.end annotation


# static fields
.field private static final API_CONFIG:Lcom/google/maps/internal/ApiConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/google/maps/internal/ApiConfig;

    const-string v1, "/maps/api/elevation/json"

    invoke-direct {v0, v1}, Lcom/google/maps/internal/ApiConfig;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/maps/ElevationApi;->API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method public static getByPath(Lcom/google/maps/GeoApiContext;ILcom/google/maps/model/EncodedPolyline;)Lcom/google/maps/PendingResult;
    .locals 7
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "samples"    # I
    .param p2, "encodedPolyline"    # Lcom/google/maps/model/EncodedPolyline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "I",
            "Lcom/google/maps/model/EncodedPolyline;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<[",
            "Lcom/google/maps/model/ElevationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    sget-object v1, Lcom/google/maps/ElevationApi;->API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v2, Lcom/google/maps/ElevationApi$MultiResponse;

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "samples"

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const-string v4, "path"

    aput-object v4, v3, v0

    const/4 v4, 0x3

    const-string v5, "enc:"

    invoke-virtual {p2}, Lcom/google/maps/model/EncodedPolyline;->getEncodedPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static varargs getByPath(Lcom/google/maps/GeoApiContext;I[Lcom/google/maps/model/LatLng;)Lcom/google/maps/PendingResult;
    .locals 5
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "samples"    # I
    .param p2, "path"    # [Lcom/google/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "I[",
            "Lcom/google/maps/model/LatLng;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<[",
            "Lcom/google/maps/model/ElevationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    sget-object v0, Lcom/google/maps/ElevationApi;->API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v1, Lcom/google/maps/ElevationApi$MultiResponse;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "samples"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "path"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Lcom/google/maps/ElevationApi;->shortestParam([Lcom/google/maps/model/LatLng;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0
.end method

.method public static getByPoint(Lcom/google/maps/GeoApiContext;Lcom/google/maps/model/LatLng;)Lcom/google/maps/PendingResult;
    .locals 5
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "point"    # Lcom/google/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "Lcom/google/maps/model/LatLng;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<",
            "Lcom/google/maps/model/ElevationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    sget-object v0, Lcom/google/maps/ElevationApi;->API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v1, Lcom/google/maps/ElevationApi$SingularResponse;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "locations"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/maps/model/LatLng;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0
.end method

.method public static getByPoints(Lcom/google/maps/GeoApiContext;Lcom/google/maps/model/EncodedPolyline;)Lcom/google/maps/PendingResult;
    .locals 7
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "encodedPolyline"    # Lcom/google/maps/model/EncodedPolyline;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "Lcom/google/maps/model/EncodedPolyline;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<[",
            "Lcom/google/maps/model/ElevationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    sget-object v1, Lcom/google/maps/ElevationApi;->API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v2, Lcom/google/maps/ElevationApi$MultiResponse;

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "locations"

    aput-object v4, v3, v0

    const/4 v4, 0x1

    const-string v5, "enc:"

    invoke-virtual {p1}, Lcom/google/maps/model/EncodedPolyline;->getEncodedPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static varargs getByPoints(Lcom/google/maps/GeoApiContext;[Lcom/google/maps/model/LatLng;)Lcom/google/maps/PendingResult;
    .locals 5
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "points"    # [Lcom/google/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "[",
            "Lcom/google/maps/model/LatLng;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<[",
            "Lcom/google/maps/model/ElevationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    sget-object v0, Lcom/google/maps/ElevationApi;->API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v1, Lcom/google/maps/ElevationApi$MultiResponse;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "locations"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/maps/ElevationApi;->shortestParam([Lcom/google/maps/model/LatLng;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0
.end method

.method private static shortestParam([Lcom/google/maps/model/LatLng;)Ljava/lang/String;
    .locals 5
    .param p0, "points"    # [Lcom/google/maps/model/LatLng;

    .prologue
    .line 76
    const/16 v2, 0x7c

    invoke-static {v2, p0}, Lcom/google/maps/internal/StringJoin;->join(C[Lcom/google/maps/internal/StringJoin$UrlValue;)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "joined":Ljava/lang/String;
    const-string v2, "enc:"

    invoke-static {p0}, Lcom/google/maps/internal/PolylineEncoding;->encode([Lcom/google/maps/model/LatLng;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "encoded":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .end local v1    # "joined":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 77
    .end local v0    # "encoded":Ljava/lang/String;
    .restart local v1    # "joined":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .restart local v0    # "encoded":Ljava/lang/String;
    :cond_1
    move-object v1, v0

    .line 78
    goto :goto_1
.end method
