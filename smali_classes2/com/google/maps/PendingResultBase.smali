.class abstract Lcom/google/maps/PendingResultBase;
.super Ljava/lang/Object;
.source "PendingResultBase.java"

# interfaces
.implements Lcom/google/maps/PendingResult;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "A:",
        "Lcom/google/maps/PendingResultBase",
        "<TT;TA;TR;>;R::",
        "Lcom/google/maps/internal/ApiResponse",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Lcom/google/maps/PendingResult",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final config:Lcom/google/maps/internal/ApiConfig;

.field private final context:Lcom/google/maps/GeoApiContext;

.field private delegate:Lcom/google/maps/PendingResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/PendingResult",
            "<TT;>;"
        }
    .end annotation
.end field

.field private params:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private responseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+TR;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/google/maps/GeoApiContext;Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;)V
    .locals 1
    .param p1, "context"    # Lcom/google/maps/GeoApiContext;
    .param p2, "config"    # Lcom/google/maps/internal/ApiConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "Lcom/google/maps/internal/ApiConfig;",
            "Ljava/lang/Class",
            "<+TR;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    .local p3, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/maps/PendingResultBase;->params:Ljava/util/HashMap;

    .line 43
    iput-object p1, p0, Lcom/google/maps/PendingResultBase;->context:Lcom/google/maps/GeoApiContext;

    .line 44
    iput-object p2, p0, Lcom/google/maps/PendingResultBase;->config:Lcom/google/maps/internal/ApiConfig;

    .line 45
    iput-object p3, p0, Lcom/google/maps/PendingResultBase;->responseClass:Ljava/lang/Class;

    .line 46
    return-void
.end method

.method private makeRequest()Lcom/google/maps/PendingResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/maps/PendingResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    iget-object v0, p0, Lcom/google/maps/PendingResultBase;->delegate:Lcom/google/maps/PendingResult;

    if-eqz v0, :cond_0

    .line 75
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "\'await\', \'awaitIgnoreError\' or \'setCallback\' was already called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/google/maps/PendingResultBase;->validateRequest()V

    .line 79
    iget-object v0, p0, Lcom/google/maps/PendingResultBase;->context:Lcom/google/maps/GeoApiContext;

    iget-object v1, p0, Lcom/google/maps/PendingResultBase;->config:Lcom/google/maps/internal/ApiConfig;

    iget-object v2, p0, Lcom/google/maps/PendingResultBase;->responseClass:Ljava/lang/Class;

    iget-object v3, p0, Lcom/google/maps/PendingResultBase;->params:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;Ljava/util/Map;)Lcom/google/maps/PendingResult;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/PendingResultBase;->delegate:Lcom/google/maps/PendingResult;

    .line 80
    iget-object v0, p0, Lcom/google/maps/PendingResultBase;->delegate:Lcom/google/maps/PendingResult;

    return-object v0
.end method


# virtual methods
.method public final await()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    invoke-direct {p0}, Lcom/google/maps/PendingResultBase;->makeRequest()Lcom/google/maps/PendingResult;

    move-result-object v0

    .line 56
    .local v0, "request":Lcom/google/maps/PendingResult;, "Lcom/google/maps/PendingResult<TT;>;"
    invoke-interface {v0}, Lcom/google/maps/PendingResult;->await()Ljava/lang/Object;

    move-result-object v1

    .line 57
    .local v1, "result":Ljava/lang/Object;, "TT;"
    return-object v1
.end method

.method public final awaitIgnoreError()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    invoke-direct {p0}, Lcom/google/maps/PendingResultBase;->makeRequest()Lcom/google/maps/PendingResult;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/maps/PendingResult;->awaitIgnoreError()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    iget-object v0, p0, Lcom/google/maps/PendingResultBase;->delegate:Lcom/google/maps/PendingResult;

    if-nez v0, :cond_0

    .line 71
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/maps/PendingResultBase;->delegate:Lcom/google/maps/PendingResult;

    invoke-interface {v0}, Lcom/google/maps/PendingResult;->cancel()V

    goto :goto_0
.end method

.method public final language(Ljava/lang/String;)Lcom/google/maps/PendingResultBase;
    .locals 1
    .param p1, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TA;"
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    const-string v0, "language"

    invoke-virtual {p0, v0, p1}, Lcom/google/maps/PendingResultBase;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    return-object v0
.end method

.method protected param(Ljava/lang/String;Lcom/google/maps/internal/StringJoin$UrlValue;)Lcom/google/maps/PendingResultBase;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "val"    # Lcom/google/maps/internal/StringJoin$UrlValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/maps/internal/StringJoin$UrlValue;",
            ")TA;"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    iget-object v1, p0, Lcom/google/maps/PendingResultBase;->params:Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    move-object v0, p0

    .line 98
    .local v0, "result":Lcom/google/maps/PendingResultBase;, "TA;"
    return-object v0
.end method

.method protected param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TA;"
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    iget-object v1, p0, Lcom/google/maps/PendingResultBase;->params:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    move-object v0, p0

    .line 90
    .local v0, "result":Lcom/google/maps/PendingResultBase;, "TA;"
    return-object v0
.end method

.method protected params()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    iget-object v0, p0, Lcom/google/maps/PendingResultBase;->params:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final setCallback(Lcom/google/maps/PendingResult$Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/PendingResult$Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "this":Lcom/google/maps/PendingResultBase;, "Lcom/google/maps/PendingResultBase<TT;TA;TR;>;"
    .local p1, "callback":Lcom/google/maps/PendingResult$Callback;, "Lcom/google/maps/PendingResult$Callback<TT;>;"
    invoke-direct {p0}, Lcom/google/maps/PendingResultBase;->makeRequest()Lcom/google/maps/PendingResult;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/maps/PendingResult;->setCallback(Lcom/google/maps/PendingResult$Callback;)V

    .line 51
    return-void
.end method

.method protected abstract validateRequest()V
.end method
