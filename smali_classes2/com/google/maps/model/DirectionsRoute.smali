.class public Lcom/google/maps/model/DirectionsRoute;
.super Ljava/lang/Object;
.source "DirectionsRoute.java"


# instance fields
.field public bounds:Lcom/google/maps/model/Bounds;

.field public copyrights:Ljava/lang/String;

.field public fare:Lcom/google/maps/model/Fare;

.field public legs:[Lcom/google/maps/model/DirectionsLeg;

.field public overviewPolyline:Lcom/google/maps/model/EncodedPolyline;

.field public summary:Ljava/lang/String;

.field public warnings:[Ljava/lang/String;

.field public waypointOrder:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
