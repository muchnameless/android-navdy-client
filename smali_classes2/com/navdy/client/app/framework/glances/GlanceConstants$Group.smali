.class public final enum Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;
.super Ljava/lang/Enum;
.source "GlanceConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/glances/GlanceConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Group"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

.field public static final enum CALENDAR_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

.field public static final enum DRIVING_GLANCES:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

.field public static final enum EMAIL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

.field public static final enum IGNORE_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

.field public static final enum MESSAGING_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

.field public static final enum MUSIC_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

.field public static final enum SOCIAL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

.field public static final enum WHITE_LIST:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 89
    new-instance v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    const-string v1, "DRIVING_GLANCES"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->DRIVING_GLANCES:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .line 90
    new-instance v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    const-string v1, "WHITE_LIST"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->WHITE_LIST:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .line 91
    new-instance v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    const-string v1, "EMAIL_GROUP"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->EMAIL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .line 92
    new-instance v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    const-string v1, "MESSAGING_GROUP"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MESSAGING_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .line 93
    new-instance v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    const-string v1, "CALENDAR_GROUP"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->CALENDAR_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .line 94
    new-instance v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    const-string v1, "SOCIAL_GROUP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->SOCIAL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .line 95
    new-instance v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    const-string v1, "MUSIC_GROUP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MUSIC_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .line 96
    new-instance v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    const-string v1, "IGNORE_GROUP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->IGNORE_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    .line 88
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->DRIVING_GLANCES:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->WHITE_LIST:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->EMAIL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MESSAGING_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->CALENDAR_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->SOCIAL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MUSIC_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->IGNORE_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->$VALUES:[Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 88
    const-class v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->$VALUES:[Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    return-object v0
.end method
