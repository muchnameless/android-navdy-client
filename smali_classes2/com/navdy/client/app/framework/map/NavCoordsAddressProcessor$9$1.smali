.class Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;->onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;

.field final synthetic val$destinations:Ljava/util/List;

.field final synthetic val$error:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;

    .prologue
    .line 773
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$destinations:Ljava/util/List;

    iput-object p3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$error:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 776
    const/4 v1, 0x0

    .line 778
    .local v1, "foundValidNavCoordinates":Z
    iget-object v8, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$destinations:Ljava/util/List;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$destinations:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_4

    .line 782
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$destinations:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_4

    if-nez v1, :cond_4

    .line 783
    iget-object v8, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$destinations:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 787
    .local v6, "result":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    if-eqz v6, :cond_2

    .line 788
    iget-object v3, v6, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    .line 789
    .local v3, "lat":Ljava/lang/String;
    iget-object v4, v6, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    .line 791
    .local v4, "lng":Ljava/lang/String;
    sget-object v5, Lcom/navdy/client/app/framework/models/Destination$Precision;->PRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 793
    .local v5, "precision":Lcom/navdy/client/app/framework/models/Destination$Precision;
    iget-object v8, v6, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->types:[Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 794
    iget-object v9, v6, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->types:[Ljava/lang/String;

    array-length v10, v9

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v10, :cond_1

    aget-object v7, v9, v8

    .line 795
    .local v7, "type":Ljava/lang/String;
    const-string v11, "locality"

    invoke-static {v7, v11}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "postal_code"

    .line 796
    invoke-static {v7, v11}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "country"

    .line 797
    invoke-static {v7, v11}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "administrative_area_level_1"

    .line 798
    invoke-static {v7, v11}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "neighborhood"

    .line 799
    invoke-static {v7, v11}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "route"

    .line 800
    invoke-static {v7, v11}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 801
    :cond_0
    sget-object v5, Lcom/navdy/client/app/framework/models/Destination$Precision;->IMPRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 807
    .end local v7    # "type":Ljava/lang/String;
    :cond_1
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 808
    const/4 v1, 0x1

    .line 810
    move-object v0, v5

    .line 812
    .local v0, "finalPrecision":Lcom/navdy/client/app/framework/models/Destination$Precision;
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$1100()Landroid/os/Handler;

    move-result-object v8

    new-instance v9, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;

    invoke-direct {v9, p0, v3, v4, v0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 782
    .end local v0    # "finalPrecision":Lcom/navdy/client/app/framework/models/Destination$Precision;
    .end local v3    # "lat":Ljava/lang/String;
    .end local v4    # "lng":Ljava/lang/String;
    .end local v5    # "precision":Lcom/navdy/client/app/framework/models/Destination$Precision;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 794
    .restart local v3    # "lat":Ljava/lang/String;
    .restart local v4    # "lng":Ljava/lang/String;
    .restart local v5    # "precision":Lcom/navdy/client/app/framework/models/Destination$Precision;
    .restart local v7    # "type":Ljava/lang/String;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 833
    .end local v2    # "i":I
    .end local v3    # "lat":Ljava/lang/String;
    .end local v4    # "lng":Ljava/lang/String;
    .end local v5    # "precision":Lcom/navdy/client/app/framework/models/Destination$Precision;
    .end local v6    # "result":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v7    # "type":Ljava/lang/String;
    :cond_4
    if-nez v1, :cond_5

    .line 834
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "Google Directions API failed."

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 835
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Could not find valid coordinates out of: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$destinations:Ljava/util/List;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 837
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$1100()Landroid/os/Handler;

    move-result-object v8

    new-instance v9, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$2;

    invoke-direct {v9, p0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$2;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 844
    :cond_5
    return-void
.end method
