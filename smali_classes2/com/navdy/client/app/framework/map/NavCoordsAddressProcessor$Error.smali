.class public final enum Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;
.super Ljava/lang/Enum;
.source "NavCoordsAddressProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

.field public static final enum BAD_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

.field public static final enum CACHE_NO_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

.field public static final enum INVALID_REQUEST:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

.field public static final enum NONE:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

.field public static final enum NO_INTERNET:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

.field public static final enum SERVICE_ERROR:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 914
    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->NONE:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .line 915
    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    const-string v1, "INVALID_REQUEST"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->INVALID_REQUEST:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .line 916
    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    const-string v1, "BAD_COORDS"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->BAD_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .line 917
    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    const-string v1, "SERVICE_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->SERVICE_ERROR:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .line 918
    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    const-string v1, "NO_INTERNET"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->NO_INTERNET:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .line 919
    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    const-string v1, "CACHE_NO_COORDS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->CACHE_NO_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .line 913
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->NONE:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->INVALID_REQUEST:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->BAD_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->SERVICE_ERROR:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->NO_INTERNET:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->CACHE_NO_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->$VALUES:[Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 913
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 913
    const-class v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;
    .locals 1

    .prologue
    .line 913
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->$VALUES:[Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    return-object v0
.end method
