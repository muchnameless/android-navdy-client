.class public Lcom/navdy/client/app/framework/models/PhoneNumberModel;
.super Ljava/lang/Object;
.source "PhoneNumberModel.java"


# instance fields
.field public customType:Ljava/lang/String;

.field public isPrimary:Z

.field public number:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "customType"    # Ljava/lang/String;
    .param p4, "isPrimary"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    .line 19
    iput p2, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    .line 20
    iput-object p3, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->customType:Ljava/lang/String;

    .line 21
    iput-boolean p4, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->isPrimary:Z

    .line 22
    return-void
.end method


# virtual methods
.method getPhoneNumberProtobufType()Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    packed-switch v0, :pswitch_data_0

    .line 39
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_OTHER:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    :goto_0
    return-object v0

    .line 33
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_HOME:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 35
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_WORK:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 37
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_MOBILE:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method isFax()Z
    .locals 2

    .prologue
    .line 25
    iget v0, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PhoneNumberModel{number=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", customType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->customType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isPrimary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->isPrimary:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
