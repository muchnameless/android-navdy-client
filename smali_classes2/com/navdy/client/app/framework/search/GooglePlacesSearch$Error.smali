.class public final enum Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
.super Ljava/lang/Enum;
.source "GooglePlacesSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

.field public static final enum NONE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

.field public static final enum SERVICE_ERROR:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 460
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->NONE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    const-string v1, "SERVICE_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->SERVICE_ERROR:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    .line 459
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->NONE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->SERVICE_ERROR:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->$VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 459
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 459
    const-class v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .locals 1

    .prologue
    .line 459
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->$VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    return-object v0
.end method
