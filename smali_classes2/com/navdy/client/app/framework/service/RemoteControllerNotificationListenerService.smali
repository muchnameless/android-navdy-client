.class public Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;
.super Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;
.source "RemoteControllerNotificationListenerService.java"

# interfaces
.implements Landroid/media/RemoteController$OnClientUpdateListener;


# static fields
.field private static final MINIMUM_UPDATE_WAIT:I = 0x3e8

.field private static final REMOTE_CONTROL_PLAYSTATE_NONE:I

.field private static final VERBOSE:Z

.field public static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private albumArt:Landroid/graphics/Bitmap;

.field private audioManager:Landroid/media/AudioManager;

.field private ignoreMetadata:Z

.field private lastUpdateTime:D

.field private lastUpdateWasClientMetadata:Z

.field private musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private playbackState:I

.field private remoteController:Landroid/media/RemoteController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    .line 49
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->EMPTY_TRACK_INFO:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iput-object v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->albumArt:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;)Landroid/media/RemoteController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->remoteController:Landroid/media/RemoteController;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;Landroid/media/RemoteController;)Landroid/media/RemoteController;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;
    .param p1, "x1"    # Landroid/media/RemoteController;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->remoteController:Landroid/media/RemoteController;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;Landroid/media/AudioManager;)Landroid/media/AudioManager;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;
    .param p1, "x1"    # Landroid/media/AudioManager;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->audioManager:Landroid/media/AudioManager;

    return-object p1
.end method

.method private checkForSpuriousStateUpdate(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    const/4 v1, 0x3

    .line 196
    if-ge p1, v1, :cond_0

    iget v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    if-ge v0, v1, :cond_0

    .line 200
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Spurious none/stop/pause detected, will ignore metadata updates until we leave none/stopped/paused states"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->ignoreMetadata:Z

    .line 205
    :goto_0
    return-void

    .line 203
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->ignoreMetadata:Z

    goto :goto_0
.end method

.method private initializeRemoteController()V
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "initializeRemoteController"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->remoteController:Landroid/media/RemoteController;

    if-eqz v0, :cond_0

    .line 61
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "RemoteController already initialized, returning"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 93
    :goto_0
    return-void

    .line 65
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;-><init>(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private isAdvertisement()Z
    .locals 5

    .prologue
    .line 282
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v1

    .line 283
    .local v1, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    .line 284
    .local v0, "currentTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    .line 285
    .local v2, "name":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 286
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "advertisement"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    .line 287
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    .line 288
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private logMetadata(Landroid/media/RemoteController$MetadataEditor;)V
    .locals 6
    .param p1, "metadataEditor"    # Landroid/media/RemoteController$MetadataEditor;

    .prologue
    .line 317
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "- METADATA_KEY_ALBUM: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 318
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "- METADATA_KEY_ARTIST: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 319
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "- METADATA_KEY_TITLE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x7

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 320
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "- METADATA_KEY_DURATION: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x9

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v2, v4, v5}, Landroid/media/RemoteController$MetadataEditor;->getLong(IJ)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 321
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "- BITMAP_KEY_ARTWORK: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x64

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/media/RemoteController$MetadataEditor;->getBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 322
    return-void
.end method

.method private musicPlaybackState(I)Lcom/navdy/service/library/events/audio/MusicPlaybackState;
    .locals 1
    .param p1, "remoteControlClientPlaybackState"    # I

    .prologue
    .line 292
    packed-switch p1, :pswitch_data_0

    .line 312
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    :goto_0
    return-object v0

    .line 294
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_STOPPED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 296
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 298
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 300
    :pswitch_3
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_FAST_FORWARDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 302
    :pswitch_4
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_REWINDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 304
    :pswitch_5
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_SKIPPING_TO_NEXT:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 306
    :pswitch_6
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_SKIPPING_TO_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 308
    :pswitch_7
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_BUFFERING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 310
    :pswitch_8
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_ERROR:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    goto :goto_0

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private sendAlbumArt()V
    .locals 3

    .prologue
    .line 277
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v0

    .line 278
    .local v0, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->albumArt:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->checkAndSetCurrentMediaArtwork(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Landroid/graphics/Bitmap;)V

    .line 279
    return-void
.end method

.method private setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 2
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 267
    if-nez p1, :cond_0

    .line 268
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->EMPTY_TRACK_INFO:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 272
    :goto_0
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v0

    .line 273
    .local v0, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    invoke-virtual {v0, p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->setCurrentMediaTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 274
    return-void

    .line 270
    .end local v0    # "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    :cond_0
    iput-object p1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 97
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onBind"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 99
    invoke-direct {p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->initializeRemoteController()V

    .line 101
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onClientChange(Z)V
    .locals 4
    .param p1, "clearing"    # Z

    .prologue
    const/4 v3, 0x0

    .line 132
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClientChange: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 134
    if-eqz p1, :cond_0

    .line 135
    invoke-direct {p0, v3}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 136
    iput-object v3, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->albumArt:Landroid/graphics/Bitmap;

    .line 137
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    .line 139
    :cond_0
    return-void
.end method

.method public onClientMetadataUpdate(Landroid/media/RemoteController$MetadataEditor;)V
    .locals 7
    .param p1, "metadataEditor"    # Landroid/media/RemoteController$MetadataEditor;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 225
    sget-object v2, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "onClientMetadataUpdate"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 226
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->lastUpdateWasClientMetadata:Z

    .line 228
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->ignoreMetadata:Z

    if-eqz v2, :cond_1

    .line 229
    sget-object v2, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "ignoring metadata due to prior spurious update"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    new-instance v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v3, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_OS_NOTIFICATION:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 238
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->dataSource(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    const-string v3, ""

    .line 239
    invoke-virtual {p1, v4, v3}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    const/4 v3, 0x2

    const-string v4, ""

    .line 240
    invoke-virtual {p1, v3, v4}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    const/4 v3, 0x7

    const-string v4, ""

    .line 241
    invoke-virtual {p1, v3, v4}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    const/16 v3, 0x9

    const-wide/16 v4, 0x0

    .line 242
    invoke-virtual {p1, v3, v4, v5}, Landroid/media/RemoteController$MetadataEditor;->getLong(IJ)J

    move-result-wide v4

    long-to-int v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->duration(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    .line 243
    invoke-direct {p0, v3}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicPlaybackState(I)Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 244
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 245
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    .line 246
    invoke-virtual {v2, v6}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    .line 247
    invoke-virtual {v2, v6}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    .line 248
    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    .line 249
    .local v1, "currentTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 251
    iput-object v6, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->albumArt:Landroid/graphics/Bitmap;

    .line 252
    const/16 v2, 0x64

    invoke-virtual {p1, v2, v6}, Landroid/media/RemoteController$MetadataEditor;->getBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 253
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 254
    iput-object v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->albumArt:Landroid/graphics/Bitmap;

    .line 255
    invoke-direct {p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->sendAlbumArt()V

    goto :goto_0
.end method

.method public onClientPlaybackStateUpdate(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    const/4 v3, 0x0

    .line 142
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClientPlaybackStateUpdate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 144
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->checkForSpuriousStateUpdate(I)V

    .line 146
    iget v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    if-ne p1, v0, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    if-eqz p1, :cond_0

    .line 152
    iput p1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    .line 153
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    iget v1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    .line 154
    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicPlaybackState(I)Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 155
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 156
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 157
    invoke-virtual {v0, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 158
    invoke-virtual {v0, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    .line 153
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 160
    invoke-direct {p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->sendAlbumArt()V

    goto :goto_0
.end method

.method public onClientPlaybackStateUpdate(IJJF)V
    .locals 8
    .param p1, "state"    # I
    .param p2, "stateChangeTimeMs"    # J
    .param p4, "currentPosMs"    # J
    .param p6, "speed"    # F

    .prologue
    const/4 v6, 0x0

    .line 165
    sget-object v2, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onClientPlaybackStateUpdate: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 167
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->checkForSpuriousStateUpdate(I)V

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 172
    .local v0, "currentTime":J
    if-eqz p1, :cond_2

    iget v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    if-ne v2, p1, :cond_0

    long-to-double v2, v0

    iget-wide v4, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->lastUpdateTime:D

    sub-double/2addr v2, v4

    const-wide v4, 0x408f400000000000L    # 1000.0

    cmpl-double v2, v2, v4

    if-gez v2, :cond_0

    iget-boolean v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->lastUpdateWasClientMetadata:Z

    if-eqz v2, :cond_2

    .line 176
    :cond_0
    long-to-double v2, v0

    iput-wide v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->lastUpdateTime:D

    .line 177
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->lastUpdateWasClientMetadata:Z

    .line 179
    new-instance v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v3, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 180
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicPlaybackState(I)Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    long-to-int v3, p4

    .line 181
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->currentPosition(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 182
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 183
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    .line 184
    invoke-virtual {v2, v6}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    .line 185
    invoke-virtual {v2, v6}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    .line 186
    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v2

    .line 179
    invoke-direct {p0, v2}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 188
    iget v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    if-eq v2, p1, :cond_1

    .line 189
    invoke-direct {p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->sendAlbumArt()V

    .line 191
    :cond_1
    iput p1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->playbackState:I

    .line 193
    :cond_2
    return-void
.end method

.method public onClientSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "b"    # Landroid/os/Bundle;

    .prologue
    .line 262
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onClientSessionEvent"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 263
    return-void
.end method

.method public onClientTransportControlUpdate(I)V
    .locals 7
    .param p1, "transportControlFlags"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 208
    sget-object v1, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onClientTransportControlUpdate: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 210
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v0

    .line 211
    .local v0, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    new-instance v4, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    iget-object v1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->musicTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {v4, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_2

    move v1, v2

    .line 212
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    .line 213
    invoke-direct {p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->isAdvertisement()Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "com.pandora.android"

    .line 214
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getLastMusicApp()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    and-int/lit16 v4, p1, 0x80

    if-eqz v4, :cond_3

    .line 213
    :cond_1
    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 216
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 217
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    .line 218
    invoke-virtual {v1, v6}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    .line 219
    invoke-virtual {v1, v6}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v1

    .line 220
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    .line 211
    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->setMusicTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 221
    invoke-direct {p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->sendAlbumArt()V

    .line 222
    return-void

    :cond_2
    move v1, v3

    .line 211
    goto :goto_0

    :cond_3
    move v2, v3

    .line 214
    goto :goto_1
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onCreate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 55
    invoke-super {p0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->onCreate()V

    .line 56
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 119
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDestroy"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->audioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->remoteController:Landroid/media/RemoteController;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteController(Landroid/media/RemoteController;)V

    .line 124
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->setMusicSeekHelper(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;)V

    .line 126
    invoke-super {p0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->onDestroy()V

    .line 127
    return-void
.end method

.method public onListenerConnected()V
    .locals 2

    .prologue
    .line 106
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onListenerConnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 107
    invoke-direct {p0}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->initializeRemoteController()V

    .line 108
    invoke-super {p0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->onListenerConnected()V

    .line 109
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 113
    sget-object v0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onUnbind"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 114
    invoke-super {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
