.class public Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StartActiveTripEvent"
.end annotation


# instance fields
.field public final destination:Lcom/navdy/client/app/framework/models/Destination;

.field public final route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

.field public final routeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "route"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .param p3, "routeId"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 61
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 62
    iput-object p3, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->routeId:Ljava/lang/String;

    .line 63
    return-void
.end method
