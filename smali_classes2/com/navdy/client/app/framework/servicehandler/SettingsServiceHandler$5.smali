.class Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$5;
.super Ljava/lang/Object;
.source "SettingsServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->onCannedMessagesRequest(Lcom/navdy/service/library/events/glances/CannedMessagesRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

.field final synthetic val$cannedMessagesRequest:Lcom/navdy/service/library/events/glances/CannedMessagesRequest;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/glances/CannedMessagesRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$5;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$5;->val$cannedMessagesRequest:Lcom/navdy/service/library/events/glances/CannedMessagesRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v11, 0x0

    .line 142
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 143
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v5, "hud_canned_response_capable"

    const/4 v6, 0x0

    .line 144
    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 146
    .local v0, "isCapableOfCannedResponses":Z
    if-nez v0, :cond_0

    .line 147
    sget-object v5, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "HUD requested canned messages but is not capable of it so ignoring request."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 162
    .end local v0    # "isCapableOfCannedResponses":Z
    .end local v1    # "sharedPrefs":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 150
    .restart local v0    # "isCapableOfCannedResponses":Z
    .restart local v1    # "sharedPrefs":Landroid/content/SharedPreferences;
    :cond_0
    const-string v5, "nav_serial_number"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 152
    .local v2, "serial":J
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$5;->val$cannedMessagesRequest:Lcom/navdy/service/library/events/glances/CannedMessagesRequest;

    iget-object v5, v5, Lcom/navdy/service/library/events/glances/CannedMessagesRequest;->serial_number:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-nez v5, :cond_1

    .line 153
    sget-object v5, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "CannedMessage prefs version up to date"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 154
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$5;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    new-instance v6, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    sget-object v7, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_VERSION_IS_CURRENT:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v8, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 158
    .end local v0    # "isCapableOfCannedResponses":Z
    .end local v1    # "sharedPrefs":Landroid/content/SharedPreferences;
    .end local v2    # "serial":J
    :catch_0
    move-exception v4

    .line 159
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 160
    new-instance v5, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    sget-object v6, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {v5, v6, v11, v7, v11}, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    invoke-static {v5}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0

    .line 156
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v0    # "isCapableOfCannedResponses":Z
    .restart local v1    # "sharedPrefs":Landroid/content/SharedPreferences;
    .restart local v2    # "serial":J
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendMessagingSettingsToTheHudBasedOnSharedPrefValue()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
