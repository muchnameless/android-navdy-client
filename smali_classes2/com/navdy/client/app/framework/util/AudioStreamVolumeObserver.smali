.class public Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;
.super Ljava/lang/Object;
.source "AudioStreamVolumeObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;,
        Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;
    }
.end annotation


# static fields
.field private static logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mAudioStreamVolumeContentObserver:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mContext:Landroid/content/Context;

    .line 75
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public start(ILcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;)V
    .locals 6
    .param p1, "audioStreamType"    # I
    .param p2, "listener"    # Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->stop()V

    .line 80
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 81
    .local v1, "handler":Landroid/os/Handler;
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mContext:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 83
    .local v0, "audioManager":Landroid/media/AudioManager;
    new-instance v2, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;

    invoke-direct {v2, v1, v0, p1, p2}, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;-><init>(Landroid/os/Handler;Landroid/media/AudioManager;ILcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;)V

    iput-object v2, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mAudioStreamVolumeContentObserver:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;

    .line 88
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mAudioStreamVolumeContentObserver:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;

    .line 89
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 92
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mAudioStreamVolumeContentObserver:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;

    if-nez v0, :cond_0

    .line 102
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mAudioStreamVolumeContentObserver:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;

    .line 100
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->mAudioStreamVolumeContentObserver:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;

    goto :goto_0
.end method
