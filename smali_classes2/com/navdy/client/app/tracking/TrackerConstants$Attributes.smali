.class public Lcom/navdy/client/app/tracking/TrackerConstants$Attributes;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Attributes"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$MusicAttributes;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$HudVoiceSearchAttributes;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$InstallAttributes;
    }
.end annotation


# static fields
.field public static final CAR_MAKE:Ljava/lang/String; = "Car_Make"

.field public static final CAR_MANUAL_ENTRY:Ljava/lang/String; = "Vehicle_Data_Entry_Manual"

.field public static final CAR_MODEL:Ljava/lang/String; = "Car_Model"

.field public static final CAR_YEAR:Ljava/lang/String; = "Car_Year"

.field public static final CONTACTS:Ljava/lang/String; = "Contacts"

.field static final CURRENT_SCREEN:Ljava/lang/String; = "Current_Screen"

.field public static final DURING_FLE:Ljava/lang/String; = "During_Fle"

.field public static final FIRST_LAUNCH_ATTRIBUTE:Ljava/lang/String; = "First_Launch"

.field public static final HUD_SERIAL_NUMBER:Ljava/lang/String; = "Hud_Serial_Number"

.field public static final HUD_VERSION_NUMBER:Ljava/lang/String; = "Hud_Version_Number"

.field public static final LOCATION:Ljava/lang/String; = "Location"

.field public static final MICROPHONE:Ljava/lang/String; = "Microphone"

.field public static final MOBILE_APP_VERSION:Ljava/lang/String; = "Mobile_App_Version"

.field public static final PHONE:Ljava/lang/String; = "Phone"

.field public static final SMS:Ljava/lang/String; = "Sms"

.field public static final STORAGE:Ljava/lang/String; = "Storage"

.field public static final SUCCESS_ATTRIBUTE:Ljava/lang/String; = "Success"

.field public static final TYPE:Ljava/lang/String; = "Type"

.field public static final VIN:Ljava/lang/String; = "VIN"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
