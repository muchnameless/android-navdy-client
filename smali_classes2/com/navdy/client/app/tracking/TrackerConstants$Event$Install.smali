.class public Lcom/navdy/client/app/tracking/TrackerConstants$Event$Install;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Install"
.end annotation


# static fields
.field public static final ACTUALLY_I_ALREADY_HAVE_MED_TALL_MOUNT:Ljava/lang/String; = "Actually_I_Already_Have_Med_Tall_Mount"

.field public static final CONFIGURATION_AT_COMPLETION:Ljava/lang/String; = "Configuration_At_Completion"

.field public static final CONTINUE_WITH_UNRECOMMENDED_SHORT_MOUNT:Ljava/lang/String; = "Continue_With_Unrecommended_Short_Mount"

.field public static final CONTINUE_WITH_UNSUPPORTED_SHORT_MOUNT:Ljava/lang/String; = "Continue_With_Unsupported_Short_Mount"

.field public static final FIRST_LAUNCH_COMPLETED:Ljava/lang/String; = "First_Launch_Completed"

.field public static final MEDIUM_TALL_MOUNT_NOT_RECOMMENDED_CONTINUE:Ljava/lang/String; = "Medium_Tall_Mount_Not_Recommended_Continue"

.field public static final MOUNT_PICKER_SELECTION:Ljava/lang/String; = "Mount_Picker_Selection"

.field public static final PURCHASE_MOUNT_KIT:Ljava/lang/String; = "Purchase_Mount_Kit"

.field public static final SHORT_MOUNT_NOT_RECOMMENDED_CONTINUE:Ljava/lang/String; = "Short_Mount_Not_Recommended_Continue"

.field public static final USER_PICKED_MEDIUM_MOUNT:Ljava/lang/String; = "User_Picked_Medium_Mount"

.field public static final USER_PICKED_SHORT_MOUNT:Ljava/lang/String; = "User_Picked_Short_Mount"

.field public static final USER_PICKED_TALL_MOUNT:Ljava/lang/String; = "User_Picked_Tall_Mount"

.field public static final VIDEO_TAPPED_ON_INSTALL_COMPLETE:Ljava/lang/String; = "Video_Tapped_On_Install_Complete"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
