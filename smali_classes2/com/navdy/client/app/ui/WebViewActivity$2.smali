.class Lcom/navdy/client/app/ui/WebViewActivity$2;
.super Landroid/webkit/WebViewClient;
.source "WebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/WebViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/WebViewActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/WebViewActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/WebViewActivity;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/navdy/client/app/ui/WebViewActivity$2;->this$0:Lcom/navdy/client/app/ui/WebViewActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4
    .param p1, "finishedWebView"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 92
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/WebViewActivity$2;->this$0:Lcom/navdy/client/app/ui/WebViewActivity;

    invoke-virtual {v1, p1}, Lcom/navdy/client/app/ui/WebViewActivity;->injectMobileStyleForZendeskPage(Landroid/webkit/WebView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 97
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/navdy/client/app/ui/WebViewActivity$2;->this$0:Lcom/navdy/client/app/ui/WebViewActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/WebViewActivity;->access$000(Lcom/navdy/client/app/ui/WebViewActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
