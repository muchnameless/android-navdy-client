.class public Lcom/navdy/client/app/ui/base/BaseFragment;
.super Landroid/app/Fragment;
.source "BaseFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/SimpleDialogFragment$DialogProvider;


# static fields
.field private static final FRAGMENT_DIALOG:Ljava/lang/String; = "DIALOG"


# instance fields
.field protected baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

.field protected final handler:Landroid/os/Handler;

.field protected final logger:Lcom/navdy/service/library/log/Logger;

.field private volatile mIsInForeground:Z

.field private progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->logger:Lcom/navdy/service/library/log/Logger;

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->handler:Landroid/os/Handler;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->mIsInForeground:Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->progressDialog:Landroid/app/ProgressDialog;

    return-void
.end method


# virtual methods
.method public createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "id"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 112
    if-eqz p2, :cond_3

    const-string v4, "title"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    const-string v4, "message"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 113
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 114
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 115
    const-string v4, "title"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "title":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 117
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 118
    :cond_1
    const-string v4, "message"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "message":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 120
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 122
    :cond_2
    const v4, 0x7f08031e

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/base/BaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 123
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 125
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_3
    return-object v3
.end method

.method public dismissProgressDialog()V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->dismissProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V

    .line 147
    return-void
.end method

.method public hideProgressDialog()V
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->hideProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V

    .line 140
    return-void
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isActivityDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInForeground()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->mIsInForeground:Z

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onAttach"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 61
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 62
    instance-of v0, p1, Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v0, :cond_0

    .line 63
    check-cast p1, Lcom/navdy/client/app/ui/base/BaseActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    .line 65
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onCreate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 34
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 35
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onDestroy"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->dismissProgressDialog()V

    .line 78
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 79
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onDetach"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    .line 71
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 72
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 40
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->mIsInForeground:Z

    .line 42
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->hideProgressDialog()V

    .line 43
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 44
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 49
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->mIsInForeground:Z

    .line 51
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 52
    return-void
.end method

.method public removeDialog()V
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->isInForeground()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 101
    .local v1, "manager":Landroid/app/FragmentManager;
    if-eqz v1, :cond_0

    .line 102
    const-string v2, "DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 103
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 108
    .end local v0    # "fragment":Landroid/app/Fragment;
    .end local v1    # "manager":Landroid/app/FragmentManager;
    :cond_0
    return-void
.end method

.method public showProgressDialog()V
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseFragment;->progressDialog:Landroid/app/ProgressDialog;

    .line 133
    return-void
.end method

.method public showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->removeDialog()V

    .line 88
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 89
    .local v2, "manager":Landroid/app/FragmentManager;
    if-eqz v2, :cond_0

    .line 90
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 91
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "title"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v3, "message"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-static {p1, v0}, Lcom/navdy/client/app/ui/SimpleDialogFragment;->newInstance(ILandroid/os/Bundle;)Lcom/navdy/client/app/ui/SimpleDialogFragment;

    move-result-object v1

    .line 94
    .local v1, "dialogFragment":Lcom/navdy/client/app/ui/SimpleDialogFragment;
    const-string v3, "DIALOG"

    invoke-virtual {v1, v2, v3}, Lcom/navdy/client/app/ui/SimpleDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 96
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "dialogFragment":Lcom/navdy/client/app/ui/SimpleDialogFragment;
    :cond_0
    return-void
.end method
