.class public Lcom/navdy/client/app/ui/base/BaseSupportFragment;
.super Landroid/support/v4/app/Fragment;
.source "BaseSupportFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/SimpleDialogFragment$DialogProvider;


# instance fields
.field protected baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

.field protected final handler:Landroid/os/Handler;

.field protected final logger:Lcom/navdy/service/library/log/Logger;

.field private volatile mIsInForeground:Z

.field private progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 24
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->logger:Lcom/navdy/service/library/log/Logger;

    .line 25
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->handler:Landroid/os/Handler;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->mIsInForeground:Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->progressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method public static hideThisFragment(Landroid/app/Fragment;Landroid/support/v4/app/FragmentActivity;)V
    .locals 3
    .param p0, "fragment"    # Landroid/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p1, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 158
    invoke-static {p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->isEnding(Landroid/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    invoke-virtual {p0}, Landroid/app/Fragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const/high16 v1, 0x10b0000

    const v2, 0x10b0001

    .line 164
    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 165
    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 168
    :cond_0
    return-void
.end method

.method private hideThisFragmentImmediately(Landroid/app/Fragment;Landroid/support/v4/app/FragmentActivity;)V
    .locals 1
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p2, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 172
    invoke-static {p2}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    invoke-static {p1}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->isEnding(Landroid/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    invoke-virtual {p1}, Landroid/app/Fragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 178
    invoke-virtual {v0, p1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 181
    :cond_0
    return-void
.end method

.method public static isEnding(Landroid/app/Fragment;)Z
    .locals 1
    .param p0, "fragment"    # Landroid/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 184
    if-eqz p0, :cond_0

    .line 185
    invoke-virtual {p0}, Landroid/app/Fragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    invoke-virtual {p0}, Landroid/app/Fragment;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEnding(Landroid/support/v4/app/Fragment;)Z
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 190
    if-eqz p0, :cond_0

    .line 191
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static showThisFragment(Landroid/app/Fragment;Landroid/support/v4/app/FragmentActivity;)V
    .locals 3
    .param p0, "fragment"    # Landroid/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p1, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 144
    invoke-static {p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->isEnding(Landroid/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    invoke-virtual {p0}, Landroid/app/Fragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const/high16 v1, 0x10b0000

    const v2, 0x10b0001

    .line 150
    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 151
    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 154
    :cond_0
    return-void
.end method


# virtual methods
.method public createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "id"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public dismissProgressDialog()V
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->dismissProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V

    .line 121
    return-void
.end method

.method protected getScreenSize()Landroid/graphics/Point;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 92
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getScreenSize(Landroid/support/v4/app/FragmentActivity;)Landroid/graphics/Point;

    move-result-object v1

    return-object v1
.end method

.method public hideProgressDialog()V
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->hideProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)V

    .line 114
    return-void
.end method

.method public hideThisFragment(Landroid/app/Fragment;)V
    .locals 1
    .param p1, "fragment"    # Landroid/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 132
    .local v0, "fragmentActivity":Landroid/support/v4/app/FragmentActivity;
    invoke-static {p1, v0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->hideThisFragment(Landroid/app/Fragment;Landroid/support/v4/app/FragmentActivity;)V

    .line 133
    return-void
.end method

.method public hideThisFragmentImmediately(Landroid/app/Fragment;)V
    .locals 1
    .param p1, "fragment"    # Landroid/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 138
    .local v0, "fragmentActivity":Landroid/support/v4/app/FragmentActivity;
    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->hideThisFragmentImmediately(Landroid/app/Fragment;Landroid/support/v4/app/FragmentActivity;)V

    .line 139
    return-void
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isActivityDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInForeground()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->mIsInForeground:Z

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onAttach"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 59
    instance-of v0, p1, Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v0, :cond_0

    .line 60
    check-cast p1, Lcom/navdy/client/app/ui/base/BaseActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    .line 62
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onCreate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 35
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onDestroy"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->dismissProgressDialog()V

    .line 75
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 76
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onDetach"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    .line 68
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 69
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 41
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->mIsInForeground:Z

    .line 43
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->hideProgressDialog()V

    .line 44
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 50
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->mIsInForeground:Z

    .line 52
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public showProgressDialog()V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showProgressDialog(Landroid/app/Activity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->progressDialog:Landroid/app/ProgressDialog;

    .line 107
    return-void
.end method

.method public showThisFragment(Landroid/app/Fragment;)V
    .locals 1
    .param p1, "fragment"    # Landroid/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 126
    .local v0, "fragmentActivity":Landroid/support/v4/app/FragmentActivity;
    invoke-static {p1, v0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->showThisFragment(Landroid/app/Fragment;Landroid/support/v4/app/FragmentActivity;)V

    .line 127
    return-void
.end method
