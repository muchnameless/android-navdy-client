.class public final enum Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;
.super Ljava/lang/Enum;
.source "AppSetupScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScreenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum ACCESS_FINE_LOCATION:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum BLUETOOTH_SUCCESS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum CALL_PHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum END:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum NOTIFICATIONS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum PROFILE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum READ_CALENDAR:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum READ_CONTACTS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum RECEIVE_SMS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum USE_MICROPHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

.field public static final enum WRITE_EXTERNAL_STORAGE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;


# instance fields
.field type:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 111
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 112
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v5, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->PROFILE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 113
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "BLUETOOTH"

    invoke-direct {v0, v1, v6, v5}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 114
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "BLUETOOTH_SUCCESS"

    invoke-direct {v0, v1, v7, v6}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH_SUCCESS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 115
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "NOTIFICATIONS"

    invoke-direct {v0, v1, v8, v7}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->NOTIFICATIONS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 116
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "ACCESS_FINE_LOCATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->ACCESS_FINE_LOCATION:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 117
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "USE_MICROPHONE"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->USE_MICROPHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 118
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "READ_CONTACTS"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CONTACTS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 119
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "RECEIVE_SMS"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->RECEIVE_SMS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 120
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "CALL_PHONE"

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->CALL_PHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 121
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "READ_CALENDAR"

    const/16 v2, 0xa

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CALENDAR:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 122
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "WRITE_EXTERNAL_STORAGE"

    const/16 v2, 0xb

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->WRITE_EXTERNAL_STORAGE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 123
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const-string v1, "END"

    const/16 v2, 0xc

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->END:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    .line 110
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->PROFILE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH_SUCCESS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->NOTIFICATIONS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->ACCESS_FINE_LOCATION:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->USE_MICROPHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CONTACTS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->RECEIVE_SMS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->CALL_PHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CALENDAR:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->WRITE_EXTERNAL_STORAGE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->END:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->$VALUES:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 128
    iput p3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->type:I

    .line 129
    return-void
.end method

.method public static fromValue(I)Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 132
    packed-switch p0, :pswitch_data_0

    .line 156
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->UNKNOWN:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    :goto_0
    return-object v0

    .line 134
    :pswitch_0
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->PROFILE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 136
    :pswitch_1
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 138
    :pswitch_2
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH_SUCCESS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 140
    :pswitch_3
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->NOTIFICATIONS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 142
    :pswitch_4
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->ACCESS_FINE_LOCATION:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 144
    :pswitch_5
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CONTACTS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 146
    :pswitch_6
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->RECEIVE_SMS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 148
    :pswitch_7
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->CALL_PHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 150
    :pswitch_8
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CALENDAR:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 152
    :pswitch_9
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->WRITE_EXTERNAL_STORAGE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 154
    :pswitch_a
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->END:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 110
    const-class v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->$VALUES:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->type:I

    return v0
.end method
