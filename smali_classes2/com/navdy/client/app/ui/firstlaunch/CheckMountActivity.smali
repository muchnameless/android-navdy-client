.class public Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "CheckMountActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->hideSystemUI()V

    .line 34
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "box"

    const-string v3, "Old_Box"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "box":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->showProgressDialog()V

    .line 36
    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;Ljava/lang/String;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    .line 68
    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 69
    return-void
.end method
