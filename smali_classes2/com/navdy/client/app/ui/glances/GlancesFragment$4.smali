.class Lcom/navdy/client/app/ui/glances/GlancesFragment$4;
.super Ljava/lang/Object;
.source "GlancesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/glances/GlancesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 220
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 224
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$302(Lcom/navdy/client/app/ui/glances/GlancesFragment;Z)Z

    .line 225
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0, v2}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$402(Lcom/navdy/client/app/ui/glances/GlancesFragment;Z)Z

    .line 239
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$500(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Landroid/widget/RadioButton;

    move-result-object v3

    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$300(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$400(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 240
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$600(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Landroid/widget/RadioButton;

    move-result-object v3

    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$300(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$400(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 241
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$700(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Landroid/widget/RadioButton;

    move-result-object v0

    iget-object v3, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$300(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$400(Lcom/navdy/client/app/ui/glances/GlancesFragment;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 243
    new-instance v0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/glances/GlancesFragment$4$1;-><init>(Lcom/navdy/client/app/ui/glances/GlancesFragment$4;)V

    new-array v1, v2, [Ljava/lang/Void;

    .line 253
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/glances/GlancesFragment$4$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 254
    return-void

    .line 229
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0, v2}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$302(Lcom/navdy/client/app/ui/glances/GlancesFragment;Z)Z

    .line 230
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$402(Lcom/navdy/client/app/ui/glances/GlancesFragment;Z)Z

    goto :goto_0

    .line 234
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$302(Lcom/navdy/client/app/ui/glances/GlancesFragment;Z)Z

    .line 235
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$4;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$402(Lcom/navdy/client/app/ui/glances/GlancesFragment;Z)Z

    goto :goto_0

    :cond_0
    move v0, v2

    .line 239
    goto :goto_1

    :cond_1
    move v0, v2

    .line 240
    goto :goto_2

    :cond_2
    move v1, v2

    .line 241
    goto :goto_3

    .line 220
    nop

    :pswitch_data_0
    .packed-switch 0x7f1002c2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
