.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->searchOrRouteToSharedText(Ljava/lang/String;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$sharedText:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 1391
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1394
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1395
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1600(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "shared text is empty"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1448
    :goto_0
    return-void

    .line 1398
    :cond_0
    const-string v2, ""

    .line 1399
    .local v2, "displayName":Ljava/lang/String;
    const-string v0, ""

    .line 1401
    .local v0, "address":Ljava/lang/String;
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "https://goo.gl/maps/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1403
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "to"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "via"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1405
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "to"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v4, v8, 0x2

    .line 1406
    .local v4, "indexOfDisplayName":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "via"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 1407
    .local v6, "indexOfVia":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    invoke-virtual {v8, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1424
    .end local v4    # "indexOfDisplayName":I
    .end local v6    # "indexOfVia":I
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1700(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "display name: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1426
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 1427
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 1428
    .local v1, "d":Lcom/navdy/client/app/framework/models/Destination;
    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/models/Destination;->setName(Ljava/lang/String;)V

    .line 1429
    invoke-virtual {v1, v0}, Lcom/navdy/client/app/framework/models/Destination;->setRawAddressNotForDisplay(Ljava/lang/String;)V

    .line 1430
    invoke-static {v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v3

    .line 1432
    .local v3, "finalDestination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v8, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17$1;

    invoke-direct {v8, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17$1;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;)V

    invoke-static {v3, v8}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto :goto_0

    .line 1409
    .end local v1    # "d":Lcom/navdy/client/app/framework/models/Destination;
    .end local v3    # "finalDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_2
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$intent:Landroid/content/Intent;

    const-string v9, "android.intent.extra.SUBJECT"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1411
    :cond_3
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "https://g.co"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1415
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "https://g.co"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1416
    .local v5, "indexOfShortFormUrl":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    invoke-virtual {v8, v10, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1417
    goto :goto_1

    .end local v5    # "indexOfShortFormUrl":I
    :cond_4
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "https://www.google.com/search"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1418
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "https://www.google.com/search"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1419
    .restart local v5    # "indexOfShortFormUrl":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    invoke-virtual {v8, v10, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1420
    goto :goto_1

    .end local v5    # "indexOfShortFormUrl":I
    :cond_5
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "https://www.yelp.com/biz/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1421
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    const-string v9, "https://www.yelp.com/biz/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 1422
    .local v7, "urlIndex":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->val$sharedText:Ljava/lang/String;

    invoke-virtual {v8, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1443
    .end local v7    # "urlIndex":I
    :cond_6
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 1444
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v8, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToFirstResultFor(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1446
    :cond_7
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$2000(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "display name and address are both empty so we can\'t navigate anywhere"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
