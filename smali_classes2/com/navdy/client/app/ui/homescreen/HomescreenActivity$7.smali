.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->saveDestinationToFavorites(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

.field final synthetic val$finalSpecialType:I


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 725
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iput p2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->val$finalSpecialType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updateOrSaveFavoriteToDbAsync(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 738
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->val$finalSpecialType:I

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7$1;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;)V

    invoke-virtual {p1, v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->saveDestinationAsFavoritesAsync(ILcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V

    .line 749
    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 733
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$600(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "navigationHelper failed to get latLng for ContactModel Destination"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 734
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->updateOrSaveFavoriteToDbAsync(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 735
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 728
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->updateOrSaveFavoriteToDbAsync(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 729
    return-void
.end method
