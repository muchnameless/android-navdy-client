.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$10;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->clearMapObjects()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 737
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$10;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 747
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 2
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 741
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$10;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0, p1, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$700(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 742
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$10;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0, p1, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$900(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V

    .line 743
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$10;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0, p1, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$300(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V

    .line 744
    return-void
.end method
