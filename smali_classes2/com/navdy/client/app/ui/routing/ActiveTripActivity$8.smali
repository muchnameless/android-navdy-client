.class Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;
.super Ljava/lang/Object;
.source "ActiveTripActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->startMapRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

.field final synthetic val$route:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 323
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->val$route:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 345
    return-void
.end method

.method public onReady(Lcom/here/android/mpa/mapping/Map;)V
    .locals 4
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 326
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->val$route:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 327
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getHereMapMarker()Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$202(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 329
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$200(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 330
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$200(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 333
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->val$route:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getRoute()Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v1

    .line 335
    .local v1, "geoPolyline":Lcom/here/android/mpa/common/GeoPolyline;
    if-eqz v1, :cond_1

    .line 336
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v1}, Lcom/navdy/client/app/framework/map/MapUtils;->generateRoutePolyline(Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$502(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    .line 337
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$500(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 338
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$300(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnRoute(Lcom/here/android/mpa/common/GeoPolyline;)V

    .line 342
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$8;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$502(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    goto :goto_0
.end method
