.class Lcom/navdy/client/app/ui/search/SearchActivity$12;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->runTextSearchOrAskHud(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 763
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$12;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchCompleted(Lcom/navdy/client/app/framework/search/SearchResults;)V
    .locals 2
    .param p1, "searchResults"    # Lcom/navdy/client/app/framework/search/SearchResults;

    .prologue
    .line 770
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$12;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$3000(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/search/SearchResults;)V

    .line 771
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$12;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$3100(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/search/SearchResults;)V

    .line 772
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$12;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$3200(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/search/SearchResults;Z)V

    .line 773
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$12;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->hideProgressDialog()V

    .line 774
    return-void
.end method

.method public onSearchStarted()V
    .locals 0

    .prologue
    .line 766
    return-void
.end method
