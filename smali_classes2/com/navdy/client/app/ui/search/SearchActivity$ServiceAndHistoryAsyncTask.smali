.class Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;
.super Landroid/os/AsyncTask;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceAndHistoryAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParams;TProgress;TResult;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;


# direct methods
.method private constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0

    .prologue
    .line 372
    .local p0, "this":Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;, "Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask<TParams;TProgress;TResult;>;"
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/ui/search/SearchActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p2, "x1"    # Lcom/navdy/client/app/ui/search/SearchActivity$1;

    .prologue
    .line 372
    .local p0, "this":Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;, "Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask<TParams;TProgress;TResult;>;"
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    return-void
.end method


# virtual methods
.method protected doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation

    .prologue
    .line 380
    .local p0, "this":Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;, "Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask<TParams;TProgress;TResult;>;"
    .local p1, "params":[Ljava/lang/Object;, "[TParams;"
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1000(Lcom/navdy/client/app/ui/search/SearchActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setUpServicesAndHistory(Z)V

    .line 381
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 386
    .local p0, "this":Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;, "Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask<TParams;TProgress;TResult;>;"
    .local p1, "result":Ljava/lang/Object;, "TResult;"
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    .line 387
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 375
    .local p0, "this":Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;, "Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask<TParams;TProgress;TResult;>;"
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->clear()V

    .line 376
    return-void
.end method
