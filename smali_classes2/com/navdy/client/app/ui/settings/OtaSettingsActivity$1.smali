.class Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;
.super Ljava/lang/Object;
.source "OtaSettingsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->onErrorCheckingForUpdate(Lcom/navdy/client/ota/OTAUpdateUIClient$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

.field final synthetic val$error:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Lcom/navdy/client/ota/OTAUpdateUIClient$Error;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->val$error:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 256
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->isInForeground()Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$100(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$200(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 260
    const/4 v0, 0x0

    .line 261
    .local v0, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f080160

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 262
    .local v1, "title":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error:[I

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->val$error:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    invoke-virtual {v3}, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 274
    :goto_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :pswitch_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/SystemUtils;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 265
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f0800b4

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 267
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f0802fe

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 269
    goto :goto_1

    .line 271
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f080404

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
