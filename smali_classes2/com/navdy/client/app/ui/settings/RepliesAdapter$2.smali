.class Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;
.super Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;
.source "RepliesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/settings/RepliesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-direct {p0}, Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public getMovementFlags(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$ViewHolder;)I
    .locals 4
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 150
    const/4 v0, 0x3

    .line 151
    .local v0, "dragFlags":I
    const/16 v1, 0x30

    .line 152
    .local v1, "swipeFlags":I
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 153
    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->makeMovementFlags(II)I

    move-result v2

    .line 155
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isItemViewSwipeEnabled()Z
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x0

    return v0
.end method

.method public isLongPressDragEnabled()Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x1

    return v0
.end method

.method public onMove(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 6
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p3, "target"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 160
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v4

    if-ne v4, v2, :cond_0

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->access$100(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/util/ArrayList;

    move-result-object v4

    if-nez v4, :cond_2

    :cond_0
    move v2, v3

    .line 183
    :cond_1
    :goto_0
    return v2

    .line 164
    :cond_2
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 165
    .local v0, "fromPos":I
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .line 167
    .local v1, "toPos":I
    if-ltz v1, :cond_3

    if-ltz v0, :cond_3

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->access$100(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->access$100(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_4

    :cond_3
    move v2, v3

    .line 169
    goto :goto_0

    .line 172
    :cond_4
    if-eq v0, v1, :cond_1

    .line 177
    if-lt v1, v0, :cond_5

    .line 178
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-static {v3}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->access$100(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/util/ArrayList;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v3

    const/4 v4, -0x1

    invoke-static {v3, v4}, Ljava/util/Collections;->rotate(Ljava/util/List;I)V

    .line 182
    :goto_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    add-int/lit8 v4, v0, 0x1

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->notifyItemMoved(II)V

    goto :goto_0

    .line 180
    :cond_5
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-static {v3}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->access$100(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/util/ArrayList;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v1, v4}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v2}, Ljava/util/Collections;->rotate(Ljava/util/List;I)V

    goto :goto_1
.end method

.method public onMoved(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$ViewHolder;ILandroid/support/v7/widget/RecyclerView$ViewHolder;III)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p3, "fromPos"    # I
    .param p4, "target"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p5, "toPos"    # I
    .param p6, "x"    # I
    .param p7, "y"    # I

    .prologue
    .line 194
    invoke-super/range {p0 .. p7}, Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;->onMoved(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$ViewHolder;ILandroid/support/v7/widget/RecyclerView$ViewHolder;III)V

    .line 195
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->access$200(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/RepliesAdapter$2;->this$0:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->access$200(Lcom/navdy/client/app/ui/settings/RepliesAdapter;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 198
    :cond_0
    return-void
.end method

.method public onSwiped(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "direction"    # I

    .prologue
    .line 204
    return-void
.end method
