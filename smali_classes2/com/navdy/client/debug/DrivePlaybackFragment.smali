.class public Lcom/navdy/client/debug/DrivePlaybackFragment;
.super Landroid/app/ListFragment;
.source "DrivePlaybackFragment.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mPlaybackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPlaybackListAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRecordingListener:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;

.field private final mRecordingServiceManager:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/CustomNotificationFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/DrivePlaybackFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 35
    new-instance v0, Lcom/navdy/client/debug/DrivePlaybackFragment$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/DrivePlaybackFragment$1;-><init>(Lcom/navdy/client/debug/DrivePlaybackFragment;)V

    iput-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mRecordingListener:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;

    .line 44
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->getInstance()Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mRecordingServiceManager:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    .line 45
    iget-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mRecordingServiceManager:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mRecordingListener:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager$Listener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->addListener(Ljava/lang/ref/WeakReference;)V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/DrivePlaybackFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/DrivePlaybackFragment;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mPlaybackList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/DrivePlaybackFragment;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/DrivePlaybackFragment;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mPlaybackListAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/DrivePlaybackFragment;)Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/DrivePlaybackFragment;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mRecordingServiceManager:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mPlaybackList:Ljava/util/List;

    .line 53
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/DrivePlaybackFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1090016

    const v3, 0x1020014

    iget-object v4, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mPlaybackList:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mPlaybackListAdapter:Landroid/widget/ArrayAdapter;

    .line 55
    iget-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mPlaybackListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DrivePlaybackFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 57
    iget-object v0, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mRecordingServiceManager:Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->sendDriveRecordingsRequest()V

    .line 58
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    const v1, 0x7f03009c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 64
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 65
    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 70
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 71
    const/4 v3, 0x0

    invoke-virtual {p1, p3, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 72
    iget-object v3, p0, Lcom/navdy/client/debug/DrivePlaybackFragment;->mPlaybackList:Ljava/util/List;

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 74
    .local v2, "clickedItem":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/navdy/client/debug/DrivePlaybackFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 77
    .local v1, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "Play"

    new-instance v5, Lcom/navdy/client/debug/DrivePlaybackFragment$3;

    invoke-direct {v5, p0, v2}, Lcom/navdy/client/debug/DrivePlaybackFragment$3;-><init>(Lcom/navdy/client/debug/DrivePlaybackFragment;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "Cancel"

    new-instance v5, Lcom/navdy/client/debug/DrivePlaybackFragment$2;

    invoke-direct {v5, p0}, Lcom/navdy/client/debug/DrivePlaybackFragment$2;-><init>(Lcom/navdy/client/debug/DrivePlaybackFragment;)V

    .line 84
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 92
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 95
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 96
    return-void
.end method
