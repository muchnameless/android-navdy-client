.class public Lcom/navdy/client/debug/MockedAddressPickerFragment;
.super Lcom/navdy/client/debug/RemoteAddressPickerFragment;
.source "MockedAddressPickerFragment.java"


# instance fields
.field private appInstance:Lcom/navdy/client/app/framework/AppInstance;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const v0, 0x7f0804b6

    sput v0, Lcom/navdy/client/debug/MockedAddressPickerFragment;->fragmentTitle:I

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/MockedAddressPickerFragment;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 28
    return-void
.end method

.method protected processSelectedLocation(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "location"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "locationLabel"    # Ljava/lang/String;
    .param p3, "streetAddress"    # Ljava/lang/String;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/client/debug/MockedAddressPickerFragment;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current location mocked to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;Z)V

    .line 33
    invoke-virtual {p0}, Lcom/navdy/client/debug/MockedAddressPickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 34
    return-void
.end method
