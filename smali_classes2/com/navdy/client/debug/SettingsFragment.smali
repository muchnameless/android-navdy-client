.class public Lcom/navdy/client/debug/SettingsFragment;
.super Landroid/app/ListFragment;
.source "SettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/SettingsFragment$ListItem;
    }
.end annotation


# instance fields
.field private mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

.field private mListItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/debug/SettingsFragment$ListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 60
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {}, Lcom/navdy/client/debug/SettingsFragment$ListItem;->values()[Lcom/navdy/client/debug/SettingsFragment$ListItem;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/navdy/client/debug/SettingsFragment;->mListItems:Ljava/util/List;

    .line 67
    iget-object v1, p0, Lcom/navdy/client/debug/SettingsFragment;->mListItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;

    .line 68
    .local v0, "item":Lcom/navdy/client/debug/SettingsFragment$ListItem;
    invoke-virtual {p0}, Lcom/navdy/client/debug/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/client/debug/SettingsFragment$ListItem;->bindResource(Landroid/content/res/Resources;)V

    goto :goto_0

    .line 71
    .end local v0    # "item":Lcom/navdy/client/debug/SettingsFragment$ListItem;
    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x1090016

    const v4, 0x1020014

    .line 72
    invoke-static {}, Lcom/navdy/client/debug/SettingsFragment$ListItem;->values()[Lcom/navdy/client/debug/SettingsFragment$ListItem;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 71
    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/SettingsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    const v1, 0x7f03009a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 81
    new-instance v1, Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-direct {v1, v0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/navdy/client/debug/SettingsFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    .line 82
    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 101
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 103
    const/4 v2, 0x0

    invoke-virtual {p1, p3, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 105
    iget-object v2, p0, Lcom/navdy/client/debug/SettingsFragment;->mListItems:Ljava/util/List;

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;

    .line 108
    .local v0, "clickedItem":Lcom/navdy/client/debug/SettingsFragment$ListItem;
    sget-object v2, Lcom/navdy/client/debug/SettingsFragment$1;->$SwitchMap$com$navdy$client$debug$SettingsFragment$ListItem:[I

    invoke-virtual {v0}, Lcom/navdy/client/debug/SettingsFragment$ListItem;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 125
    :goto_0
    return-void

    .line 111
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/debug/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/settings/BluetoothPairActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "1"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 116
    .end local v1    # "i":Landroid/content/Intent;
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/client/debug/SettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/navdy/client/debug/HudSettingsFragment;

    invoke-static {v2, v3}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    goto :goto_0

    .line 119
    :pswitch_2
    invoke-virtual {p0}, Lcom/navdy/client/debug/SettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/navdy/client/debug/ScreenConfigFragment;

    invoke-static {v2, v3}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    goto :goto_0

    .line 122
    :pswitch_3
    invoke-virtual {p0}, Lcom/navdy/client/debug/SettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/navdy/client/debug/AboutFragment;

    invoke-static {v2, v3}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/client/debug/SettingsFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-virtual {v0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->onPause()V

    .line 96
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 97
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 90
    iget-object v0, p0, Lcom/navdy/client/debug/SettingsFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-virtual {v0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->onResume()V

    .line 91
    return-void
.end method
