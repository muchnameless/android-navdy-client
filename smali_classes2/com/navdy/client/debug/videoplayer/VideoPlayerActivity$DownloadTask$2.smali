.class Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$2;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Lcom/amazonaws/event/ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$2;->this$1:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public progressChanged(Lcom/amazonaws/event/ProgressEvent;)V
    .locals 4
    .param p1, "progressEvent"    # Lcom/amazonaws/event/ProgressEvent;

    .prologue
    .line 227
    invoke-static {}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Download event "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 228
    invoke-virtual {p1}, Lcom/amazonaws/event/ProgressEvent;->getEventCode()I

    move-result v0

    .line 229
    .local v0, "eventCode":I
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 230
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$2;->this$1:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    iget-object v1, v1, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    new-instance v2, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$2$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$2$1;-><init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$2;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 237
    invoke-static {}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Failed to download db"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
