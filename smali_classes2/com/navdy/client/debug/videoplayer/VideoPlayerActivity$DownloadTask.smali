.class Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;
.super Landroid/os/AsyncTask;
.source "VideoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;
    .param p2, "x1"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$1;

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;-><init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 182
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 196
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v5}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$100(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "db.sqlite3"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    new-instance v5, Ljava/io/File;

    sget-object v6, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    .line 198
    invoke-static {v6}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 197
    invoke-static {v4, v5}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$202(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Ljava/io/File;)Ljava/io/File;

    .line 202
    invoke-static {}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Downloading s3 object ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v6}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$200(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 204
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v4}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$200(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    .line 205
    .local v3, "parentFile":Ljava/io/File;
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    move-result v2

    .line 206
    .local v2, "parentDirCreated":Z
    if-nez v2, :cond_0

    .line 207
    invoke-static {}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to create directory for db at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v6}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$200(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 248
    :goto_0
    return-object v8

    .line 211
    :cond_0
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v4}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$200(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 213
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    new-instance v5, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$1;

    invoke-direct {v5, p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$1;-><init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;)V

    invoke-virtual {v4, v5}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 222
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    iget-object v5, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v5}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$700(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v6}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$600(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v7}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$200(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v5, v6, v1, v7}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;->download(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$502(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    .line 224
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v4}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$500(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    move-result-object v4

    new-instance v5, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$2;

    invoke-direct {v5, p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask$2;-><init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;)V

    invoke-interface {v4, v5}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;->addProgressListener(Lcom/amazonaws/event/ProgressListener;)V

    .line 243
    :try_start_0
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v4}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$500(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    move-result-object v4

    invoke-interface {v4}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;->waitForCompletion()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :goto_1
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v4, v8}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$802(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;)Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Download interrupted "

    invoke-virtual {v4, v5, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 182
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    iget-object v0, v0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadStatus:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 192
    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 186
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    iget-object v0, v0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadStatus:Landroid/widget/TextView;

    const-string v1, "Downloading %s%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->this$0:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-static {v4}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->access$100(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "db.sqlite3"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    return-void
.end method
