.class Lcom/navdy/client/ota/OTAUpdateService$1;
.super Ljava/lang/Object;
.source "OTAUpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/ota/OTAUpdateService;->checkForUpdate()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/ota/OTAUpdateService;


# direct methods
.method constructor <init>(Lcom/navdy/client/ota/OTAUpdateService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/navdy/client/ota/OTAUpdateService$1;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 141
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 143
    .local v3, "preferences":Landroid/content/SharedPreferences;
    const-string v7, "DEVICE_ID"

    const/4 v8, 0x0

    invoke-interface {v3, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 144
    .local v2, "lastConnectedDeviceId":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 145
    :cond_0
    iget-object v7, p0, Lcom/navdy/client/ota/OTAUpdateService$1;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v7}, Lcom/navdy/client/ota/OTAUpdateService;->access$000(Lcom/navdy/client/ota/OTAUpdateService;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 146
    iget-object v7, p0, Lcom/navdy/client/ota/OTAUpdateService$1;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    sget-object v8, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-static {v7, v8}, Lcom/navdy/client/ota/OTAUpdateService;->access$100(Lcom/navdy/client/ota/OTAUpdateService;Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 147
    iget-object v7, p0, Lcom/navdy/client/ota/OTAUpdateService$1;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-virtual {v7}, Lcom/navdy/client/ota/OTAUpdateService;->removeNotification()V

    .line 172
    :goto_0
    return-void

    .line 150
    :cond_1
    invoke-static {v2}, Lcom/navdy/client/ota/OTAUpdateService;->access$200(Ljava/lang/String;)I

    move-result v0

    .line 152
    .local v0, "currentSwVersion":I
    invoke-static {v2}, Lcom/navdy/client/ota/OTAUpdateService;->access$300(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "FORCE_FULL_UPDATE"

    .line 153
    invoke-interface {v3, v7, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    const/4 v1, 0x1

    .line 155
    .local v1, "forceFullUpdate":Z
    :cond_3
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Device needs full update? :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 158
    const-string v7, "OTA_STATUS"

    sget-object v8, Lcom/navdy/client/app/ui/settings/SettingsConstants;->OTA_STATUS_DEFAULT:Ljava/lang/String;

    invoke-interface {v3, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 159
    .local v5, "stateString":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 160
    invoke-static {v5}, Lcom/navdy/client/ota/OTAUpdateService$State;->valueOf(Ljava/lang/String;)Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v4

    .line 161
    .local v4, "state":Lcom/navdy/client/ota/OTAUpdateService$State;
    sget-object v7, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-eq v4, v7, :cond_4

    .line 163
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bReadUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v6

    .line 164
    .local v6, "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    iget-boolean v7, v6, Lcom/navdy/client/ota/model/UpdateInfo;->incremental:Z

    if-eqz v7, :cond_4

    iget v7, v6, Lcom/navdy/client/ota/model/UpdateInfo;->fromVersion:I

    if-eq v7, v0, :cond_4

    .line 166
    iget-object v7, p0, Lcom/navdy/client/ota/OTAUpdateService$1;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    sget-object v8, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-static {v7, v8}, Lcom/navdy/client/ota/OTAUpdateService;->access$100(Lcom/navdy/client/ota/OTAUpdateService;Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 170
    .end local v4    # "state":Lcom/navdy/client/ota/OTAUpdateService$State;
    .end local v6    # "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    :cond_4
    iget-object v7, p0, Lcom/navdy/client/ota/OTAUpdateService$1;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v7}, Lcom/navdy/client/ota/OTAUpdateService;->access$500(Lcom/navdy/client/ota/OTAUpdateService;)V

    .line 171
    iget-object v7, p0, Lcom/navdy/client/ota/OTAUpdateService$1;->this$0:Lcom/navdy/client/ota/OTAUpdateService;

    invoke-static {v7}, Lcom/navdy/client/ota/OTAUpdateService;->access$600(Lcom/navdy/client/ota/OTAUpdateService;)Lcom/navdy/client/ota/OTAUpdateManager;

    move-result-object v7

    invoke-interface {v7, v0, v1}, Lcom/navdy/client/ota/OTAUpdateManager;->checkForUpdate(IZ)V

    goto :goto_0
.end method
