.class Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$3;
.super Ljava/lang/Object;
.source "OTAUpdateManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->onError(ILjava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

.field final synthetic val$id:I


# direct methods
.method constructor <init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;I)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$3;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iput p2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$3;->val$id:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 320
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$3;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v1, v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v1

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    iget v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$3;->val$id:I

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$3;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v6, v6, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->val$info:Lcom/navdy/client/ota/model/UpdateInfo;

    iget-wide v6, v6, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    invoke-interface/range {v1 .. v7}, Lcom/navdy/client/ota/OTAUpdateListener;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :goto_0
    return-void

    .line 321
    :catch_0
    move-exception v0

    .line 322
    .local v0, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Bad listener "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
