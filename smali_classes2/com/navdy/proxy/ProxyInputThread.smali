.class public Lcom/navdy/proxy/ProxyInputThread;
.super Ljava/lang/Thread;
.source "ProxyInputThread.java"


# static fields
.field private static final VERBOSE_DBG:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field mInputStream:Ljava/io/InputStream;

.field mProxyThread:Lcom/navdy/proxy/ProxyThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/proxy/ProxyInputThread;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/proxy/ProxyInputThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/proxy/ProxyThread;Ljava/io/InputStream;)V
    .locals 0
    .param p1, "proxyThread"    # Lcom/navdy/proxy/ProxyThread;
    .param p2, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/navdy/proxy/ProxyInputThread;->mProxyThread:Lcom/navdy/proxy/ProxyThread;

    .line 21
    iput-object p2, p0, Lcom/navdy/proxy/ProxyInputThread;->mInputStream:Ljava/io/InputStream;

    .line 22
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 26
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 27
    const/16 v3, 0x400

    new-array v0, v3, [B

    .line 31
    .local v0, "buffer":[B
    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/navdy/proxy/ProxyInputThread;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 33
    .local v1, "bytesRead":I
    if-gez v1, :cond_1

    .line 40
    .end local v1    # "bytesRead":I
    :goto_1
    iget-object v3, p0, Lcom/navdy/proxy/ProxyInputThread;->mProxyThread:Lcom/navdy/proxy/ProxyThread;

    invoke-virtual {v3}, Lcom/navdy/proxy/ProxyThread;->inputThreadWillFinish()V

    .line 41
    return-void

    .line 34
    .restart local v1    # "bytesRead":I
    :cond_1
    if-lez v1, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/navdy/proxy/ProxyInputThread;->mProxyThread:Lcom/navdy/proxy/ProxyThread;

    invoke-virtual {v3, v0, v1}, Lcom/navdy/proxy/ProxyThread;->receiveBytes([BI)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 36
    .end local v1    # "bytesRead":I
    :catch_0
    move-exception v2

    .line 37
    .local v2, "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/proxy/ProxyInputThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mInputStream closed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_1
.end method
