.class public Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;
.super Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;
.source "BTRemoteDeviceScanner.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private btAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private devicesSeen:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private handleStartScan:Ljava/lang/Runnable;

.field private handleStopScan:Ljava/lang/Runnable;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private scanning:Z

.field private taskQueue:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serialTaskQueue"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;-><init>(Landroid/content/Context;)V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->scanning:Z

    .line 51
    new-instance v0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;-><init>(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->handleStartScan:Ljava/lang/Runnable;

    .line 86
    new-instance v0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$2;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$2;-><init>(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->handleStopScan:Ljava/lang/Runnable;

    .line 102
    new-instance v0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$3;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$3;-><init>(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 41
    iput p2, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->taskQueue:I

    .line 42
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->btAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->scanning:Z

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->scanning:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->devicesSeen:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;Ljava/util/Set;)Ljava/util/Set;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;
    .param p1, "x1"    # Ljava/util/Set;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->devicesSeen:Ljava/util/Set;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->btAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public startScan()Z
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->handleStartScan:Ljava/lang/Runnable;

    iget v2, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->taskQueue:I

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public stopScan()Z
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->handleStopScan:Ljava/lang/Runnable;

    iget v2, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->taskQueue:I

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 83
    const/4 v0, 0x1

    return v0
.end method
