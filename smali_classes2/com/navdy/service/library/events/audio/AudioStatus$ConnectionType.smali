.class public final enum Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;
.super Ljava/lang/Enum;
.source "AudioStatus.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/AudioStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

.field public static final enum AUDIO_CONNECTION_AUX:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

.field public static final enum AUDIO_CONNECTION_BLUETOOTH:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

.field public static final enum AUDIO_CONNECTION_NONE:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

.field public static final enum AUDIO_CONNECTION_SPEAKER:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 159
    new-instance v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    const-string v1, "AUDIO_CONNECTION_NONE"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_NONE:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 160
    new-instance v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    const-string v1, "AUDIO_CONNECTION_BLUETOOTH"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_BLUETOOTH:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 161
    new-instance v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    const-string v1, "AUDIO_CONNECTION_SPEAKER"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_SPEAKER:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 162
    new-instance v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    const-string v1, "AUDIO_CONNECTION_AUX"

    invoke-direct {v0, v1, v4, v6}, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_AUX:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 157
    new-array v0, v6, [Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    sget-object v1, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_NONE:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_BLUETOOTH:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_SPEAKER:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_AUX:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->$VALUES:[Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 167
    iput p3, p0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->value:I

    .line 168
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 157
    const-class v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->$VALUES:[Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->value:I

    return v0
.end method
