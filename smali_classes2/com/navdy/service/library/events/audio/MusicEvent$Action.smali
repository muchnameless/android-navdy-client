.class public final enum Lcom/navdy/service/library/events/audio/MusicEvent$Action;
.super Ljava/lang/Enum;
.source "MusicEvent.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicEvent$Action;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_FAST_FORWARD_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_FAST_FORWARD_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_MODE_CHANGE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_NEXT:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_PAUSE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_REWIND_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final enum MUSIC_ACTION_REWIND_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 229
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_PLAY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 230
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_PAUSE"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PAUSE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 231
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_NEXT"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_NEXT:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 232
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_PREVIOUS"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 233
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_REWIND_START"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 234
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_FAST_FORWARD_START"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 235
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_REWIND_STOP"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 236
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_FAST_FORWARD_STOP"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 237
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const-string v1, "MUSIC_ACTION_MODE_CHANGE"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_MODE_CHANGE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 224
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PAUSE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_NEXT:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_MODE_CHANGE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 241
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 242
    iput p3, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->value:I

    .line 243
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicEvent$Action;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 224
    const-class v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/MusicEvent$Action;
    .locals 1

    .prologue
    .line 224
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/MusicEvent$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->value:I

    return v0
.end method
