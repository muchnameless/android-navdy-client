.class public final Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicTrackInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicTrackInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public author:Ljava/lang/String;

.field public collectionId:Ljava/lang/String;

.field public collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public currentPosition:Ljava/lang/Integer;

.field public dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public duration:Ljava/lang/Integer;

.field public index:Ljava/lang/Long;

.field public isNextAllowed:Ljava/lang/Boolean;

.field public isOnlineStream:Ljava/lang/Boolean;

.field public isPreviousAllowed:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;

.field public playCount:Ljava/lang/Integer;

.field public playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

.field public shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

.field public trackId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 242
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 246
    if-nez p1, :cond_0

    .line 265
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 248
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->index:Ljava/lang/Long;

    .line 249
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name:Ljava/lang/String;

    .line 250
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author:Ljava/lang/String;

    .line 251
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album:Ljava/lang/String;

    .line 252
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->duration:Ljava/lang/Integer;

    .line 253
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->currentPosition:Ljava/lang/Integer;

    .line 254
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed:Ljava/lang/Boolean;

    .line 255
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed:Ljava/lang/Boolean;

    .line 256
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 257
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isOnlineStream:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isOnlineStream:Ljava/lang/Boolean;

    .line 258
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 259
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 260
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId:Ljava/lang/String;

    .line 261
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId:Ljava/lang/String;

    .line 262
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playCount:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playCount:Ljava/lang/Integer;

    .line 263
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 264
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    goto :goto_0
.end method


# virtual methods
.method public album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album:Ljava/lang/String;

    .line 304
    return-object p0
.end method

.method public author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 295
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author:Ljava/lang/String;

    .line 296
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .locals 2

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->checkRequiredFields()V

    .line 411
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;Lcom/navdy/service/library/events/audio/MusicTrackInfo$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    return-object v0
.end method

.method public collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "collectionId"    # Ljava/lang/String;

    .prologue
    .line 371
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionId:Ljava/lang/String;

    .line 372
    return-object p0
.end method

.method public collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 362
    return-object p0
.end method

.method public collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 367
    return-object p0
.end method

.method public currentPosition(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "currentPosition"    # Ljava/lang/Integer;

    .prologue
    .line 320
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->currentPosition:Ljava/lang/Integer;

    .line 321
    return-object p0
.end method

.method public dataSource(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "dataSource"    # Lcom/navdy/service/library/events/audio/MusicDataSource;

    .prologue
    .line 345
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 346
    return-object p0
.end method

.method public duration(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "duration"    # Ljava/lang/Integer;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->duration:Ljava/lang/Integer;

    .line 313
    return-object p0
.end method

.method public index(Ljava/lang/Long;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "index"    # Ljava/lang/Long;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->index:Ljava/lang/Long;

    .line 280
    return-object p0
.end method

.method public isNextAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "isNextAllowed"    # Ljava/lang/Boolean;

    .prologue
    .line 336
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed:Ljava/lang/Boolean;

    .line 337
    return-object p0
.end method

.method public isOnlineStream(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "isOnlineStream"    # Ljava/lang/Boolean;

    .prologue
    .line 353
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isOnlineStream:Ljava/lang/Boolean;

    .line 354
    return-object p0
.end method

.method public isPreviousAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "isPreviousAllowed"    # Ljava/lang/Boolean;

    .prologue
    .line 328
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed:Ljava/lang/Boolean;

    .line 329
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name:Ljava/lang/String;

    .line 288
    return-object p0
.end method

.method public playCount(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "playCount"    # Ljava/lang/Integer;

    .prologue
    .line 388
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playCount:Ljava/lang/Integer;

    .line 389
    return-object p0
.end method

.method public playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "playbackState"    # Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 272
    return-object p0
.end method

.method public repeatMode(Lcom/navdy/service/library/events/audio/MusicRepeatMode;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "repeatMode"    # Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .prologue
    .line 404
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .line 405
    return-object p0
.end method

.method public shuffleMode(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "shuffleMode"    # Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 397
    return-object p0
.end method

.method public trackId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;
    .locals 0
    .param p1, "trackId"    # Ljava/lang/String;

    .prologue
    .line 380
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->trackId:Ljava/lang/String;

    .line 381
    return-object p0
.end method
