.class public final Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VoiceAssistRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/VoiceAssistRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/VoiceAssistRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public end:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/VoiceAssistRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/VoiceAssistRequest;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 57
    if-nez p1, :cond_0

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->end:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;->end:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/VoiceAssistRequest;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;-><init>(Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;Lcom/navdy/service/library/events/audio/VoiceAssistRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceAssistRequest;

    move-result-object v0

    return-object v0
.end method

.method public end(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;
    .locals 0
    .param p1, "end"    # Ljava/lang/Boolean;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;->end:Ljava/lang/Boolean;

    .line 66
    return-object p0
.end method
