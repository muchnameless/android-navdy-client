.class public final Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LinkPropertiesChanged.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;",
        ">;"
    }
.end annotation


# instance fields
.field public bandwidthLevel:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 55
    if-nez p1, :cond_0

    .line 57
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;->bandwidthLevel:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;->bandwidthLevel:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public bandwidthLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;
    .locals 0
    .param p1, "bandwidthLevel"    # Ljava/lang/Integer;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;->bandwidthLevel:Ljava/lang/Integer;

    .line 61
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;-><init>(Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;->build()Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;

    move-result-object v0

    return-object v0
.end method
