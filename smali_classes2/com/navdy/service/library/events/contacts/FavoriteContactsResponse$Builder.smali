.class public final Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FavoriteContactsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 84
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 88
    if-nez p1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 90
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 91
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->contacts:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->contacts:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->checkRequiredFields()V

    .line 121
    new-instance v0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;-><init>(Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->build()Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    move-result-object v0

    return-object v0
.end method

.method public contacts(Ljava/util/List;)Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;)",
            "Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->contacts:Ljava/util/List;

    .line 115
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 99
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 107
    return-object p0
.end method
