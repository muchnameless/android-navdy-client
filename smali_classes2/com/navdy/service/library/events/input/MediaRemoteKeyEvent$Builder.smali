.class public final Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MediaRemoteKeyEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/navdy/service/library/events/input/KeyEvent;

.field public key:Lcom/navdy/service/library/events/input/MediaRemoteKey;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 59
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 63
    if-nez p1, :cond_0

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    iput-object v0, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 65
    iget-object v0, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    iput-object v0, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    goto :goto_0
.end method


# virtual methods
.method public action(Lcom/navdy/service/library/events/input/KeyEvent;)Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/input/KeyEvent;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    .line 75
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->checkRequiredFields()V

    .line 81
    new-instance v0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;-><init>(Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->build()Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    move-result-object v0

    return-object v0
.end method

.method public key(Lcom/navdy/service/library/events/input/MediaRemoteKey;)Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;
    .locals 0
    .param p1, "key"    # Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 70
    return-object p0
.end method
