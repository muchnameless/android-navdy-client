.class public final Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationsStatusUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field public errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

.field public service:Lcom/navdy/service/library/events/notification/ServiceType;

.field public state:Lcom/navdy/service/library/events/notification/NotificationsState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 81
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 85
    if-nez p1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 87
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 88
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->checkRequiredFields()V

    .line 119
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;-><init>(Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;

    move-result-object v0

    return-object v0
.end method

.method public errorDetails(Lcom/navdy/service/library/events/notification/NotificationsError;)Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;
    .locals 0
    .param p1, "errorDetails"    # Lcom/navdy/service/library/events/notification/NotificationsError;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->errorDetails:Lcom/navdy/service/library/events/notification/NotificationsError;

    .line 113
    return-object p0
.end method

.method public service(Lcom/navdy/service/library/events/notification/ServiceType;)Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;
    .locals 0
    .param p1, "service"    # Lcom/navdy/service/library/events/notification/ServiceType;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 104
    return-object p0
.end method

.method public state(Lcom/navdy/service/library/events/notification/NotificationsState;)Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;
    .locals 0
    .param p1, "state"    # Lcom/navdy/service/library/events/notification/NotificationsState;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->state:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 96
    return-object p0
.end method
