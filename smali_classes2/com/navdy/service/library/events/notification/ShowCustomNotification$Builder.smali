.class public final Lcom/navdy/service/library/events/notification/ShowCustomNotification$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ShowCustomNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/notification/ShowCustomNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/notification/ShowCustomNotification;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/ShowCustomNotification;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 53
    if-nez p1, :cond_0

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/ShowCustomNotification$Builder;->id:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/notification/ShowCustomNotification;
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/ShowCustomNotification$Builder;->checkRequiredFields()V

    .line 68
    new-instance v0, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Lcom/navdy/service/library/events/notification/ShowCustomNotification$Builder;Lcom/navdy/service/library/events/notification/ShowCustomNotification$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/ShowCustomNotification$Builder;->build()Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/ShowCustomNotification$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/ShowCustomNotification$Builder;->id:Ljava/lang/String;

    .line 62
    return-object p0
.end method
