.class public final Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ObdStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/obd/ObdStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/obd/ObdStatusResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public isConnected:Ljava/lang/Boolean;

.field public speed:Ljava/lang/Integer;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public vin:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 95
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/obd/ObdStatusResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/obd/ObdStatusResponse;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 99
    if-nez p1, :cond_0

    .line 104
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 101
    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->isConnected:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->isConnected:Ljava/lang/Boolean;

    .line 102
    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->vin:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->vin:Ljava/lang/String;

    .line 103
    iget-object v0, p1, Lcom/navdy/service/library/events/obd/ObdStatusResponse;->speed:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->speed:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/obd/ObdStatusResponse;
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->checkRequiredFields()V

    .line 142
    new-instance v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;-><init>(Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;Lcom/navdy/service/library/events/obd/ObdStatusResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->build()Lcom/navdy/service/library/events/obd/ObdStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public isConnected(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;
    .locals 0
    .param p1, "isConnected"    # Ljava/lang/Boolean;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->isConnected:Ljava/lang/Boolean;

    .line 119
    return-object p0
.end method

.method public speed(Ljava/lang/Integer;)Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;
    .locals 0
    .param p1, "speed"    # Ljava/lang/Integer;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->speed:Ljava/lang/Integer;

    .line 136
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 111
    return-object p0
.end method

.method public vin(Ljava/lang/String;)Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;
    .locals 0
    .param p1, "vin"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/service/library/events/obd/ObdStatusResponse$Builder;->vin:Ljava/lang/String;

    .line 127
    return-object p0
.end method
