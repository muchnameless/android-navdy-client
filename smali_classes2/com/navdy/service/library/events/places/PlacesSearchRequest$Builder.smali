.class public final Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PlacesSearchRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/PlacesSearchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/PlacesSearchRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public maxResults:Ljava/lang/Integer;

.field public requestId:Ljava/lang/String;

.field public searchArea:Ljava/lang/Integer;

.field public searchQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 94
    if-nez p1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchQuery:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchQuery:Ljava/lang/String;

    .line 96
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchArea:Ljava/lang/Integer;

    .line 97
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->maxResults:Ljava/lang/Integer;

    .line 98
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->requestId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->requestId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/PlacesSearchRequest;
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->checkRequiredFields()V

    .line 138
    new-instance v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/PlacesSearchRequest;-><init>(Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;Lcom/navdy/service/library/events/places/PlacesSearchRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->build()Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    move-result-object v0

    return-object v0
.end method

.method public maxResults(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;
    .locals 0
    .param p1, "maxResults"    # Ljava/lang/Integer;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->maxResults:Ljava/lang/Integer;

    .line 122
    return-object p0
.end method

.method public requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->requestId:Ljava/lang/String;

    .line 132
    return-object p0
.end method

.method public searchArea(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;
    .locals 0
    .param p1, "searchArea"    # Ljava/lang/Integer;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchArea:Ljava/lang/Integer;

    .line 114
    return-object p0
.end method

.method public searchQuery(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;
    .locals 0
    .param p1, "searchQuery"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchQuery:Ljava/lang/String;

    .line 106
    return-object p0
.end method
