.class public final enum Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
.super Ljava/lang/Enum;
.source "DriverProfilePreferences.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ObdScanSetting"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

.field public static final enum SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

.field public static final enum SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

.field public static final enum SCAN_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

.field public static final enum SCAN_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 537
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    const-string v1, "SCAN_DEFAULT_ON"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 538
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    const-string v1, "SCAN_ON"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 539
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    const-string v1, "SCAN_OFF"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 540
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    const-string v1, "SCAN_DEFAULT_OFF"

    invoke-direct {v0, v1, v4, v6}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 535
    new-array v0, v6, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->$VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 544
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 545
    iput p3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->value:I

    .line 546
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 535
    const-class v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .locals 1

    .prologue
    .line 535
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->$VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 550
    iget v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->value:I

    return v0
.end method
