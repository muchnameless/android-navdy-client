.class public abstract Lcom/navdy/service/library/file/TransferDataSource;
.super Ljava/lang/Object;
.source "TransferDataSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;,
        Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;
    }
.end annotation


# static fields
.field public static final TEST_DATA_NAME:Ljava/lang/String; = "<TESTDATA>"

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected mCurOffset:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/file/TransferDataSource;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/file/TransferDataSource;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/service/library/file/TransferDataSource;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static fileSource(Ljava/io/File;)Lcom/navdy/service/library/file/TransferDataSource;
    .locals 1
    .param p0, "f"    # Ljava/io/File;

    .prologue
    .line 146
    new-instance v0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public static testSource(J)Lcom/navdy/service/library/file/TransferDataSource;
    .locals 2
    .param p0, "length"    # J

    .prologue
    .line 150
    new-instance v0, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/file/TransferDataSource$TestDataSource;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public abstract checkSum()Ljava/lang/String;
.end method

.method public abstract checkSum(J)Ljava/lang/String;
.end method

.method public abstract getFile()Ljava/io/File;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract length()J
.end method

.method public abstract read([B)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public seek(J)V
    .locals 1
    .param p1, "offset"    # J

    .prologue
    .line 134
    iput-wide p1, p0, Lcom/navdy/service/library/file/TransferDataSource;->mCurOffset:J

    .line 135
    return-void
.end method
