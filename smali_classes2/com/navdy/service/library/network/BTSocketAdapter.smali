.class public Lcom/navdy/service/library/network/BTSocketAdapter;
.super Ljava/lang/Object;
.source "BTSocketAdapter.java"

# interfaces
.implements Lcom/navdy/service/library/network/SocketAdapter;


# static fields
.field private static final needsPatch:Z

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private closed:Z

.field private mFd:I

.field private mPfd:Landroid/os/ParcelFileDescriptor;

.field final socket:Landroid/bluetooth/BluetoothSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/network/BTSocketAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/network/BTSocketAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 22
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/navdy/service/library/network/BTSocketAdapter;->needsPatch:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/bluetooth/BluetoothSocket;)V
    .locals 2
    .param p1, "socket"    # Landroid/bluetooth/BluetoothSocket;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mFd:I

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->closed:Z

    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_0
    iput-object p1, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    .line 37
    sget-boolean v0, Lcom/navdy/service/library/network/BTSocketAdapter;->needsPatch:Z

    if-eqz v0, :cond_1

    .line 38
    invoke-direct {p0}, Lcom/navdy/service/library/network/BTSocketAdapter;->storeFd()V

    .line 40
    :cond_1
    return-void
.end method

.method private declared-synchronized cleanupFd()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 125
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mPfd:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 126
    sget-object v0, Lcom/navdy/service/library/network/BTSocketAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Closing mPfd"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mPfd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mPfd:Landroid/os/ParcelFileDescriptor;

    .line 130
    :cond_0
    iget v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mFd:I

    if-eq v0, v2, :cond_1

    .line 131
    sget-object v0, Lcom/navdy/service/library/network/BTSocketAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Closing mFd:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mFd:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 132
    iget v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mFd:I

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeFD(I)V

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mFd:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    :cond_1
    monitor-exit p0

    return-void

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized storeFd()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 99
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mPfd:Landroid/os/ParcelFileDescriptor;

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mFd:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v4, v6, :cond_0

    .line 121
    :goto_0
    monitor-exit p0

    return-void

    .line 105
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "mPfd"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 106
    .local v2, "field":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 108
    iget-object v4, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/ParcelFileDescriptor;

    .line 109
    .local v3, "pfd":Landroid/os/ParcelFileDescriptor;
    if-eqz v3, :cond_1

    .line 110
    iput-object v3, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mPfd:Landroid/os/ParcelFileDescriptor;

    .line 113
    :cond_1
    iget-object v4, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->getSocketFD(Landroid/bluetooth/BluetoothSocket;)I

    move-result v1

    .line 114
    .local v1, "fd":I
    if-eq v1, v6, :cond_2

    .line 115
    iput v1, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mFd:I

    .line 117
    :cond_2
    sget-object v4, Lcom/navdy/service/library/network/BTSocketAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Stored "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mPfd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fd:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->mFd:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 118
    .end local v1    # "fd":I
    .end local v2    # "field":Ljava/lang/reflect/Field;
    .end local v3    # "pfd":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v4, Lcom/navdy/service/library/network/BTSocketAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Exception storing socket internals"

    invoke-virtual {v4, v5, v0}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 99
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v1, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    monitor-enter v1

    .line 60
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->closed:Z

    if-nez v0, :cond_2

    .line 61
    sget-boolean v0, Lcom/navdy/service/library/network/BTSocketAdapter;->needsPatch:Z

    if-eqz v0, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/navdy/service/library/network/BTSocketAdapter;->storeFd()V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    .line 65
    sget-boolean v0, Lcom/navdy/service/library/network/BTSocketAdapter;->needsPatch:Z

    if-eqz v0, :cond_1

    .line 66
    invoke-direct {p0}, Lcom/navdy/service/library/network/BTSocketAdapter;->cleanupFd()V

    .line 68
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->closed:Z

    .line 70
    :cond_2
    monitor-exit v1

    .line 71
    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public connect()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->connect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    sget-boolean v1, Lcom/navdy/service/library/network/BTSocketAdapter;->needsPatch:Z

    if-eqz v1, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/navdy/service/library/network/BTSocketAdapter;->storeFd()V

    .line 55
    :cond_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/io/IOException;
    sget-boolean v1, Lcom/navdy/service/library/network/BTSocketAdapter;->needsPatch:Z

    if-eqz v1, :cond_1

    .line 48
    invoke-direct {p0}, Lcom/navdy/service/library/network/BTSocketAdapter;->storeFd()V

    .line 50
    :cond_1
    throw v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteDevice()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 2

    .prologue
    .line 91
    iget-object v1, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 92
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_0

    .line 93
    new-instance v1, Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {v1, v0}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Landroid/bluetooth/BluetoothDevice;)V

    .line 95
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAdapter;->socket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->isConnected()Z

    move-result v0

    return v0
.end method
