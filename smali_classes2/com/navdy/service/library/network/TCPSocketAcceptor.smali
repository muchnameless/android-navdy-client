.class public Lcom/navdy/service/library/network/TCPSocketAcceptor;
.super Ljava/lang/Object;
.source "TCPSocketAcceptor.java"

# interfaces
.implements Lcom/navdy/service/library/network/SocketAcceptor;


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final local:Z

.field private final port:I

.field private serverSocket:Ljava/net/ServerSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/network/TCPSocketAcceptor;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "port"    # I

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/service/library/network/TCPSocketAcceptor;-><init>(IZ)V

    .line 24
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 0
    .param p1, "port"    # I
    .param p2, "bindToLocalHost"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean p2, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->local:Z

    .line 30
    iput p1, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->port:I

    .line 31
    return-void
.end method


# virtual methods
.method public accept()Lcom/navdy/service/library/network/SocketAdapter;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    .line 35
    iget-object v1, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->serverSocket:Ljava/net/ServerSocket;

    if-nez v1, :cond_0

    .line 36
    iget-boolean v1, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->local:Z

    if-eqz v1, :cond_1

    .line 37
    const/4 v1, 0x4

    new-array v1, v1, [B

    fill-array-data v1, :array_0

    invoke-static {v1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    .line 38
    .local v0, "address":Ljava/net/InetAddress;
    new-instance v1, Ljava/net/ServerSocket;

    iget v2, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->port:I

    invoke-direct {v1, v2, v3, v0}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    iput-object v1, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->serverSocket:Ljava/net/ServerSocket;

    .line 42
    .end local v0    # "address":Ljava/net/InetAddress;
    :goto_0
    sget-object v2, Lcom/navdy/service/library/network/TCPSocketAcceptor;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Socket bound to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v1, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->local:Z

    if-eqz v1, :cond_2

    const-string v1, "localhost"

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", port "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->port:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 44
    :cond_0
    new-instance v1, Lcom/navdy/service/library/network/TCPSocketAdapter;

    iget-object v2, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/service/library/network/TCPSocketAdapter;-><init>(Ljava/net/Socket;)V

    return-object v1

    .line 40
    :cond_1
    new-instance v1, Ljava/net/ServerSocket;

    iget v2, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->port:I

    invoke-direct {v1, v2, v3}, Ljava/net/ServerSocket;-><init>(II)V

    iput-object v1, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->serverSocket:Ljava/net/ServerSocket;

    goto :goto_0

    .line 42
    :cond_2
    const-string v1, "all interfaces"

    goto :goto_1

    .line 37
    :array_0
    .array-data 1
        0x7ft
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->serverSocket:Ljava/net/ServerSocket;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->close()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAcceptor;->serverSocket:Ljava/net/ServerSocket;

    .line 53
    :cond_0
    return-void
.end method

.method public getRemoteConnectionInfo(Lcom/navdy/service/library/network/SocketAdapter;Lcom/navdy/service/library/device/connection/ConnectionType;)Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 4
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;
    .param p2, "connectionType"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 57
    instance-of v2, p1, Lcom/navdy/service/library/network/TCPSocketAdapter;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 58
    check-cast v0, Lcom/navdy/service/library/network/TCPSocketAdapter;

    .line 59
    .local v0, "adapter":Lcom/navdy/service/library/network/TCPSocketAdapter;
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getRemoteDevice()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    .line 60
    .local v1, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    if-eqz v1, :cond_0

    .line 61
    new-instance v2, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;

    invoke-virtual {v0}, Lcom/navdy/service/library/network/TCPSocketAdapter;->getRemoteAddress()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;Ljava/lang/String;)V

    .line 64
    .end local v0    # "adapter":Lcom/navdy/service/library/network/TCPSocketAdapter;
    .end local v1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
