.class public Lcom/vividsolutions/jts/geom/Polygon;
.super Lcom/vividsolutions/jts/geom/Geometry;
.source "Polygon.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/Polygonal;


# static fields
.field private static final serialVersionUID:J = -0x307ffefd8dc971b5L


# instance fields
.field protected holes:[Lcom/vividsolutions/jts/geom/LinearRing;

.field protected shell:Lcom/vividsolutions/jts/geom/LinearRing;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/PrecisionModel;I)V
    .locals 2
    .param p1, "shell"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "precisionModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;
    .param p3, "SRID"    # I

    .prologue
    .line 100
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/LinearRing;

    new-instance v1, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v1, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;I)V

    invoke-direct {p0, p1, v0, v1}, Lcom/vividsolutions/jts/geom/Polygon;-><init>(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 101
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 2
    .param p1, "shell"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "holes"    # [Lcom/vividsolutions/jts/geom/LinearRing;
    .param p3, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    const/4 v0, 0x0

    .line 135
    invoke-direct {p0, p3}, Lcom/vividsolutions/jts/geom/Geometry;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 78
    iput-object v0, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 136
    if-nez p1, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    check-cast v0, Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object p1

    .line 139
    :cond_0
    if-nez p2, :cond_1

    .line 140
    const/4 v0, 0x0

    new-array p2, v0, [Lcom/vividsolutions/jts/geom/LinearRing;

    .line 142
    :cond_1
    invoke-static {p2}, Lcom/vividsolutions/jts/geom/Polygon;->hasNullElements([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "holes must not contain null elements"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p2}, Lcom/vividsolutions/jts/geom/Polygon;->hasNonEmptyElements([Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "shell is empty but holes are not"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_3
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 149
    iput-object p2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    .line 150
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/PrecisionModel;I)V
    .locals 1
    .param p1, "shell"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "holes"    # [Lcom/vividsolutions/jts/geom/LinearRing;
    .param p3, "precisionModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;
    .param p4, "SRID"    # I

    .prologue
    .line 120
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0, p3, p4}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;I)V

    invoke-direct {p0, p1, p2, v0}, Lcom/vividsolutions/jts/geom/Polygon;-><init>(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 121
    return-void
.end method

.method private normalize(Lcom/vividsolutions/jts/geom/LinearRing;Z)V
    .locals 5
    .param p1, "ring"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "clockwise"    # Z

    .prologue
    const/4 v4, 0x0

    .line 425
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    new-array v1, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 429
    .local v1, "uniqueCoordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    array-length v3, v1

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 430
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-static {v2}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->minCoordinate([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 431
    .local v0, "minCoordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v1, v0}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->scroll([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 432
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 433
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    array-length v3, v1

    aget-object v4, v1, v4

    aput-object v4, v2, v3

    .line 434
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-static {v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isCCW([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    if-ne v2, p2, :cond_0

    .line 435
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-static {v2}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->reverse([Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0
.end method


# virtual methods
.method public apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/CoordinateFilter;

    .prologue
    .line 335
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/LinearRing;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 336
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 337
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/LinearRing;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 336
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 339
    :cond_0
    return-void
.end method

.method public apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;

    .prologue
    .line 343
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/LinearRing;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 344
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 345
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 346
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/LinearRing;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 347
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;->isDone()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 351
    .end local v0    # "i":I
    :cond_0
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;->isGeometryChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 352
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->geometryChanged()V

    .line 353
    :cond_1
    return-void

    .line 345
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/GeometryComponentFilter;

    .prologue
    .line 360
    invoke-interface {p1, p0}, Lcom/vividsolutions/jts/geom/GeometryComponentFilter;->filter(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 361
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/LinearRing;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 362
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 363
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/LinearRing;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 362
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 365
    :cond_0
    return-void
.end method

.method public apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V
    .locals 0
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/GeometryFilter;

    .prologue
    .line 356
    invoke-interface {p1, p0}, Lcom/vividsolutions/jts/geom/GeometryFilter;->filter(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 357
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 374
    invoke-super {p0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Polygon;

    .line 375
    .local v1, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    iput-object v2, v1, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 376
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v2, v2

    new-array v2, v2, [Lcom/vividsolutions/jts/geom/LinearRing;

    iput-object v2, v1, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    .line 377
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 378
    iget-object v3, v1, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    aput-object v2, v3, v0

    .line 377
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 380
    :cond_0
    return-object v1
.end method

.method protected compareToSameClass(Ljava/lang/Object;)I
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 396
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 397
    .local v1, "thisShell":Lcom/vividsolutions/jts/geom/LinearRing;
    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v0, p1, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 398
    .local v0, "otherShell":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/LinearRing;->compareToSameClass(Ljava/lang/Object;)I

    move-result v2

    return v2
.end method

.method protected compareToSameClass(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I
    .locals 10
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "comp"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;

    .prologue
    .line 402
    move-object v6, p1

    check-cast v6, Lcom/vividsolutions/jts/geom/Polygon;

    .line 404
    .local v6, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    iget-object v9, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 405
    .local v9, "thisShell":Lcom/vividsolutions/jts/geom/LinearRing;
    iget-object v5, v6, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 406
    .local v5, "otherShell":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v9, v5, p2}, Lcom/vividsolutions/jts/geom/LinearRing;->compareToSameClass(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I

    move-result v7

    .line 407
    .local v7, "shellComp":I
    if-eqz v7, :cond_0

    .line 421
    .end local v7    # "shellComp":I
    :goto_0
    return v7

    .line 409
    .restart local v7    # "shellComp":I
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v2

    .line 410
    .local v2, "nHole1":I
    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v3

    .line 411
    .local v3, "nHole2":I
    const/4 v1, 0x0

    .line 412
    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_2

    if-ge v1, v3, :cond_2

    .line 413
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v8

    check-cast v8, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 414
    .local v8, "thisHole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v6, v1}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 415
    .local v4, "otherHole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v8, v4, p2}, Lcom/vividsolutions/jts/geom/LinearRing;->compareToSameClass(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I

    move-result v0

    .line 416
    .local v0, "holeComp":I
    if-eqz v0, :cond_1

    move v7, v0

    goto :goto_0

    .line 417
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 418
    goto :goto_1

    .line 419
    .end local v0    # "holeComp":I
    .end local v4    # "otherHole":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v8    # "thisHole":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_2
    if-ge v1, v2, :cond_3

    const/4 v7, 0x1

    goto :goto_0

    .line 420
    :cond_3
    if-ge v1, v3, :cond_4

    const/4 v7, -0x1

    goto :goto_0

    .line 421
    :cond_4
    const/4 v7, 0x0

    goto :goto_0
.end method

.method protected computeEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    return-object v0
.end method

.method public convexHull()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->convexHull()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 8
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "tolerance"    # D

    .prologue
    const/4 v4, 0x0

    .line 314
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Polygon;->isEquivalentClass(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v4

    :cond_1
    move-object v1, p1

    .line 317
    check-cast v1, Lcom/vividsolutions/jts/geom/Polygon;

    .line 318
    .local v1, "otherPolygon":Lcom/vividsolutions/jts/geom/Polygon;
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 319
    .local v3, "thisShell":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v2, v1, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 320
    .local v2, "otherPolygonShell":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v3, v2, p2, p3}, Lcom/vividsolutions/jts/geom/Geometry;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 323
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v5, v5

    iget-object v6, v1, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v6, v6

    if-ne v5, v6, :cond_0

    .line 326
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v5, v5

    if-ge v0, v5, :cond_2

    .line 327
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v5, v5, v0

    iget-object v6, v1, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6, p2, p3}, Lcom/vividsolutions/jts/geom/Geometry;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 326
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 331
    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public getArea()D
    .locals 6

    .prologue
    .line 265
    const-wide/16 v0, 0x0

    .line 266
    .local v0, "area":D
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v3

    invoke-static {v3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->signedArea(Lcom/vividsolutions/jts/geom/CoordinateSequence;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    add-double/2addr v0, v4

    .line 267
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 268
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v3

    invoke-static {v3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->signedArea(Lcom/vividsolutions/jts/geom/CoordinateSequence;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    sub-double/2addr v0, v4

    .line 267
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 270
    :cond_0
    return-wide v0
.end method

.method public getBoundary()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 295
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v2

    .line 306
    :goto_0
    return-object v2

    .line 298
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v1, v2, [Lcom/vividsolutions/jts/geom/LinearRing;

    .line 299
    .local v1, "rings":[Lcom/vividsolutions/jts/geom/LinearRing;
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    aput-object v2, v1, v4

    .line 300
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 301
    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v3, v3, v0

    aput-object v3, v1, v2

    .line 300
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 304
    :cond_1
    array-length v2, v1

    const/4 v3, 0x1

    if-gt v2, v3, :cond_2

    .line 305
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v2

    goto :goto_0

    .line 306
    :cond_2
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v2

    goto :goto_0
.end method

.method public getBoundaryDimension()I
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x1

    return v0
.end method

.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 158
    const/4 v7, 0x0

    new-array v1, v7, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 174
    :cond_0
    return-object v1

    .line 160
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getNumPoints()I

    move-result v7

    new-array v1, v7, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 161
    .local v1, "coordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v4, -0x1

    .line 162
    .local v4, "k":I
    iget-object v7, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 163
    .local v5, "shellCoordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_0
    array-length v7, v5

    if-ge v6, v7, :cond_2

    .line 164
    add-int/lit8 v4, v4, 0x1

    .line 165
    aget-object v7, v5, v6

    aput-object v7, v1, v4

    .line 163
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 167
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v7, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v7, v7

    if-ge v2, v7, :cond_0

    .line 168
    iget-object v7, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v7, v7, v2

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 169
    .local v0, "childCoordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    array-length v7, v0

    if-ge v3, v7, :cond_3

    .line 170
    add-int/lit8 v4, v4, 0x1

    .line 171
    aget-object v7, v0, v3

    aput-object v7, v1, v4

    .line 169
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 167
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getDimension()I
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x2

    return v0
.end method

.method public getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    return-object v0
.end method

.method public getGeometryType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    const-string v0, "Polygon"

    return-object v0
.end method

.method public getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 251
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getLength()D
    .locals 6

    .prologue
    .line 280
    const-wide/16 v2, 0x0

    .line 281
    .local v2, "len":D
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LinearRing;->getLength()D

    move-result-wide v4

    add-double/2addr v2, v4

    .line 282
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 283
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LinearRing;->getLength()D

    move-result-wide v4

    add-double/2addr v2, v4

    .line 282
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 285
    :cond_0
    return-wide v2
.end method

.method public getNumInteriorRing()I
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v0, v0

    return v0
.end method

.method public getNumPoints()I
    .locals 3

    .prologue
    .line 178
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->getNumPoints()I

    move-result v1

    .line 179
    .local v1, "numPoints":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 180
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->getNumPoints()I

    move-result v2

    add-int/2addr v1, v2

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_0
    return v1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LinearRing;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isRectangle()Z
    .locals 18

    .prologue
    .line 211
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v15

    if-eqz v15, :cond_0

    const/4 v15, 0x0

    .line 239
    :goto_0
    return v15

    .line 212
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    if-nez v15, :cond_1

    const/4 v15, 0x0

    goto :goto_0

    .line 213
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v15}, Lcom/vividsolutions/jts/geom/LinearRing;->getNumPoints()I

    move-result v15

    const/16 v16, 0x5

    move/from16 v0, v16

    if-eq v15, v0, :cond_2

    const/4 v15, 0x0

    goto :goto_0

    .line 215
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v15}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v8

    .line 218
    .local v8, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/geom/Polygon;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    .line 219
    .local v2, "env":Lcom/vividsolutions/jts/geom/Envelope;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    const/4 v15, 0x5

    if-ge v3, v15, :cond_5

    .line 220
    invoke-interface {v8, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getX(I)D

    move-result-wide v10

    .line 221
    .local v10, "x":D
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v16

    cmpl-double v15, v10, v16

    if-eqz v15, :cond_3

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v16

    cmpl-double v15, v10, v16

    if-eqz v15, :cond_3

    const/4 v15, 0x0

    goto :goto_0

    .line 222
    :cond_3
    invoke-interface {v8, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getY(I)D

    move-result-wide v12

    .line 223
    .local v12, "y":D
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v16

    cmpl-double v15, v12, v16

    if-eqz v15, :cond_4

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v16

    cmpl-double v15, v12, v16

    if-eqz v15, :cond_4

    const/4 v15, 0x0

    goto :goto_0

    .line 219
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 227
    .end local v10    # "x":D
    .end local v12    # "y":D
    :cond_5
    const/4 v15, 0x0

    invoke-interface {v8, v15}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getX(I)D

    move-result-wide v4

    .line 228
    .local v4, "prevX":D
    const/4 v15, 0x0

    invoke-interface {v8, v15}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getY(I)D

    move-result-wide v6

    .line 229
    .local v6, "prevY":D
    const/4 v3, 0x1

    :goto_2
    const/4 v15, 0x4

    if-gt v3, v15, :cond_9

    .line 230
    invoke-interface {v8, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getX(I)D

    move-result-wide v10

    .line 231
    .restart local v10    # "x":D
    invoke-interface {v8, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getY(I)D

    move-result-wide v12

    .line 232
    .restart local v12    # "y":D
    cmpl-double v15, v10, v4

    if-eqz v15, :cond_6

    const/4 v9, 0x1

    .line 233
    .local v9, "xChanged":Z
    :goto_3
    cmpl-double v15, v12, v6

    if-eqz v15, :cond_7

    const/4 v14, 0x1

    .line 234
    .local v14, "yChanged":Z
    :goto_4
    if-ne v9, v14, :cond_8

    .line 235
    const/4 v15, 0x0

    goto :goto_0

    .line 232
    .end local v9    # "xChanged":Z
    .end local v14    # "yChanged":Z
    :cond_6
    const/4 v9, 0x0

    goto :goto_3

    .line 233
    .restart local v9    # "xChanged":Z
    :cond_7
    const/4 v14, 0x0

    goto :goto_4

    .line 236
    .restart local v14    # "yChanged":Z
    :cond_8
    move-wide v4, v10

    .line 237
    move-wide v6, v12

    .line 229
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 239
    .end local v9    # "xChanged":Z
    .end local v10    # "x":D
    .end local v12    # "y":D
    .end local v14    # "yChanged":Z
    :cond_9
    const/4 v15, 0x1

    goto/16 :goto_0
.end method

.method public normalize()V
    .locals 3

    .prologue
    .line 388
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/vividsolutions/jts/geom/Polygon;->normalize(Lcom/vividsolutions/jts/geom/LinearRing;Z)V

    .line 389
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/vividsolutions/jts/geom/Polygon;->normalize(Lcom/vividsolutions/jts/geom/LinearRing;Z)V

    .line 389
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 392
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 393
    return-void
.end method

.method public reverse()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4

    .prologue
    .line 441
    invoke-super {p0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Polygon;

    .line 442
    .local v1, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->reverse()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    iput-object v2, v1, Lcom/vividsolutions/jts/geom/Polygon;->shell:Lcom/vividsolutions/jts/geom/LinearRing;

    .line 443
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v2, v2

    new-array v2, v2, [Lcom/vividsolutions/jts/geom/LinearRing;

    iput-object v2, v1, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    .line 444
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 445
    iget-object v3, v1, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/Polygon;->holes:[Lcom/vividsolutions/jts/geom/LinearRing;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->reverse()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    aput-object v2, v3, v0

    .line 444
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 447
    :cond_0
    return-object v1
.end method
