.class Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContains;
.super Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;
.source "PreparedPolygonContains.java"


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V
    .locals 0
    .param p1, "prepPoly"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 75
    return-void
.end method

.method public static contains(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "prep"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 63
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContains;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContains;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 64
    .local v0, "polyInt":Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContains;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContains;->contains(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public contains(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContains;->eval(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method protected fullTopologicalPredicate(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 97
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContains;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/Geometry;->contains(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    .line 98
    .local v0, "isContained":Z
    return v0
.end method
