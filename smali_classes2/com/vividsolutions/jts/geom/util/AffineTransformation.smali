.class public Lcom/vividsolutions/jts/geom/util/AffineTransformation;
.super Ljava/lang/Object;
.source "AffineTransformation.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;


# instance fields
.field private m00:D

.field private m01:D

.field private m02:D

.field private m10:D

.field private m11:D

.field private m12:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToIdentity()Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 274
    return-void
.end method

.method public constructor <init>(DDDDDD)V
    .locals 1
    .param p1, "m00"    # D
    .param p3, "m01"    # D
    .param p5, "m02"    # D
    .param p7, "m10"    # D
    .param p9, "m11"    # D
    .param p11, "m12"    # D

    .prologue
    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    invoke-virtual/range {p0 .. p12}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setTransformation(DDDDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 313
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 0
    .param p1, "src0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "src1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "src2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "dest0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p5, "dest1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p6, "dest2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)V
    .locals 0
    .param p1, "trans"    # Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .prologue
    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setTransformation(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 324
    return-void
.end method

.method public constructor <init>([D)V
    .locals 2
    .param p1, "matrix"    # [D

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    .line 287
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    .line 288
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 289
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    .line 290
    const/4 v0, 0x4

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    .line 291
    const/4 v0, 0x5

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 292
    return-void
.end method

.method public static reflectionInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 2
    .param p0, "x"    # D
    .param p2, "y"    # D

    .prologue
    .line 124
    new-instance v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>()V

    .line 125
    .local v0, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToReflection(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 126
    return-object v0
.end method

.method public static reflectionInstance(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 10
    .param p0, "x0"    # D
    .param p2, "y0"    # D
    .param p4, "x1"    # D
    .param p6, "y1"    # D

    .prologue
    .line 109
    new-instance v1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>()V

    .local v1, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    move-wide v2, p0

    move-wide v4, p2

    move-wide v6, p4

    move-wide/from16 v8, p6

    .line 110
    invoke-virtual/range {v1 .. v9}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToReflection(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 111
    return-object v1
.end method

.method public static rotationInstance(D)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 4
    .param p0, "theta"    # D

    .prologue
    .line 141
    invoke-static {p0, p1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotationInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    return-object v0
.end method

.method public static rotationInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 2
    .param p0, "sinTheta"    # D
    .param p2, "cosTheta"    # D

    .prologue
    .line 157
    new-instance v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>()V

    .line 158
    .local v0, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToRotation(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 159
    return-object v0
.end method

.method public static rotationInstance(DDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 8
    .param p0, "theta"    # D
    .param p2, "x"    # D
    .param p4, "y"    # D

    .prologue
    .line 175
    invoke-static {p0, p1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v0 .. v7}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotationInstance(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    return-object v0
.end method

.method public static rotationInstance(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 10
    .param p0, "sinTheta"    # D
    .param p2, "cosTheta"    # D
    .param p4, "x"    # D
    .param p6, "y"    # D

    .prologue
    .line 193
    new-instance v1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>()V

    .local v1, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    move-wide v2, p0

    move-wide v4, p2

    move-wide v6, p4

    move-wide/from16 v8, p6

    .line 194
    invoke-virtual/range {v1 .. v9}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToRotation(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 195
    return-object v1
.end method

.method public static scaleInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 2
    .param p0, "xScale"    # D
    .param p2, "yScale"    # D

    .prologue
    .line 207
    new-instance v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>()V

    .line 208
    .local v0, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToScale(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 209
    return-object v0
.end method

.method public static scaleInstance(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 6
    .param p0, "xScale"    # D
    .param p2, "yScale"    # D
    .param p4, "x"    # D
    .param p6, "y"    # D

    .prologue
    .line 223
    new-instance v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>()V

    .line 224
    .local v0, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    neg-double v2, p4

    neg-double v4, p6

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->translate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 225
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->scale(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 226
    invoke-virtual {v0, p4, p5, p6, p7}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->translate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 227
    return-object v0
.end method

.method public static shearInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 2
    .param p0, "xShear"    # D
    .param p2, "yShear"    # D

    .prologue
    .line 240
    new-instance v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>()V

    .line 241
    .local v0, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToShear(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 242
    return-object v0
.end method

.method public static translationInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 2
    .param p0, "x"    # D
    .param p2, "y"    # D

    .prologue
    .line 254
    new-instance v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>()V

    .line 255
    .local v0, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToTranslation(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 256
    return-object v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1109
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1113
    :goto_0
    return-object v1

    .line 1110
    :catch_0
    move-exception v0

    .line 1111
    .local v0, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere()V

    .line 1113
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 20
    .param p1, "trans"    # Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .prologue
    .line 929
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v2, v14, v16

    .line 930
    .local v2, "mp00":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v4, v14, v16

    .line 931
    .local v4, "mp01":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    move-wide/from16 v16, v0

    add-double v6, v14, v16

    .line 932
    .local v6, "mp02":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v8, v14, v16

    .line 933
    .local v8, "mp10":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v10, v14, v16

    .line 934
    .local v10, "mp11":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v14, v14, v16

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    move-wide/from16 v16, v0

    add-double v12, v14, v16

    .line 935
    .local v12, "mp12":D
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    .line 936
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    .line 937
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 938
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    .line 939
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    .line 940
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 941
    return-object p0
.end method

.method public composeBefore(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 20
    .param p1, "trans"    # Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .prologue
    .line 960
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v2, v14, v16

    .line 961
    .local v2, "mp00":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v4, v14, v16

    .line 962
    .local v4, "mp01":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    move-wide/from16 v16, v0

    add-double v6, v14, v16

    .line 963
    .local v6, "mp02":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v8, v14, v16

    .line 964
    .local v8, "mp10":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v10, v14, v16

    .line 965
    .local v10, "mp11":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    add-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    move-wide/from16 v16, v0

    add-double v12, v14, v16

    .line 966
    .local v12, "mp12":D
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    .line 967
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    .line 968
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 969
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    .line 970
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    .line 971
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 972
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1071
    if-nez p1, :cond_1

    .line 1076
    :cond_0
    :goto_0
    return v1

    .line 1072
    :cond_1
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 1075
    check-cast v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 1076
    .local v0, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public filter(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V
    .locals 0
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "i"    # I

    .prologue
    .line 1030
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->transform(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V

    .line 1031
    return-void
.end method

.method public getDeterminant()D
    .locals 6

    .prologue
    .line 439
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public getInverse()Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vividsolutions/jts/geom/util/NoninvertibleTransformationException;
        }
    .end annotation

    .prologue
    .line 484
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->getDeterminant()D

    move-result-wide v16

    .line 485
    .local v16, "det":D
    const-wide/16 v2, 0x0

    cmpl-double v2, v16, v2

    if-nez v2, :cond_0

    .line 486
    new-instance v2, Lcom/vividsolutions/jts/geom/util/NoninvertibleTransformationException;

    const-string v3, "Transformation is non-invertible"

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/geom/util/NoninvertibleTransformationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 488
    :cond_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    div-double v4, v2, v16

    .line 489
    .local v4, "im00":D
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    neg-double v2, v2

    div-double v10, v2, v16

    .line 490
    .local v10, "im10":D
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    neg-double v2, v2

    div-double v6, v2, v16

    .line 491
    .local v6, "im01":D
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    div-double v12, v2, v16

    .line 492
    .local v12, "im11":D
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    move-wide/from16 v18, v0

    mul-double v2, v2, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    sub-double v2, v2, v18

    div-double v8, v2, v16

    .line 493
    .local v8, "im02":D
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    neg-double v2, v2

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    move-wide/from16 v18, v0

    mul-double v2, v2, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    add-double v2, v2, v18

    div-double v14, v2, v16

    .line 495
    .local v14, "im12":D
    new-instance v3, Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    invoke-direct/range {v3 .. v15}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;-><init>(DDDDDD)V

    return-object v3
.end method

.method public getMatrixEntries()[D
    .locals 4

    .prologue
    .line 418
    const/4 v0, 0x6

    new-array v0, v0, [D

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    aput-wide v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    aput-wide v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    aput-wide v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    aput-wide v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    aput-wide v2, v0, v1

    return-object v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 1046
    const/4 v0, 0x0

    return v0
.end method

.method public isGeometryChanged()Z
    .locals 1

    .prologue
    .line 1035
    const/4 v0, 0x1

    return v0
.end method

.method public isIdentity()Z
    .locals 6

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 1056
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reflect(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 795
    invoke-static {p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->reflectionInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 796
    return-object p0
.end method

.method public reflect(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "x0"    # D
    .param p3, "y0"    # D
    .param p5, "x1"    # D
    .param p7, "y1"    # D

    .prologue
    .line 780
    invoke-static/range {p1 .. p8}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->reflectionInstance(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 781
    return-object p0
.end method

.method public rotate(D)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "theta"    # D

    .prologue
    .line 811
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotationInstance(D)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 812
    return-object p0
.end method

.method public rotate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "sinTheta"    # D
    .param p3, "cosTheta"    # D

    .prologue
    .line 827
    invoke-static {p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotationInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 828
    return-object p0
.end method

.method public rotate(DDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "theta"    # D
    .param p3, "x"    # D
    .param p5, "y"    # D

    .prologue
    .line 845
    invoke-static/range {p1 .. p6}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotationInstance(DDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 846
    return-object p0
.end method

.method public rotate(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "sinTheta"    # D
    .param p3, "cosTheta"    # D
    .param p5, "x"    # D
    .param p7, "y"    # D

    .prologue
    .line 863
    invoke-static {p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotationInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 864
    return-object p0
.end method

.method public scale(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "xScale"    # D
    .param p3, "yScale"    # D

    .prologue
    .line 878
    invoke-static {p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->scaleInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 879
    return-object p0
.end method

.method public setToIdentity()Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 360
    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 361
    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 362
    return-object p0
.end method

.method public setToReflection(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 11
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 576
    const-wide/16 v6, 0x0

    cmpl-double v6, p1, v6

    if-nez v6, :cond_0

    const-wide/16 v6, 0x0

    cmpl-double v6, p3, v6

    if-nez v6, :cond_0

    .line 577
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Reflection vector must be non-zero"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 584
    :cond_0
    cmpl-double v6, p1, p3

    if-nez v6, :cond_1

    .line 585
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    .line 586
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    iput-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    .line 587
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 588
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    iput-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    .line 589
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    .line 590
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 603
    :goto_0
    return-object p0

    .line 595
    :cond_1
    mul-double v6, p1, p1

    mul-double v8, p3, p3

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 596
    .local v2, "d":D
    div-double v4, p3, v2

    .line 597
    .local v4, "sin":D
    div-double v0, p1, v2

    .line 598
    .local v0, "cos":D
    neg-double v6, v4

    invoke-virtual {p0, v6, v7, v0, v1}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 600
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->scale(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 602
    invoke-virtual {p0, v4, v5, v0, v1}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    goto :goto_0
.end method

.method public setToReflection(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 17
    .param p1, "x0"    # D
    .param p3, "y0"    # D
    .param p5, "x1"    # D
    .param p7, "y1"    # D

    .prologue
    .line 535
    cmpl-double v12, p1, p5

    if-nez v12, :cond_0

    cmpl-double v12, p3, p7

    if-nez v12, :cond_0

    .line 536
    new-instance v12, Ljava/lang/IllegalArgumentException;

    const-string v13, "Reflection line points must be distinct"

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 539
    :cond_0
    move-wide/from16 v0, p1

    neg-double v12, v0

    move-wide/from16 v0, p3

    neg-double v14, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14, v15}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToTranslation(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 542
    sub-double v6, p5, p1

    .line 543
    .local v6, "dx":D
    sub-double v8, p7, p3

    .line 544
    .local v8, "dy":D
    mul-double v12, v6, v6

    mul-double v14, v8, v8

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    .line 545
    .local v4, "d":D
    div-double v10, v8, v4

    .line 546
    .local v10, "sin":D
    div-double v2, v6, v4

    .line 547
    .local v2, "cos":D
    neg-double v12, v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v2, v3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 549
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v14, -0x4010000000000000L    # -1.0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14, v15}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->scale(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 551
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11, v2, v3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 553
    invoke-virtual/range {p0 .. p4}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->translate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 554
    return-object p0
.end method

.method public setToReflectionBasic(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 23
    .param p1, "x0"    # D
    .param p3, "y0"    # D
    .param p5, "x1"    # D
    .param p7, "y1"    # D

    .prologue
    .line 508
    cmpl-double v18, p1, p5

    if-nez v18, :cond_0

    cmpl-double v18, p3, p7

    if-nez v18, :cond_0

    .line 509
    new-instance v18, Ljava/lang/IllegalArgumentException;

    const-string v19, "Reflection line points must be distinct"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 511
    :cond_0
    sub-double v12, p5, p1

    .line 512
    .local v12, "dx":D
    sub-double v14, p7, p3

    .line 513
    .local v14, "dy":D
    mul-double v18, v12, v12

    mul-double v20, v14, v14

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    .line 514
    .local v10, "d":D
    div-double v16, v14, v10

    .line 515
    .local v16, "sin":D
    div-double v6, v12, v10

    .line 516
    .local v6, "cos":D
    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    mul-double v18, v18, v16

    mul-double v8, v18, v6

    .line 517
    .local v8, "cs2":D
    mul-double v18, v6, v6

    mul-double v20, v16, v16

    sub-double v4, v18, v20

    .line 518
    .local v4, "c2s2":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 519
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    neg-double v0, v4

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 520
    return-object p0
.end method

.method public setToRotation(D)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 5
    .param p1, "theta"    # D

    .prologue
    .line 624
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToRotation(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 625
    return-object p0
.end method

.method public setToRotation(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 5
    .param p1, "sinTheta"    # D
    .param p3, "cosTheta"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 645
    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    neg-double v0, p1

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 646
    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 647
    return-object p0
.end method

.method public setToRotation(DDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 11
    .param p1, "theta"    # D
    .param p3, "x"    # D
    .param p5, "y"    # D

    .prologue
    .line 671
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    move-object v1, p0

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-virtual/range {v1 .. v9}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->setToRotation(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 672
    return-object p0
.end method

.method public setToRotation(DDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 5
    .param p1, "sinTheta"    # D
    .param p3, "cosTheta"    # D
    .param p5, "x"    # D
    .param p7, "y"    # D

    .prologue
    .line 696
    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    neg-double v0, p1

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    mul-double v0, p5, p3

    sub-double v0, p5, v0

    mul-double v2, p7, p1

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 697
    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    mul-double v0, p5, p1

    sub-double v0, p7, v0

    mul-double v2, p7, p3

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 698
    return-object p0
.end method

.method public setToScale(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 3
    .param p1, "xScale"    # D
    .param p3, "yScale"    # D

    .prologue
    const-wide/16 v0, 0x0

    .line 717
    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 718
    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 719
    return-object p0
.end method

.method public setToShear(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 5
    .param p1, "xShear"    # D
    .param p3, "yShear"    # D

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 742
    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 743
    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 744
    return-object p0
.end method

.method public setToTranslation(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 5
    .param p1, "dx"    # D
    .param p3, "dy"    # D

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 762
    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 763
    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 764
    return-object p0
.end method

.method public setTransformation(DDDDDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "m00"    # D
    .param p3, "m01"    # D
    .param p5, "m02"    # D
    .param p7, "m10"    # D
    .param p9, "m11"    # D
    .param p11, "m12"    # D

    .prologue
    .line 383
    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    .line 384
    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    .line 385
    iput-wide p5, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 386
    iput-wide p7, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    .line 387
    iput-wide p9, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    .line 388
    iput-wide p11, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 389
    return-object p0
.end method

.method public setTransformation(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 2
    .param p1, "trans"    # Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .prologue
    .line 400
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    .line 401
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    .line 402
    return-object p0
.end method

.method public shear(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "xShear"    # D
    .param p3, "yShear"    # D

    .prologue
    .line 893
    invoke-static {p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->shearInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 894
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1096
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AffineTransformation[["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 10
    .param p1, "src"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "dest"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 986
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    add-double v0, v4, v6

    .line 987
    .local v0, "xp":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    add-double v2, v4, v6

    .line 988
    .local v2, "yp":D
    iput-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 989
    iput-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 990
    return-object p2
.end method

.method public transform(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1002
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 1003
    .local v0, "g2":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 1004
    return-object v0
.end method

.method public transform(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V
    .locals 12
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "i"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1016
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m00:D

    invoke-interface {p1, p2, v10}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v6

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m01:D

    invoke-interface {p1, p2, v11}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m02:D

    add-double v0, v4, v6

    .line 1017
    .local v0, "xp":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m10:D

    invoke-interface {p1, p2, v10}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v6

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m11:D

    invoke-interface {p1, p2, v11}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->m12:D

    add-double v2, v4, v6

    .line 1018
    .local v2, "yp":D
    invoke-interface {p1, p2, v10, v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->setOrdinate(IID)V

    .line 1019
    invoke-interface {p1, p2, v11, v2, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->setOrdinate(IID)V

    .line 1020
    return-void
.end method

.method public translate(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 908
    invoke-static {p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->translationInstance(DD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->compose(Lcom/vividsolutions/jts/geom/util/AffineTransformation;)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    .line 909
    return-object p0
.end method
