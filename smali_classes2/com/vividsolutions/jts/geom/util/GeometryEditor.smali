.class public Lcom/vividsolutions/jts/geom/util/GeometryEditor;
.super Ljava/lang/Object;
.source "GeometryEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateSequenceOperation;,
        Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;,
        Lcom/vividsolutions/jts/geom/util/GeometryEditor$NoOpGeometryOperation;,
        Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;
    }
.end annotation


# instance fields
.field private factory:Lcom/vividsolutions/jts/geom/GeometryFactory;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 1
    .param p1, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 108
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 109
    return-void
.end method

.method private editGeometryCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/GeometryCollection;
    .locals 7
    .param p1, "collection"    # Lcom/vividsolutions/jts/geom/GeometryCollection;
    .param p2, "operation"    # Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;

    .prologue
    const/4 v6, 0x0

    .line 184
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-interface {p2, p1, v4}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 188
    .local v0, "collectionForType":Lcom/vividsolutions/jts/geom/GeometryCollection;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v1, "geometries":Ljava/util/ArrayList;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 190
    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-virtual {p0, v4, p2}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 191
    .local v2, "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 189
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 194
    :cond_1
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 197
    .end local v2    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/vividsolutions/jts/geom/MultiPoint;

    if-ne v4, v5, :cond_3

    .line 198
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    new-array v4, v6, [Lcom/vividsolutions/jts/geom/Point;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/vividsolutions/jts/geom/Point;

    check-cast v4, [Lcom/vividsolutions/jts/geom/Point;

    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Point;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v4

    .line 209
    :goto_2
    return-object v4

    .line 201
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-ne v4, v5, :cond_4

    .line 202
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    new-array v4, v6, [Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/vividsolutions/jts/geom/LineString;

    check-cast v4, [Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v4

    goto :goto_2

    .line 205
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-ne v4, v5, :cond_5

    .line 206
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    new-array v4, v6, [Lcom/vividsolutions/jts/geom/Polygon;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/vividsolutions/jts/geom/Polygon;

    check-cast v4, [Lcom/vividsolutions/jts/geom/Polygon;

    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPolygon([Lcom/vividsolutions/jts/geom/Polygon;)Lcom/vividsolutions/jts/geom/MultiPolygon;

    move-result-object v4

    goto :goto_2

    .line 209
    :cond_5
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    new-array v4, v6, [Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/vividsolutions/jts/geom/Geometry;

    check-cast v4, [Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v4

    goto :goto_2
.end method

.method private editPolygon(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Polygon;
    .locals 8
    .param p1, "polygon"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p2, "operation"    # Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;

    .prologue
    const/4 v6, 0x0

    .line 152
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-interface {p2, p1, v5}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Polygon;

    .line 154
    .local v3, "newPolygon":Lcom/vividsolutions/jts/geom/Polygon;
    if-nez v3, :cond_0

    .line 155
    iget-object v7, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object v5, v6

    check-cast v5, Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-virtual {v7, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v3

    .line 156
    :cond_0
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Polygon;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 176
    .end local v3    # "newPolygon":Lcom/vividsolutions/jts/geom/Polygon;
    :goto_0
    return-object v3

    .line 161
    .restart local v3    # "newPolygon":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_1
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v5

    invoke-virtual {p0, v5, p2}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 162
    .local v4, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/LinearRing;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 164
    :cond_2
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v5, v6, v6}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v3

    goto :goto_0

    .line 167
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v1, "holes":Ljava/util/ArrayList;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v5

    if-ge v2, v5, :cond_6

    .line 169
    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v5

    invoke-virtual {p0, v5, p2}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 170
    .local v0, "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LinearRing;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 168
    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 173
    :cond_5
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 176
    .end local v0    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_6
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/vividsolutions/jts/geom/LinearRing;

    check-cast v5, [Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v6, v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method public edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "operation"    # Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;

    .prologue
    const/4 v0, 0x0

    .line 123
    if-nez p1, :cond_0

    .line 147
    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_0
    return-object v0

    .line 126
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    if-nez v1, :cond_1

    .line 127
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    iput-object v1, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 129
    :cond_1
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v1, :cond_2

    .line 130
    check-cast p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->editGeometryCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v0

    goto :goto_0

    .line 134
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v1, :cond_3

    .line 135
    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->editPolygon(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v0

    goto :goto_0

    .line 138
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v1, :cond_4

    .line 139
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-interface {p2, p1, v0}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0

    .line 142
    :cond_4
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v1, :cond_5

    .line 143
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-interface {p2, p1, v0}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0

    .line 146
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported Geometry class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere(Ljava/lang/String;)V

    goto :goto_0
.end method
