.class public Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;
.super Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;
.source "SimpleMCSweepLineIntersector.java"


# instance fields
.field events:Ljava/util/List;

.field nOverlaps:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;-><init>()V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    .line 67
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/lang/Object;)V
    .locals 15
    .param p1, "edge"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "edgeSet"    # Ljava/lang/Object;

    .prologue
    .line 103
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getMonotoneChainEdge()Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

    move-result-object v13

    .line 104
    .local v13, "mce":Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;
    invoke-virtual {v13}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;->getStartIndexes()[I

    move-result-object v14

    .line 105
    .local v14, "startIndex":[I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    array-length v1, v14

    add-int/lit8 v1, v1, -0x1

    if-ge v12, v1, :cond_0

    .line 106
    new-instance v5, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;

    invoke-direct {v5, v13, v12}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;-><init>(Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;I)V

    .line 107
    .local v5, "mc":Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    invoke-virtual {v13, v12}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;->getMinX(I)D

    move-result-wide v2

    const/4 v4, 0x0

    move-object/from16 v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;-><init>(Ljava/lang/Object;DLcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Ljava/lang/Object;)V

    .line 108
    .local v0, "insertEvent":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    new-instance v6, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    invoke-virtual {v13, v12}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;->getMaxX(I)D

    move-result-wide v8

    move-object/from16 v7, p2

    move-object v10, v0

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;-><init>(Ljava/lang/Object;DLcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Ljava/lang/Object;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 111
    .end local v0    # "insertEvent":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    .end local v5    # "mc":Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;
    :cond_0
    return-void
.end method

.method private add(Ljava/util/List;)V
    .locals 3
    .param p1, "edges"    # Ljava/util/List;

    .prologue
    .line 87
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 88
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 90
    .local v0, "edge":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-direct {p0, v0, v0}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->add(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    .end local v0    # "edge":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method

.method private add(Ljava/util/List;Ljava/lang/Object;)V
    .locals 3
    .param p1, "edges"    # Ljava/util/List;
    .param p2, "edgeSet"    # Ljava/lang/Object;

    .prologue
    .line 95
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 97
    .local v0, "edge":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-direct {p0, v0, p2}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->add(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    .end local v0    # "edge":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method

.method private computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 3
    .param p1, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 132
    const/4 v2, 0x0

    iput v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->nOverlaps:I

    .line 133
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->prepareEvents()V

    .line 135
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 137
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    .line 138
    .local v0, "ev":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->isInsert()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->getDeleteEventIndex()I

    move-result v2

    invoke-direct {p0, v1, v2, v0, p1}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->processOverlaps(IILcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 135
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    .end local v0    # "ev":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    :cond_1
    return-void
.end method

.method private prepareEvents()V
    .locals 3

    .prologue
    .line 120
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 121
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 123
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    .line 124
    .local v0, "ev":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->isDelete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->getInsertEvent()Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->setDeleteEventIndex(I)V

    .line 121
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    .end local v0    # "ev":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    :cond_1
    return-void
.end method

.method private processOverlaps(IILcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "ev0"    # Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    .param p4, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 146
    invoke-virtual {p3}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->getObject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;

    .line 152
    .local v2, "mc0":Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_2

    .line 153
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    .line 154
    .local v0, "ev1":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->isInsert()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 155
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->getObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;

    .line 158
    .local v3, "mc1":Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;
    iget-object v4, p3, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->edgeSet:Ljava/lang/Object;

    if-eqz v4, :cond_0

    iget-object v4, p3, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->edgeSet:Ljava/lang/Object;

    iget-object v5, v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->edgeSet:Ljava/lang/Object;

    if-eq v4, v5, :cond_1

    .line 159
    :cond_0
    invoke-virtual {v2, v3, p4}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;->computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 160
    iget v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->nOverlaps:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->nOverlaps:I

    .line 152
    .end local v3    # "mc1":Lcom/vividsolutions/jts/geomgraph/index/MonotoneChain;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    .end local v0    # "ev1":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    :cond_2
    return-void
.end method


# virtual methods
.method public computeIntersections(Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;Z)V
    .locals 1
    .param p1, "edges"    # Ljava/util/List;
    .param p2, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    .param p3, "testAllSegments"    # Z

    .prologue
    .line 71
    if-eqz p3, :cond_0

    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->add(Ljava/util/List;Ljava/lang/Object;)V

    .line 75
    :goto_0
    invoke-direct {p0, p2}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 76
    return-void

    .line 74
    :cond_0
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->add(Ljava/util/List;)V

    goto :goto_0
.end method

.method public computeIntersections(Ljava/util/List;Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 0
    .param p1, "edges0"    # Ljava/util/List;
    .param p2, "edges1"    # Ljava/util/List;
    .param p3, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 80
    invoke-direct {p0, p1, p1}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->add(Ljava/util/List;Ljava/lang/Object;)V

    .line 81
    invoke-direct {p0, p2, p2}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->add(Ljava/util/List;Ljava/lang/Object;)V

    .line 82
    invoke-direct {p0, p3}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;->computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 83
    return-void
.end method
