.class public Lcom/vividsolutions/jts/noding/SimpleSegmentSetMutualIntersector;
.super Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;
.source "SimpleSegmentSetMutualIntersector.java"


# instance fields
.field private baseSegStrings:Ljava/util/Collection;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;-><init>()V

    .line 53
    return-void
.end method

.method private intersect(Lcom/vividsolutions/jts/noding/SegmentString;Lcom/vividsolutions/jts/noding/SegmentString;)V
    .locals 5
    .param p1, "ss0"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p2, "ss1"    # Lcom/vividsolutions/jts/noding/SegmentString;

    .prologue
    .line 74
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 75
    .local v2, "pts0":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p2}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 76
    .local v3, "pts1":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i0":I
    :goto_0
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_0

    .line 77
    const/4 v1, 0x0

    .local v1, "i1":I
    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_2

    .line 78
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/SimpleSegmentSetMutualIntersector;->segInt:Lcom/vividsolutions/jts/noding/SegmentIntersector;

    invoke-interface {v4, p1, v0, p2, v1}, Lcom/vividsolutions/jts/noding/SegmentIntersector;->processIntersections(Lcom/vividsolutions/jts/noding/SegmentString;ILcom/vividsolutions/jts/noding/SegmentString;I)V

    .line 79
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/SimpleSegmentSetMutualIntersector;->segInt:Lcom/vividsolutions/jts/noding/SegmentIntersector;

    invoke-interface {v4}, Lcom/vividsolutions/jts/noding/SegmentIntersector;->isDone()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 83
    .end local v1    # "i1":I
    :cond_0
    return-void

    .line 77
    .restart local v1    # "i1":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 76
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public process(Ljava/util/Collection;)V
    .locals 5
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 62
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/SimpleSegmentSetMutualIntersector;->baseSegStrings:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 63
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 64
    .local v0, "baseSS":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "j":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 66
    .local v3, "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-direct {p0, v0, v3}, Lcom/vividsolutions/jts/noding/SimpleSegmentSetMutualIntersector;->intersect(Lcom/vividsolutions/jts/noding/SegmentString;Lcom/vividsolutions/jts/noding/SegmentString;)V

    .line 67
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/SimpleSegmentSetMutualIntersector;->segInt:Lcom/vividsolutions/jts/noding/SegmentIntersector;

    invoke-interface {v4}, Lcom/vividsolutions/jts/noding/SegmentIntersector;->isDone()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 70
    .end local v0    # "baseSS":Lcom/vividsolutions/jts/noding/SegmentString;
    .end local v2    # "j":Ljava/util/Iterator;
    .end local v3    # "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    :cond_2
    return-void
.end method

.method public setBaseSegments(Ljava/util/Collection;)V
    .locals 0
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/SimpleSegmentSetMutualIntersector;->baseSegStrings:Ljava/util/Collection;

    .line 58
    return-void
.end method
