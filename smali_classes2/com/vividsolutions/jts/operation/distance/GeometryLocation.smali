.class public Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
.super Ljava/lang/Object;
.source "GeometryLocation.java"


# static fields
.field public static final INSIDE_AREA:I = -0x1


# instance fields
.field private component:Lcom/vividsolutions/jts/geom/Geometry;

.field private pt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private segIndex:I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "component"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "segIndex"    # I
    .param p3, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->component:Lcom/vividsolutions/jts/geom/Geometry;

    .line 61
    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 74
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->component:Lcom/vividsolutions/jts/geom/Geometry;

    .line 75
    iput p2, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->segIndex:I

    .line 76
    iput-object p3, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "component"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 87
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 88
    return-void
.end method


# virtual methods
.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getGeometryComponent()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->component:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method public getSegmentIndex()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->segIndex:I

    return v0
.end method

.method public isInsideArea()Z
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->segIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
