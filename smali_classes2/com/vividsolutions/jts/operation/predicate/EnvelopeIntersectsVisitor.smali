.class Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;
.super Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;
.source "RectangleIntersects.java"


# instance fields
.field private intersects:Z

.field private rectEnv:Lcom/vividsolutions/jts/geom/Envelope;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Envelope;)V
    .locals 1
    .param p1, "rectEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;-><init>()V

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->intersects:Z

    .line 145
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 146
    return-void
.end method


# virtual methods
.method public intersects()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->intersects:Z

    return v0
.end method

.method protected isDone()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 196
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->intersects:Z

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected visit(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 7
    .param p1, "element"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v6, 0x1

    .line 162
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 165
    .local v0, "elementEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 170
    iput-boolean v6, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->intersects:Z

    goto :goto_0

    .line 182
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_3

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v2

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v4

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_3

    .line 184
    iput-boolean v6, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->intersects:Z

    goto :goto_0

    .line 187
    :cond_3
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v2

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v2

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v4

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 189
    iput-boolean v6, p0, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->intersects:Z

    goto :goto_0
.end method
