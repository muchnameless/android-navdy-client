.class public Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;
.super Ljava/lang/Object;
.source "DirectedEdgeStar.java"


# instance fields
.field protected outEdges:Ljava/util/List;

.field private sorted:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->sorted:Z

    .line 60
    return-void
.end method

.method private sortEdges()V
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->sorted:Z

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->sorted:Z

    .line 116
    :cond_0
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 1
    .param p1, "de"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->sorted:Z

    .line 68
    return-void
.end method

.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 96
    .local v1, "it":Ljava/util/Iterator;
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 98
    :goto_0
    return-object v2

    .line 97
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 98
    .local v0, "e":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    goto :goto_0
.end method

.method public getDegree()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEdges()Ljava/util/List;
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->sortEdges()V

    .line 107
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    return-object v0
.end method

.method public getIndex(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 153
    iget-object v1, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int v0, p1, v1

    .line 155
    .local v0, "modi":I
    if-gez v0, :cond_0

    iget-object v1, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_0
    return v0
.end method

.method public getIndex(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)I
    .locals 3
    .param p1, "dirEdge"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->sortEdges()V

    .line 138
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 139
    iget-object v2, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 140
    .local v0, "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    if-ne v0, p1, :cond_0

    .line 143
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 138
    .restart local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getIndex(Lcom/vividsolutions/jts/planargraph/Edge;)I
    .locals 3
    .param p1, "edge"    # Lcom/vividsolutions/jts/planargraph/Edge;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->sortEdges()V

    .line 124
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 125
    iget-object v2, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 126
    .local v0, "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 129
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 124
    .restart local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getNextCWEdge(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .locals 3
    .param p1, "dirEdge"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 177
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getIndex(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)I

    move-result v0

    .line 178
    .local v0, "i":I
    iget-object v1, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getIndex(I)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    return-object v1
.end method

.method public getNextEdge(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .locals 3
    .param p1, "dirEdge"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 166
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getIndex(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)I

    move-result v0

    .line 167
    .local v0, "i":I
    iget-object v1, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getIndex(I)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    return-object v1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->sortEdges()V

    .line 82
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 1
    .param p1, "de"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->outEdges:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 75
    return-void
.end method
