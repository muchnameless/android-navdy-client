.class public Lcom/vividsolutions/jts/planargraph/NodeMap;
.super Ljava/lang/Object;
.source "NodeMap.java"


# instance fields
.field private nodeMap:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/NodeMap;->nodeMap:Ljava/util/Map;

    .line 59
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/Node;
    .locals 2
    .param p1, "n"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-object p1
.end method

.method public find(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;
    .locals 1
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/Node;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/Node;

    return-object v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
