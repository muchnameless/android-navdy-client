.class public Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;
.super Ljava/lang/Object;
.source "GeometryPrecisionReducer.java"


# instance fields
.field private changePrecisionModel:Z

.field private isPointwise:Z

.field private removeCollapsed:Z

.field private targetPM:Lcom/vividsolutions/jts/geom/PrecisionModel;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V
    .locals 2
    .param p1, "pm"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->removeCollapsed:Z

    .line 85
    iput-boolean v1, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->changePrecisionModel:Z

    .line 86
    iput-boolean v1, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->isPointwise:Z

    .line 90
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->targetPM:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 91
    return-void
.end method

.method private changePM(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "newPM"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 210
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->createEditor(Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/util/GeometryEditor;

    move-result-object v0

    .line 212
    .local v0, "geomEditor":Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    new-instance v1, Lcom/vividsolutions/jts/geom/util/GeometryEditor$NoOpGeometryOperation;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$NoOpGeometryOperation;-><init>()V

    invoke-virtual {v0, p1, v1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private createEditor(Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    .locals 3
    .param p1, "geomFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;
    .param p2, "newPM"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 218
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v2

    if-ne v2, p2, :cond_0

    .line 219
    new-instance v0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;-><init>()V

    .line 223
    :goto_0
    return-object v0

    .line 221
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->createFactory(Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    .line 222
    .local v1, "newFactory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    new-instance v0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 223
    .local v0, "geomEdit":Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    goto :goto_0
.end method

.method private createFactory(Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/GeometryFactory;
    .locals 3
    .param p1, "inputFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;
    .param p2, "pm"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 228
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getSRID()I

    move-result v1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getCoordinateSequenceFactory()Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    move-result-object v2

    invoke-direct {v0, p2, v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;ILcom/vividsolutions/jts/geom/CoordinateSequenceFactory;)V

    .line 232
    .local v0, "newFactory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    return-object v0
.end method

.method private fixPolygonalTopology(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 185
    move-object v2, p1

    .line 186
    .local v2, "geomToBuffer":Lcom/vividsolutions/jts/geom/Geometry;
    iget-boolean v3, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->changePrecisionModel:Z

    if-nez v3, :cond_0

    .line 187
    iget-object v3, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->targetPM:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-direct {p0, p1, v3}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->changePM(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 190
    :cond_0
    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/vividsolutions/jts/geom/Geometry;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 192
    .local v0, "bufGeom":Lcom/vividsolutions/jts/geom/Geometry;
    move-object v1, v0

    .line 193
    .local v1, "finalGeom":Lcom/vividsolutions/jts/geom/Geometry;
    iget-boolean v3, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->changePrecisionModel:Z

    if-nez v3, :cond_1

    .line 195
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 197
    :cond_1
    return-object v1
.end method

.method public static reduce(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "precModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 61
    new-instance v0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    .line 62
    .local v0, "reducer":Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->reduce(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private reducePointwise(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 157
    iget-boolean v4, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->changePrecisionModel:Z

    if-eqz v4, :cond_1

    .line 158
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->targetPM:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-direct {p0, v4, v5}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->createFactory(Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v2

    .line 159
    .local v2, "newFactory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    new-instance v1, Lcom/vividsolutions/jts/geom/util/GeometryEditor;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 169
    .end local v2    # "newFactory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    .local v1, "geomEdit":Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    :goto_0
    iget-boolean v0, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->removeCollapsed:Z

    .line 170
    .local v0, "finalRemoveCollapsed":Z
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_0

    .line 171
    const/4 v0, 0x1

    .line 173
    :cond_0
    new-instance v4, Lcom/vividsolutions/jts/precision/PrecisionReducerCoordinateOperation;

    iget-object v5, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->targetPM:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-direct {v4, v5, v0}, Lcom/vividsolutions/jts/precision/PrecisionReducerCoordinateOperation;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;Z)V

    invoke-virtual {v1, p1, v4}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    .line 176
    .local v3, "reduceGeom":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v3

    .line 163
    .end local v0    # "finalRemoveCollapsed":Z
    .end local v1    # "geomEdit":Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    .end local v3    # "reduceGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    new-instance v1, Lcom/vividsolutions/jts/geom/util/GeometryEditor;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;-><init>()V

    .restart local v1    # "geomEdit":Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    goto :goto_0
.end method

.method public static reducePointwise(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "precModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 78
    new-instance v0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    .line 79
    .local v0, "reducer":Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->setPointwise(Z)V

    .line 80
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->reduce(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public reduce(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->reducePointwise(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 139
    .local v0, "reducePW":Lcom/vividsolutions/jts/geom/Geometry;
    iget-boolean v1, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->isPointwise:Z

    if-eqz v1, :cond_1

    .line 151
    .end local v0    # "reducePW":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_0
    return-object v0

    .line 143
    .restart local v0    # "reducePW":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v1, v0, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v1, :cond_0

    .line 147
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 151
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->fixPolygonalTopology(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method

.method public setChangePrecisionModel(Z)V
    .locals 0
    .param p1, "changePrecisionModel"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->changePrecisionModel:Z

    .line 119
    return-void
.end method

.method public setPointwise(Z)V
    .locals 0
    .param p1, "isPointwise"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->isPointwise:Z

    .line 134
    return-void
.end method

.method public setRemoveCollapsedComponents(Z)V
    .locals 0
    .param p1, "removeCollapsed"    # Z

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/vividsolutions/jts/precision/GeometryPrecisionReducer;->removeCollapsed:Z

    .line 104
    return-void
.end method
