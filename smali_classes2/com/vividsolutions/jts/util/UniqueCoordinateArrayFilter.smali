.class public Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;
.super Ljava/lang/Object;
.source "UniqueCoordinateArrayFilter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateFilter;


# instance fields
.field list:Ljava/util/ArrayList;

.field treeSet:Ljava/util/TreeSet;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;->treeSet:Ljava/util/TreeSet;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;->list:Ljava/util/ArrayList;

    .line 53
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;->treeSet:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;->list:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;->treeSet:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_0
    return-void
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 61
    iget-object v1, p0, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;->list:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v0, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 62
    .local v0, "coordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;->list:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    check-cast v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v1
.end method
