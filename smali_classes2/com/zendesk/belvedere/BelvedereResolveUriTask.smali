.class Lcom/zendesk/belvedere/BelvedereResolveUriTask;
.super Landroid/os/AsyncTask;
.source "BelvedereResolveUriTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/belvedere/BelvedereResult;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "BelvedereResolveUriTask"


# instance fields
.field final belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

.field final callback:Lcom/zendesk/belvedere/BelvedereCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/belvedere/BelvedereCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;>;"
        }
    .end annotation
.end field

.field final context:Landroid/content/Context;

.field final log:Lcom/zendesk/belvedere/BelvedereLogger;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereLogger;Lcom/zendesk/belvedere/BelvedereStorage;Lcom/zendesk/belvedere/BelvedereCallback;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "belvedereLogger"    # Lcom/zendesk/belvedere/BelvedereLogger;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "belvedereStorage"    # Lcom/zendesk/belvedere/BelvedereStorage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/zendesk/belvedere/BelvedereCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/zendesk/belvedere/BelvedereLogger;",
            "Lcom/zendesk/belvedere/BelvedereStorage;",
            "Lcom/zendesk/belvedere/BelvedereCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p4, "callback":Lcom/zendesk/belvedere/BelvedereCallback;, "Lcom/zendesk/belvedere/BelvedereCallback<Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->context:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    .line 44
    iput-object p3, p0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    .line 45
    iput-object p4, p0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->callback:Lcom/zendesk/belvedere/BelvedereCallback;

    .line 46
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->doInBackground([Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/net/Uri;)Ljava/util/List;
    .locals 19
    .param p1, "uris"    # [Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v8, "success":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;"
    move-object/from16 v0, p1

    array-length v12, v0

    const/4 v10, 0x0

    move v11, v10

    :goto_0
    if-ge v11, v12, :cond_d

    aget-object v9, p1, v11

    .line 54
    .local v9, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 55
    .local v6, "inputStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 58
    .local v4, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v6

    .line 59
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->context:Landroid/content/Context;

    invoke-virtual {v10, v13, v9}, Lcom/zendesk/belvedere/BelvedereStorage;->getTempFileForGalleryImage(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    .line 61
    .local v3, "file":Ljava/io/File;
    if-eqz v6, :cond_0

    if-nez v3, :cond_5

    .line 62
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v14, "BelvedereResolveUriTask"

    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v16, "Unable to resolve uri. InputStream null = %s, File null = %s"

    const/4 v10, 0x2

    new-array v0, v10, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    if-nez v6, :cond_3

    const/4 v10, 0x1

    .line 67
    :goto_1
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v17, v18

    const/16 v18, 0x1

    if-nez v3, :cond_4

    const/4 v10, 0x1

    :goto_2
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v17, v18

    .line 64
    invoke-static/range {v15 .. v17}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 62
    invoke-interface {v13, v14, v10}, Lcom/zendesk/belvedere/BelvedereLogger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    if-eqz v6, :cond_1

    .line 94
    :try_start_1
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 100
    :cond_1
    :goto_3
    if-eqz v4, :cond_2

    .line 101
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 52
    .end local v3    # "file":Ljava/io/File;
    :cond_2
    :goto_4
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_0

    .line 62
    .restart local v3    # "file":Ljava/io/File;
    :cond_3
    const/4 v10, 0x0

    goto :goto_1

    .line 67
    :cond_4
    const/4 v10, 0x0

    goto :goto_2

    .line 96
    :catch_0
    move-exception v2

    .line 97
    .local v2, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    const-string v14, "Error closing InputStream"

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 103
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 104
    .restart local v2    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    const-string v14, "Error closing FileOutputStream"

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 73
    .end local v2    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "Copying media file into private cache - Uri: %s - Dest: %s"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v9, v16, v17

    const/16 v17, 0x1

    aput-object v3, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v13, v14}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 77
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v5, "fileOutputStream":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    :try_start_4
    new-array v1, v10, [B

    .line 79
    .local v1, "buf":[B
    :goto_5
    invoke-virtual {v6, v1}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .local v7, "len":I
    if-lez v7, :cond_7

    .line 80
    const/4 v10, 0x0

    invoke-virtual {v5, v1, v10, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_c
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 85
    .end local v1    # "buf":[B
    .end local v7    # "len":I
    :catch_2
    move-exception v2

    move-object v4, v5

    .line 86
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v2, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :goto_6
    :try_start_5
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "File not found error copying file, uri: %s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v9, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 93
    if-eqz v6, :cond_6

    .line 94
    :try_start_6
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 100
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :cond_6
    :goto_7
    if-eqz v4, :cond_2

    .line 101
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_4

    .line 103
    :catch_3
    move-exception v2

    .line 104
    .local v2, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    const-string v14, "Error closing FileOutputStream"

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 83
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "buf":[B
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "len":I
    :cond_7
    :try_start_8
    new-instance v10, Lcom/zendesk/belvedere/BelvedereResult;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->context:Landroid/content/Context;

    invoke-virtual {v13, v14, v3}, Lcom/zendesk/belvedere/BelvedereStorage;->getFileProviderUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v13

    invoke-direct {v10, v3, v13}, Lcom/zendesk/belvedere/BelvedereResult;-><init>(Ljava/io/File;Landroid/net/Uri;)V

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_c
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 93
    if-eqz v6, :cond_8

    .line 94
    :try_start_9
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 100
    :cond_8
    :goto_8
    if-eqz v5, :cond_9

    .line 101
    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    :cond_9
    move-object v4, v5

    .line 105
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .line 96
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v2

    .line 97
    .restart local v2    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    const-string v14, "Error closing InputStream"

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    .line 103
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 104
    .restart local v2    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    const-string v14, "Error closing FileOutputStream"

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v5

    .line 106
    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .line 96
    .end local v1    # "buf":[B
    .end local v3    # "file":Ljava/io/File;
    .end local v7    # "len":I
    .local v2, "e":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v2

    .line 97
    .local v2, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    const-string v14, "Error closing InputStream"

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 88
    .end local v2    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v2

    .line 89
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_9
    :try_start_b
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "IO Error copying file, uri: %s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v9, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 93
    if-eqz v6, :cond_a

    .line 94
    :try_start_c
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 100
    :cond_a
    :goto_a
    if-eqz v4, :cond_2

    .line 101
    :try_start_d
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    goto/16 :goto_4

    .line 103
    :catch_8
    move-exception v2

    .line 104
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    const-string v14, "Error closing FileOutputStream"

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 96
    :catch_9
    move-exception v2

    .line 97
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v13, "BelvedereResolveUriTask"

    const-string v14, "Error closing InputStream"

    invoke-interface {v10, v13, v14, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_a

    .line 92
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 93
    :goto_b
    if-eqz v6, :cond_b

    .line 94
    :try_start_e
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    .line 100
    :cond_b
    :goto_c
    if-eqz v4, :cond_c

    .line 101
    :try_start_f
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    .line 105
    :cond_c
    :goto_d
    throw v10

    .line 96
    :catch_a
    move-exception v2

    .line 97
    .restart local v2    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v12, "BelvedereResolveUriTask"

    const-string v13, "Error closing InputStream"

    invoke-interface {v11, v12, v13, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_c

    .line 103
    .end local v2    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v2

    .line 104
    .restart local v2    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v12, "BelvedereResolveUriTask"

    const-string v13, "Error closing FileOutputStream"

    invoke-interface {v11, v12, v13, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_d

    .line 108
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_d
    return-object v8

    .line 92
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v9    # "uri":Landroid/net/Uri;
    :catchall_1
    move-exception v10

    move-object v4, v5

    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_b

    .line 88
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_c
    move-exception v2

    move-object v4, v5

    .end local v5    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_9

    .line 85
    .end local v3    # "file":Ljava/io/File;
    :catch_d
    move-exception v2

    goto/16 :goto_6
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "resolvedUris":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;"
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 114
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->callback:Lcom/zendesk/belvedere/BelvedereCallback;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->callback:Lcom/zendesk/belvedere/BelvedereCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/belvedere/BelvedereCallback;->internalSuccess(Ljava/lang/Object;)V

    .line 117
    :cond_0
    return-void
.end method
