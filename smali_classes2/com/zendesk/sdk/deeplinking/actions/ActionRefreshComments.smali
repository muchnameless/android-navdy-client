.class public Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments;
.super Lcom/zendesk/sdk/deeplinking/actions/Action;
.source "ActionRefreshComments.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;,
        Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/deeplinking/actions/Action",
        "<",
        "Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsListener;",
        "Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 12
    sget-object v0, Lcom/zendesk/sdk/deeplinking/actions/ActionType;->RELOAD_COMMENT_STREAM:Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    new-instance v1, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;

    invoke-direct {v1, p1}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/zendesk/sdk/deeplinking/actions/Action;-><init>(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)V

    .line 13
    return-void
.end method


# virtual methods
.method public bridge synthetic canHandleData(Lcom/zendesk/sdk/deeplinking/actions/ActionData;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments;->canHandleData(Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;)Z

    move-result v0

    return v0
.end method

.method public canHandleData(Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;)Z
    .locals 2
    .param p1, "data"    # Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments;->getActionData()Lcom/zendesk/sdk/deeplinking/actions/ActionData;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;

    invoke-virtual {v0}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;->getRequestId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;->getRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic execute(Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsListener;

    check-cast p2, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;

    invoke-virtual {p0, p1, p2}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments;->execute(Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsListener;Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;)V

    return-void
.end method

.method public execute(Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsListener;Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;)V
    .locals 0
    .param p1, "refreshCommentsAction"    # Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsListener;
    .param p2, "data"    # Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;

    .prologue
    .line 17
    invoke-interface {p1, p2}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsListener;->refreshComments(Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;)V

    .line 18
    return-void
.end method
