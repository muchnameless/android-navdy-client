.class public Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;
.super Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;
.source "DeepLinkingTargetRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget",
        "<",
        "Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static EXTRA_REQUEST_ID:Ljava/lang/String;

.field private static EXTRA_REQUEST_SUBJECT:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "extra_request_id"

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->EXTRA_REQUEST_ID:Ljava/lang/String;

    .line 20
    const-string v0, "extra_request_subject"

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->EXTRA_REQUEST_SUBJECT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;-><init>()V

    return-void
.end method


# virtual methods
.method extractConfiguration(Landroid/os/Bundle;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "targetConfiguration"    # Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;

    .prologue
    .line 49
    sget-object v2, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->EXTRA_REQUEST_ID:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 50
    const/4 v2, 0x0

    .line 56
    :goto_0
    return-object v2

    .line 53
    :cond_0
    sget-object v2, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->EXTRA_REQUEST_ID:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "requestId":Ljava/lang/String;
    sget-object v2, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->EXTRA_REQUEST_SUBJECT:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "subject":Ljava/lang/String;
    new-instance v2, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getFallbackActivity()Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method bridge synthetic extractConfiguration(Landroid/os/Bundle;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->extractConfiguration(Landroid/os/Bundle;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;

    move-result-object v0

    return-object v0
.end method

.method getExtra(Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;)Landroid/os/Bundle;
    .locals 3
    .param p1, "configuration"    # Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->EXTRA_REQUEST_ID:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->getRequestId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->getSubject()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    sget-object v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->EXTRA_REQUEST_SUBJECT:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->getSubject()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_0
    return-object v0
.end method

.method bridge synthetic getExtra(Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Landroid/os/Bundle;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 17
    check-cast p1, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->getExtra(Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method openTargetActivity(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "configuration"    # Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    sget-object v2, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->INSTANCE:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->getRequestId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->refreshComments(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    :goto_0
    return-void

    .line 32
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/zendesk/sdk/requests/ViewRequestActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "requestId"

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->getRequestId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->getSubject()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36
    const-string v2, "subject"

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    :cond_1
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->getTaskStackBuilder(Landroid/content/Context;Ljava/util/List;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v1

    .line 41
    .local v1, "taskStackBuilder":Landroid/support/v4/app/TaskStackBuilder;
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/zendesk/sdk/requests/RequestActivity;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntentWithParentStack(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    .line 42
    invoke-virtual {v1, v0}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntentWithParentStack(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    .line 44
    invoke-virtual {v1}, Landroid/support/v4/app/TaskStackBuilder;->startActivities()V

    goto :goto_0
.end method

.method bridge synthetic openTargetActivity(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)V
    .locals 0
    .param p2    # Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 17
    check-cast p2, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;

    invoke-virtual {p0, p1, p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;->openTargetActivity(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;)V

    return-void
.end method
