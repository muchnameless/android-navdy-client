.class public Lcom/zendesk/sdk/model/request/CreateRequest;
.super Ljava/lang/Object;
.source "CreateRequest.java"


# static fields
.field private static final transient METADATA_SDK_KEY:Ljava/lang/String; = "sdk"


# instance fields
.field private comment:Lcom/zendesk/sdk/model/request/Comment;

.field private customFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;"
        }
    .end annotation
.end field

.field private email:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private subject:Ljava/lang/String;

.field private tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ticketFormId:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->comment:Lcom/zendesk/sdk/model/request/Comment;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->comment:Lcom/zendesk/sdk/model/request/Comment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/request/Comment;->getBody()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public getTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->tags:Ljava/util/List;

    return-object v0
.end method

.method public setAttachments(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "attachments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->comment:Lcom/zendesk/sdk/model/request/Comment;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/zendesk/sdk/model/request/Comment;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/request/Comment;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->comment:Lcom/zendesk/sdk/model/request/Comment;

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->comment:Lcom/zendesk/sdk/model/request/Comment;

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/model/request/Comment;->setAttachments(Ljava/util/List;)V

    .line 104
    return-void
.end method

.method public setCustomFields(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "customFields":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/CustomField;>;"
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->customFields:Ljava/util/List;

    .line 152
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->comment:Lcom/zendesk/sdk/model/request/Comment;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lcom/zendesk/sdk/model/request/Comment;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/request/Comment;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->comment:Lcom/zendesk/sdk/model/request/Comment;

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->comment:Lcom/zendesk/sdk/model/request/Comment;

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/model/request/Comment;->setBody(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->email:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->id:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setMetadata(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "metadata":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->metadata:Ljava/util/Map;

    .line 162
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->metadata:Ljava/util/Map;

    const-string v1, "sdk"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->subject:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setTags(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->tags:Ljava/util/List;

    .line 69
    return-void
.end method

.method public setTicketFormId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "ticketFormId"    # Ljava/lang/Long;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CreateRequest;->ticketFormId:Ljava/lang/Long;

    .line 176
    return-void
.end method
