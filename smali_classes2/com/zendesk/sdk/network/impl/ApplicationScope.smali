.class public Lcom/zendesk/sdk/network/impl/ApplicationScope;
.super Ljava/lang/Object;
.source "ApplicationScope.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ApplicationScope"


# instance fields
.field private final appId:Ljava/lang/String;

.field private final applicationContext:Landroid/content/Context;

.field private final coppaEnabled:Z

.field private final customFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;"
        }
    .end annotation
.end field

.field private final developmentMode:Z

.field private libraryModuleCache:Lcom/zendesk/sdk/util/ScopeCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/util/LibraryModule;",
            ">;"
        }
    .end annotation
.end field

.field private final locale:Ljava/util/Locale;

.field private final oAuthToken:Ljava/lang/String;

.field private restAdapterCache:Lcom/zendesk/sdk/util/ScopeCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/network/impl/RestAdapterModule;",
            ">;"
        }
    .end annotation
.end field

.field private final sdkOptions:Lcom/zendesk/sdk/network/SdkOptions;

.field private storageModuleCache:Lcom/zendesk/sdk/util/ScopeCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/storage/StorageModule;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketFormId:Ljava/lang/Long;

.field private final url:Ljava/lang/String;

.field private final userAgentHeader:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/zendesk/sdk/util/ScopeCache;

    invoke-direct {v0}, Lcom/zendesk/sdk/util/ScopeCache;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->libraryModuleCache:Lcom/zendesk/sdk/util/ScopeCache;

    .line 47
    new-instance v0, Lcom/zendesk/sdk/util/ScopeCache;

    invoke-direct {v0}, Lcom/zendesk/sdk/util/ScopeCache;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->restAdapterCache:Lcom/zendesk/sdk/util/ScopeCache;

    .line 48
    new-instance v0, Lcom/zendesk/sdk/util/ScopeCache;

    invoke-direct {v0}, Lcom/zendesk/sdk/util/ScopeCache;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->storageModuleCache:Lcom/zendesk/sdk/util/ScopeCache;

    .line 51
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$000(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->applicationContext:Landroid/content/Context;

    .line 52
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$100(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->url:Ljava/lang/String;

    .line 53
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$200(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->appId:Ljava/lang/String;

    .line 54
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$300(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->oAuthToken:Ljava/lang/String;

    .line 55
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$400(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->locale:Ljava/util/Locale;

    .line 56
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$500(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->coppaEnabled:Z

    .line 57
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$600(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->developmentMode:Z

    .line 58
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$700(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Lcom/zendesk/sdk/network/SdkOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->sdkOptions:Lcom/zendesk/sdk/network/SdkOptions;

    .line 59
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$800(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->ticketFormId:Ljava/lang/Long;

    .line 60
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$900(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->customFields:Ljava/util/List;

    .line 61
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->access$1000(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->userAgentHeader:Ljava/lang/String;

    .line 62
    return-void
.end method

.method synthetic constructor <init>(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;Lcom/zendesk/sdk/network/impl/ApplicationScope$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    .param p2, "x1"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;-><init>(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->coppaEnabled:Z

    return v0
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->applicationContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCustomFields()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->customFields:Ljava/util/List;

    invoke-static {v0}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLibraryModuleCache()Lcom/zendesk/sdk/util/ScopeCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/util/LibraryModule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->libraryModuleCache:Lcom/zendesk/sdk/util/ScopeCache;

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getOAuthToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->oAuthToken:Ljava/lang/String;

    return-object v0
.end method

.method public getRestAdapterCache()Lcom/zendesk/sdk/util/ScopeCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/network/impl/RestAdapterModule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->restAdapterCache:Lcom/zendesk/sdk/util/ScopeCache;

    return-object v0
.end method

.method public getSdkOptions()Lcom/zendesk/sdk/network/SdkOptions;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->sdkOptions:Lcom/zendesk/sdk/network/SdkOptions;

    return-object v0
.end method

.method public getStorageModuleCache()Lcom/zendesk/sdk/util/ScopeCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/storage/StorageModule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->storageModuleCache:Lcom/zendesk/sdk/util/ScopeCache;

    return-object v0
.end method

.method public getTicketFormId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->ticketFormId:Ljava/lang/Long;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->url:Ljava/lang/String;

    return-object v0
.end method

.method public getUserAgentHeader()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->userAgentHeader:Ljava/lang/String;

    return-object v0
.end method

.method public isCoppaEnabled()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->coppaEnabled:Z

    return v0
.end method

.method public isDevelopmentMode()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope;->developmentMode:Z

    return v0
.end method

.method public newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 198
    new-instance v0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;-><init>(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    return-object v0
.end method
