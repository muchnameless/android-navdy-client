.class public interface abstract Lcom/zendesk/sdk/network/impl/ProviderStore;
.super Ljava/lang/Object;
.source "ProviderStore.java"


# virtual methods
.method public abstract helpCenterProvider()Lcom/zendesk/sdk/network/HelpCenterProvider;
.end method

.method public abstract networkInfoProvider()Lcom/zendesk/sdk/network/NetworkInfoProvider;
.end method

.method public abstract pushRegistrationProvider()Lcom/zendesk/sdk/network/PushRegistrationProvider;
.end method

.method public abstract requestProvider()Lcom/zendesk/sdk/network/RequestProvider;
.end method

.method public abstract sdkSettingsProvider()Lcom/zendesk/sdk/network/SdkSettingsProvider;
.end method

.method public abstract uiSettingsHelper()Lcom/zendesk/sdk/network/SettingsHelper;
.end method

.method public abstract uploadProvider()Lcom/zendesk/sdk/network/UploadProvider;
.end method

.method public abstract userProvider()Lcom/zendesk/sdk/network/UserProvider;
.end method
