.class Lcom/zendesk/sdk/network/impl/ZendeskConfigInjector;
.super Ljava/lang/Object;
.source "ZendeskConfigInjector.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static injectStubZendeskConfigHelper(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 18
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    .line 19
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectStubProviderStore(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v1

    .line 20
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectStubStorageStore(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StubStorageStore;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;-><init>(Lcom/zendesk/sdk/network/impl/ProviderStore;Lcom/zendesk/sdk/storage/StorageStore;)V

    return-object v0
.end method

.method static injectZendeskConfigHelper(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 11
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    .line 12
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ProviderInjector;->injectProviderStore(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v1

    .line 13
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectStorageStore(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;-><init>(Lcom/zendesk/sdk/network/impl/ProviderStore;Lcom/zendesk/sdk/storage/StorageStore;)V

    return-object v0
.end method
