.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->submitRecordArticleView(Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

.field final synthetic val$articleId:Ljava/lang/Long;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$locale:Ljava/util/Locale;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;Ljava/util/Locale;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$articleId:Ljava/lang/Long;

    iput-object p5, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$locale:Ljava/util/Locale;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 7
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 473
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheckHelpCenterSettings(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    new-instance v4, Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;

    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    .line 476
    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$100(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/HelpCenterSessionCache;->getLastSearch()Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    .line 477
    invoke-static {v1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$100(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/HelpCenterSessionCache;->isUniqueSearchResultClick()Z

    move-result v1

    invoke-direct {v4, v0, v1}, Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;-><init>(Lcom/zendesk/sdk/model/helpcenter/LastSearch;Z)V

    .line 480
    .local v4, "recordArticleViewRequest":Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    move-result-object v0

    .line 481
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$articleId:Ljava/lang/Long;

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$locale:Ljava/util/Locale;

    new-instance v5, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16$1;

    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v5, p0, v6}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;Lcom/zendesk/service/ZendeskCallback;)V

    .line 480
    invoke-virtual/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->submitRecordArticleView(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;Lcom/zendesk/service/ZendeskCallback;)V

    .line 497
    .end local v4    # "recordArticleViewRequest":Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 470
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
