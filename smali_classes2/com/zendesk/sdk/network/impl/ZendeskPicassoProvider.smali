.class public Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;
.super Ljava/lang/Object;
.source "ZendeskPicassoProvider.java"


# static fields
.field private static final HTTPS_SCHEME:Ljava/lang/String; = "https"

.field private static final HTTP_SCHEME:Ljava/lang/String; = "http"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static singleton:Lcom/squareup/picasso/Picasso;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->singleton:Lcom/squareup/picasso/Picasso;

    if-nez v0, :cond_1

    .line 69
    const-class v1, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;

    monitor-enter v1

    .line 71
    :try_start_0
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->singleton:Lcom/squareup/picasso/Picasso;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/squareup/picasso/Picasso$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/picasso/Picasso$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x2

    .line 73
    invoke-static {v2}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/picasso/Picasso$Builder;->executor(Ljava/util/concurrent/ExecutorService;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object v0

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 74
    invoke-virtual {v0, v2}, Lcom/squareup/picasso/Picasso$Builder;->defaultBitmapConfig(Landroid/graphics/Bitmap$Config;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object v0

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider$1;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider$1;-><init>(Landroid/content/Context;)V

    .line 75
    invoke-virtual {v0, v2}, Lcom/squareup/picasso/Picasso$Builder;->downloader(Lcom/squareup/picasso/Downloader;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/squareup/picasso/Picasso$Builder;->build()Lcom/squareup/picasso/Picasso;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->singleton:Lcom/squareup/picasso/Picasso;

    .line 105
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :cond_1
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->singleton:Lcom/squareup/picasso/Picasso;

    return-object v0

    .line 105
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
