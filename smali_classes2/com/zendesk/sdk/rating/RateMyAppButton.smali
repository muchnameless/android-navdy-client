.class public interface abstract Lcom/zendesk/sdk/rating/RateMyAppButton;
.super Ljava/lang/Object;
.source "RateMyAppButton.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract getId()I
.end method

.method public abstract getLabel()Ljava/lang/String;
.end method

.method public abstract getOnClickListener()Landroid/view/View$OnClickListener;
.end method

.method public abstract getStyleAttributeId()I
.end method

.method public abstract shouldDismissDialog()Z
.end method
