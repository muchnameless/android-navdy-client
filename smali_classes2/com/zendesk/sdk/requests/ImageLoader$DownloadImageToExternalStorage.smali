.class Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;
.super Landroid/os/AsyncTask;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadImageToExternalStorage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/zendesk/sdk/requests/ImageLoader$TaskData;",
        "Ljava/lang/Void;",
        "Lcom/zendesk/sdk/requests/ImageLoader$Result",
        "<",
        "Landroid/net/Uri;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mBelvedere:Lcom/zendesk/belvedere/Belvedere;

.field private final mCallback:Lcom/zendesk/service/ZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/zendesk/sdk/requests/ImageLoader;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/belvedere/Belvedere;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ImageLoader;
    .param p3, "belvedere"    # Lcom/zendesk/belvedere/Belvedere;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/zendesk/belvedere/Belvedere;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Landroid/net/Uri;>;"
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 107
    iput-object p2, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->mCallback:Lcom/zendesk/service/ZendeskCallback;

    .line 108
    iput-object p3, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->mBelvedere:Lcom/zendesk/belvedere/Belvedere;

    .line 109
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/zendesk/sdk/requests/ImageLoader$TaskData;)Lcom/zendesk/sdk/requests/ImageLoader$Result;
    .locals 14
    .param p1, "params"    # [Lcom/zendesk/sdk/requests/ImageLoader$TaskData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/zendesk/sdk/requests/ImageLoader$TaskData;",
            ")",
            "Lcom/zendesk/sdk/requests/ImageLoader$Result",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    const/4 v8, 0x0

    aget-object v8, p1, v8

    invoke-static {v8}, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;->access$000(Lcom/zendesk/sdk/requests/ImageLoader$TaskData;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 115
    .local v3, "bitmap":Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    aget-object v8, p1, v8

    invoke-static {v8}, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;->access$100(Lcom/zendesk/sdk/requests/ImageLoader$TaskData;)Lcom/zendesk/sdk/model/request/Attachment;

    move-result-object v0

    .line 117
    .local v0, "attachment":Lcom/zendesk/sdk/model/request/Attachment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/request/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_0
    const-string v1, ""

    .line 121
    .local v1, "attachmentContentType":Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_3

    const/4 v2, 0x0

    .line 125
    .local v2, "attachmentId":Ljava/lang/Long;
    :goto_1
    const-string v8, "image/"

    invoke-virtual {v1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 127
    new-instance v8, Lcom/zendesk/sdk/requests/ImageLoader$Result;

    iget-object v9, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    new-instance v10, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v11, "attachment is not an image"

    invoke-direct {v10, v11}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v9, v10}, Lcom/zendesk/sdk/requests/ImageLoader$Result;-><init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ErrorResponse;)V

    .line 162
    :cond_1
    :goto_2
    return-object v8

    .line 119
    .end local v1    # "attachmentContentType":Ljava/lang/String;
    .end local v2    # "attachmentId":Ljava/lang/Long;
    :cond_2
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/request/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 123
    .restart local v1    # "attachmentContentType":Ljava/lang/String;
    :cond_3
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/request/Attachment;->getId()Ljava/lang/Long;

    move-result-object v2

    goto :goto_1

    .line 130
    .restart local v2    # "attachmentId":Ljava/lang/Long;
    :cond_4
    if-nez v2, :cond_5

    .line 131
    new-instance v8, Lcom/zendesk/sdk/requests/ImageLoader$Result;

    iget-object v9, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    new-instance v10, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v11, "attachment does not have an id"

    invoke-direct {v10, v11}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v9, v10}, Lcom/zendesk/sdk/requests/ImageLoader$Result;-><init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_2

    .line 134
    :cond_5
    iget-object v8, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->mBelvedere:Lcom/zendesk/belvedere/Belvedere;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%s-%s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v2, v11, v12

    const/4 v12, 0x1

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/request/Attachment;->getFileName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/zendesk/belvedere/Belvedere;->getFileRepresentation(Ljava/lang/String;)Lcom/zendesk/belvedere/BelvedereResult;

    move-result-object v5

    .line 136
    .local v5, "file":Lcom/zendesk/belvedere/BelvedereResult;
    if-nez v5, :cond_6

    .line 137
    new-instance v8, Lcom/zendesk/sdk/requests/ImageLoader$Result;

    iget-object v9, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    new-instance v10, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v11, "Error creating tmp file"

    invoke-direct {v10, v11}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v9, v10}, Lcom/zendesk/sdk/requests/ImageLoader$Result;-><init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_2

    .line 140
    :cond_6
    const/4 v6, 0x0

    .line 144
    .local v6, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-virtual {v5}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v7, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x2a

    invoke-virtual {v3, v8, v9, v7}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 147
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V

    .line 148
    new-instance v8, Lcom/zendesk/sdk/requests/ImageLoader$Result;

    iget-object v9, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-virtual {v5}, Lcom/zendesk/belvedere/BelvedereResult;->getUri()Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/zendesk/sdk/requests/ImageLoader$Result;-><init>(Lcom/zendesk/sdk/requests/ImageLoader;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 157
    if-eqz v7, :cond_1

    .line 159
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 160
    :catch_0
    move-exception v4

    .line 161
    .local v4, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/zendesk/sdk/requests/ImageLoader;->access$200()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Couldn\'t close fileoutputstream"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v10, v4, v11}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_2

    .line 150
    .end local v4    # "e":Ljava/io/IOException;
    .end local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v4

    .line 153
    .restart local v4    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_3
    new-instance v8, Lcom/zendesk/sdk/requests/ImageLoader$Result;

    iget-object v9, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    new-instance v10, Lcom/zendesk/service/ErrorResponseAdapter;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v9, v10}, Lcom/zendesk/sdk/requests/ImageLoader$Result;-><init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ErrorResponse;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 157
    if-eqz v6, :cond_1

    .line 159
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2

    .line 160
    :catch_2
    move-exception v4

    .line 161
    invoke-static {}, Lcom/zendesk/sdk/requests/ImageLoader;->access$200()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Couldn\'t close fileoutputstream"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v10, v4, v11}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 157
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_4
    if-eqz v6, :cond_7

    .line 159
    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 162
    :cond_7
    :goto_5
    throw v8

    .line 160
    :catch_3
    move-exception v4

    .line 161
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-static {}, Lcom/zendesk/sdk/requests/ImageLoader;->access$200()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Couldn\'t close fileoutputstream"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v10, v4, v11}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_5

    .line 157
    .end local v4    # "e":Ljava/io/IOException;
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 150
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v4

    move-object v6, v7

    .end local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 101
    check-cast p1, [Lcom/zendesk/sdk/requests/ImageLoader$TaskData;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->doInBackground([Lcom/zendesk/sdk/requests/ImageLoader$TaskData;)Lcom/zendesk/sdk/requests/ImageLoader$Result;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/zendesk/sdk/requests/ImageLoader$Result;)V
    .locals 2
    .param p1    # Lcom/zendesk/sdk/requests/ImageLoader$Result;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/requests/ImageLoader$Result",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "result":Lcom/zendesk/sdk/requests/ImageLoader$Result;, "Lcom/zendesk/sdk/requests/ImageLoader$Result<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->mCallback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/ImageLoader$Result;->isError()Z

    move-result v0

    if-nez v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->mCallback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/ImageLoader$Result;->getResult()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->mCallback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/requests/ImageLoader$Result;->getErrorResponse()Lcom/zendesk/service/ErrorResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 101
    check-cast p1, Lcom/zendesk/sdk/requests/ImageLoader$Result;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->onPostExecute(Lcom/zendesk/sdk/requests/ImageLoader$Result;)V

    return-void
.end method
