.class public Lcom/zendesk/sdk/requests/ViewRequestFragment;
.super Landroid/support/v4/app/Fragment;
.source "ViewRequestFragment.java"

# interfaces
.implements Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;
    }
.end annotation


# static fields
.field public static final EXTRA_REQUEST_ID:Ljava/lang/String; = "requestId"

.field public static final EXTRA_REQUEST_SUBJECT:Ljava/lang/String; = "subject"

.field private static final FOURTY_PERCENT_OPACITY:I = 0x66

.field private static final FULL_OPACTITY:I = 0xff

.field private static final TAG:Ljava/lang/String; = "ViewRequestFragment"


# instance fields
.field private mAttachmentButton:Landroid/widget/Button;

.field private mAttachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

.field private mAttachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

.field private mCommentListAdapter:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;

.field private mCommentListCallback:Lcom/zendesk/service/ZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CommentsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private mCommentListView:Landroid/widget/ListView;

.field private mEditText:Landroid/widget/EditText;

.field private mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

.field private mImageUploadProgressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

.field private mImagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/belvedere/BelvedereCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLoadImageCallback:Lcom/zendesk/service/ZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mRequestId:Ljava/lang/String;

.field private mRetryable:Lcom/zendesk/sdk/network/Retryable;

.field private mSendButton:Landroid/widget/Button;

.field private mSubject:Ljava/lang/String;

.field private mSubmissionListener:Lcom/zendesk/sdk/network/SubmissionListener;

.field private mSubmitCommentCallback:Lcom/zendesk/service/ZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/Comment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->checkSendButtonState()V

    return-void
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->sendComment()V

    return-void
.end method

.method static synthetic access$1000(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/network/SubmissionListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubmissionListener:Lcom/zendesk/sdk/network/SubmissionListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/zendesk/sdk/requests/ViewRequestFragment;Lcom/zendesk/sdk/model/request/Attachment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;
    .param p1, "x1"    # Lcom/zendesk/sdk/model/request/Attachment;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->loadImage(Lcom/zendesk/sdk/model/request/Attachment;)V

    return-void
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    return-object v0
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/requests/ViewRequestFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setLoadingVisibility(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRequestId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/zendesk/sdk/requests/ViewRequestFragment;Lcom/zendesk/sdk/model/request/CommentsResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;
    .param p1, "x1"    # Lcom/zendesk/sdk/model/request/CommentsResponse;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->renderComments(Lcom/zendesk/sdk/model/request/CommentsResponse;)V

    return-void
.end method

.method static synthetic access$700(Lcom/zendesk/sdk/requests/ViewRequestFragment;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;
    .param p1, "x1"    # Lcom/zendesk/service/ErrorResponse;
    .param p2, "x2"    # Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->handleError(Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;)V

    return-void
.end method

.method static synthetic access$800(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListAdapter:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->loadRequest()V

    return-void
.end method

.method private canGetAttachments()Z
    .locals 2

    .prologue
    .line 633
    sget-object v0, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/belvedere/Belvedere;->oneOrMoreSourceAvailable()Z

    move-result v0

    return v0
.end method

.method private checkSendButtonState()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 569
    iget-object v6, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v2, v4

    .line 571
    .local v2, "shouldShowSendButton":Z
    :goto_0
    sget-object v6, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v6}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v3

    .line 572
    .local v3, "storedSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    if-eqz v3, :cond_2

    invoke-static {v3}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->isAttachmentSupportEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->canGetAttachments()Z

    move-result v6

    if-eqz v6, :cond_2

    move v1, v4

    .line 574
    .local v1, "shouldShowAttachmentButton":Z
    :goto_1
    iget-object v6, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v6}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->isImageUploadCompleted()Z

    move-result v6

    if-eqz v6, :cond_3

    move v0, v4

    .line 576
    .local v0, "imagesUploaded":Z
    :goto_2
    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 579
    invoke-direct {p0, v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setAttachmentButtonVisibility(Z)V

    .line 580
    invoke-direct {p0, v4}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setSendButtonVisibility(Z)V

    .line 582
    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-virtual {v5, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 583
    iget-object v4, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 602
    :cond_0
    :goto_3
    return-void

    .end local v0    # "imagesUploaded":Z
    .end local v1    # "shouldShowAttachmentButton":Z
    .end local v2    # "shouldShowSendButton":Z
    .end local v3    # "storedSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    :cond_1
    move v2, v5

    .line 569
    goto :goto_0

    .restart local v2    # "shouldShowSendButton":Z
    .restart local v3    # "storedSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    :cond_2
    move v1, v5

    .line 572
    goto :goto_1

    .restart local v1    # "shouldShowAttachmentButton":Z
    :cond_3
    move v0, v5

    .line 574
    goto :goto_2

    .line 585
    .restart local v0    # "imagesUploaded":Z
    :cond_4
    if-eqz v2, :cond_5

    .line 587
    invoke-direct {p0, v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setAttachmentButtonVisibility(Z)V

    .line 588
    invoke-direct {p0, v4}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setSendButtonVisibility(Z)V

    .line 590
    iget-object v4, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 591
    iget-object v4, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/16 v5, 0x66

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_3

    .line 596
    :cond_5
    invoke-direct {p0, v5}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setSendButtonVisibility(Z)V

    .line 598
    if-eqz v1, :cond_0

    .line 599
    invoke-direct {p0, v4}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setAttachmentButtonVisibility(Z)V

    goto :goto_3
.end method

.method private handleError(Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;)V
    .locals 5
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;
    .param p2, "action"    # Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    .prologue
    .line 467
    const-string v2, "ViewRequestFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to complete action: Status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 469
    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getStatus()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 471
    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    .line 467
    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 473
    const/4 v0, 0x0

    .line 474
    .local v0, "message":Ljava/lang/String;
    const/4 v1, 0x0

    .line 476
    .local v1, "retryAction":Landroid/view/View$OnClickListener;
    sget-object v2, Lcom/zendesk/sdk/requests/ViewRequestFragment$13;->$SwitchMap$com$zendesk$sdk$requests$ViewRequestFragment$Action:[I

    invoke-virtual {p2}, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 504
    :goto_0
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    if-eqz v2, :cond_0

    .line 505
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    invoke-interface {v2, v0, v1}, Lcom/zendesk/sdk/network/Retryable;->onRetryAvailable(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 507
    :cond_0
    return-void

    .line 479
    :pswitch_0
    sget v2, Lcom/zendesk/sdk/R$string;->view_request_send_comment_error:I

    invoke-virtual {p0, v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 480
    new-instance v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$10;

    .end local v1    # "retryAction":Landroid/view/View$OnClickListener;
    invoke-direct {v1, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$10;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    .line 487
    .restart local v1    # "retryAction":Landroid/view/View$OnClickListener;
    goto :goto_0

    .line 490
    :pswitch_1
    sget v2, Lcom/zendesk/sdk/R$string;->view_request_load_comments_error:I

    invoke-virtual {p0, v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 491
    new-instance v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$11;

    .end local v1    # "retryAction":Landroid/view/View$OnClickListener;
    invoke-direct {v1, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$11;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    .line 498
    .restart local v1    # "retryAction":Landroid/view/View$OnClickListener;
    goto :goto_0

    .line 500
    :pswitch_2
    const/4 v1, 0x0

    goto :goto_0

    .line 476
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private loadImage(Lcom/zendesk/sdk/model/request/Attachment;)V
    .locals 4
    .param p1, "attachment"    # Lcom/zendesk/sdk/model/request/Attachment;

    .prologue
    .line 460
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setLoadingVisibility(Z)V

    .line 461
    sget-object v1, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;

    move-result-object v0

    .line 462
    .local v0, "belvedere":Lcom/zendesk/belvedere/Belvedere;
    sget-object v1, Lcom/zendesk/sdk/requests/ImageLoader;->INSTANCE:Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mLoadImageCallback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/zendesk/sdk/requests/ImageLoader;->loadAndShowImage(Lcom/zendesk/sdk/model/request/Attachment;Landroid/content/Context;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/belvedere/Belvedere;)V

    .line 463
    return-void
.end method

.method private loadRequest()V
    .locals 4

    .prologue
    .line 434
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubject:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setTitle(Ljava/lang/String;)V

    .line 435
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setLoadingVisibility(Z)V

    .line 446
    :try_start_0
    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/zendesk/sdk/network/impl/ProviderStore;->requestProvider()Lcom/zendesk/sdk/network/RequestProvider;

    move-result-object v1

    .line 447
    .local v1, "provider":Lcom/zendesk/sdk/network/RequestProvider;
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRequestId:Ljava/lang/String;

    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListCallback:Lcom/zendesk/service/ZendeskCallback;

    invoke-interface {v1, v2, v3}, Lcom/zendesk/sdk/network/RequestProvider;->getComments(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    .end local v1    # "provider":Lcom/zendesk/sdk/network/RequestProvider;
    :cond_0
    :goto_0
    return-void

    .line 449
    :catch_0
    move-exception v0

    .line 453
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 454
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/zendesk/sdk/requests/ViewRequestFragment;
    .locals 1
    .param p0, "requestId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-static {p0, v0, v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/network/SubmissionListener;)Lcom/zendesk/sdk/requests/ViewRequestFragment;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/zendesk/sdk/requests/ViewRequestFragment;
    .locals 1
    .param p0, "requestId"    # Ljava/lang/String;
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 123
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/network/SubmissionListener;)Lcom/zendesk/sdk/requests/ViewRequestFragment;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/network/SubmissionListener;)Lcom/zendesk/sdk/requests/ViewRequestFragment;
    .locals 3
    .param p0, "requestId"    # Ljava/lang/String;
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/zendesk/sdk/network/SubmissionListener;

    .prologue
    .line 138
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 139
    .local v0, "fragmentArgs":Landroid/os/Bundle;
    const-string v2, "requestId"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    const-string v2, "subject"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_0
    new-instance v1, Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-direct {v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;-><init>()V

    .line 146
    .local v1, "viewRequestFragment":Lcom/zendesk/sdk/requests/ViewRequestFragment;
    invoke-virtual {v1, v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setArguments(Landroid/os/Bundle;)V

    .line 147
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setRetainInstance(Z)V

    .line 148
    invoke-virtual {v1, p2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setSubmissionListener(Lcom/zendesk/sdk/network/SubmissionListener;)V

    .line 150
    return-object v1
.end method

.method private renderComments(Lcom/zendesk/sdk/model/request/CommentsResponse;)V
    .locals 11
    .param p1, "response"    # Lcom/zendesk/sdk/model/request/CommentsResponse;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 511
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/CommentsResponse;->getComments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 513
    .local v2, "commentWithUserList":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/requests/CommentWithUser;>;"
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/CommentsResponse;->getComments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/request/CommentResponse;

    .line 514
    .local v0, "comment":Lcom/zendesk/sdk/model/request/CommentResponse;
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/CommentsResponse;->getUsers()Ljava/util/List;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/zendesk/sdk/requests/CommentWithUser;->build(Lcom/zendesk/sdk/model/request/CommentResponse;Ljava/util/List;)Lcom/zendesk/sdk/requests/CommentWithUser;

    move-result-object v1

    .line 516
    .local v1, "commentWithUser":Lcom/zendesk/sdk/requests/CommentWithUser;
    if-eqz v1, :cond_0

    .line 517
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 519
    :cond_0
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "Comment is null %s, users list is empty %s"

    const/4 v4, 0x2

    new-array v10, v4, [Ljava/lang/Object;

    if-nez v0, :cond_1

    move v4, v5

    .line 522
    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v10, v6

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/CommentsResponse;->getUsers()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v10, v5

    .line 520
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 524
    .local v3, "warning":Ljava/lang/String;
    const-string v4, "ViewRequestFragment"

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v4, v3, v8}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .end local v3    # "warning":Ljava/lang/String;
    :cond_1
    move v4, v6

    .line 519
    goto :goto_1

    .line 528
    .end local v0    # "comment":Lcom/zendesk/sdk/model/request/CommentResponse;
    .end local v1    # "commentWithUser":Lcom/zendesk/sdk/requests/CommentWithUser;
    :cond_2
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 529
    new-instance v4, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    sget v6, Lcom/zendesk/sdk/R$layout;->row_agent_comment:I

    new-instance v7, Lcom/zendesk/sdk/requests/ViewRequestFragment$12;

    invoke-direct {v7, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$12;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    invoke-direct {v4, v5, v6, v2, v7}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V

    iput-object v4, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListAdapter:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;

    .line 535
    iget-object v4, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListAdapter:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 538
    :cond_3
    return-void
.end method

.method private sendComment()V
    .locals 4

    .prologue
    .line 551
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setLoadingVisibility(Z)V

    .line 553
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubmissionListener:Lcom/zendesk/sdk/network/SubmissionListener;

    if-eqz v2, :cond_0

    .line 554
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubmissionListener:Lcom/zendesk/sdk/network/SubmissionListener;

    invoke-interface {v2}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionStarted()V

    .line 557
    :cond_0
    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/zendesk/sdk/network/impl/ProviderStore;->requestProvider()Lcom/zendesk/sdk/network/RequestProvider;

    move-result-object v1

    .line 559
    .local v1, "provider":Lcom/zendesk/sdk/network/RequestProvider;
    new-instance v0, Lcom/zendesk/sdk/model/request/EndUserComment;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/request/EndUserComment;-><init>()V

    .line 560
    .local v0, "endUserComment":Lcom/zendesk/sdk/model/request/EndUserComment;
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/zendesk/sdk/model/request/EndUserComment;->setValue(Ljava/lang/String;)V

    .line 561
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v2}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->getUploadTokens()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/zendesk/sdk/model/request/EndUserComment;->setAttachments(Ljava/util/List;)V

    .line 563
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRequestId:Ljava/lang/String;

    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubmitCommentCallback:Lcom/zendesk/service/ZendeskCallback;

    invoke-interface {v1, v2, v0, v3}, Lcom/zendesk/sdk/network/RequestProvider;->addComment(Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V

    .line 565
    return-void
.end method

.method private setAttachmentButtonVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 613
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 614
    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 616
    :cond_0
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setEditTextMarginRight(Z)V

    .line 617
    return-void

    .line 614
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setEditTextMarginRight(Z)V
    .locals 3
    .param p1, "isButtonVisible"    # Z

    .prologue
    .line 620
    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 621
    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 623
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    if-eqz p1, :cond_1

    .line 624
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/zendesk/sdk/util/UiUtils;->dpToPixels(ILandroid/util/DisplayMetrics;)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 628
    :goto_0
    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 630
    .end local v0    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void

    .line 626
    .restart local v0    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$dimen;->view_request_horizontal_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_0
.end method

.method private setLoadingVisibility(Z)V
    .locals 4
    .param p1, "isVisible"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 647
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    if-eqz v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/Retryable;->onRetryUnavailable()V

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 652
    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_5

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 654
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 655
    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    if-nez p1, :cond_6

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 658
    :cond_2
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    if-eqz v0, :cond_3

    .line 659
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    if-nez p1, :cond_7

    :goto_2
    invoke-virtual {v0, v2}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentsDeletable(Z)V

    .line 663
    :cond_3
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    if-eqz p1, :cond_4

    .line 664
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 665
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 667
    :cond_4
    return-void

    .line 652
    :cond_5
    const/4 v0, 0x4

    goto :goto_0

    :cond_6
    move v0, v1

    .line 655
    goto :goto_1

    :cond_7
    move v2, v1

    .line 659
    goto :goto_2
.end method

.method private setSendButtonVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 606
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 607
    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 609
    :cond_0
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setEditTextMarginRight(Z)V

    .line 610
    return-void

    .line 607
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 541
    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 542
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 543
    .local v1, "activity":Landroid/app/Activity;
    instance-of v2, v1, Landroid/support/v7/app/AppCompatActivity;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 544
    check-cast v0, Landroid/support/v7/app/AppCompatActivity;

    .line 545
    .local v0, "aba":Landroid/support/v7/app/AppCompatActivity;
    invoke-virtual {v0, p1}, Landroid/support/v7/app/AppCompatActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 548
    .end local v0    # "aba":Landroid/support/v7/app/AppCompatActivity;
    .end local v1    # "activity":Landroid/app/Activity;
    :cond_0
    return-void
.end method

.method private setupCallbacks()V
    .locals 1

    .prologue
    .line 329
    new-instance v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListCallback:Lcom/zendesk/service/ZendeskCallback;

    .line 344
    new-instance v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$8;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubmitCommentCallback:Lcom/zendesk/service/ZendeskCallback;

    .line 398
    new-instance v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mLoadImageCallback:Lcom/zendesk/service/ZendeskCallback;

    .line 424
    return-void
.end method

.method private tearDownCallbacks()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 427
    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListCallback:Lcom/zendesk/service/ZendeskCallback;

    .line 428
    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubmitCommentCallback:Lcom/zendesk/service/ZendeskCallback;

    .line 429
    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mLoadImageCallback:Lcom/zendesk/service/ZendeskCallback;

    .line 430
    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;

    .line 431
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 295
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 296
    new-instance v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$6;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$6;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    iput-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;

    .line 307
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setLoadingVisibility(Z)V

    .line 309
    sget-object v1, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;

    move-result-object v0

    .line 310
    .local v0, "belvedere":Lcom/zendesk/belvedere/Belvedere;
    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/zendesk/belvedere/Belvedere;->getFilesFromActivityOnResult(IILandroid/content/Intent;Lcom/zendesk/belvedere/BelvedereCallback;)V

    .line 311
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 264
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 266
    new-instance v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$5;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$5;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    .line 273
    instance-of v0, p1, Lcom/zendesk/sdk/network/Retryable;

    if-eqz v0, :cond_0

    .line 274
    check-cast p1, Lcom/zendesk/sdk/network/Retryable;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    .line 276
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 216
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 218
    new-instance v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$4;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$4;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadProgressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    .line 234
    new-instance v0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadProgressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/zendesk/sdk/network/impl/ProviderStore;->uploadProvider()Lcom/zendesk/sdk/network/UploadProvider;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;-><init>(Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;Lcom/zendesk/sdk/network/UploadProvider;)V

    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .line 235
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 156
    sget v2, Lcom/zendesk/sdk/R$layout;->fragment_view_request:I

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 158
    .local v0, "fragmentLayout":Landroid/view/View;
    sget v2, Lcom/zendesk/sdk/R$id;->view_request_comment_edittext:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    .line 160
    sget v2, Lcom/zendesk/sdk/R$id;->view_request_comment_send_bth:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    .line 161
    sget v2, Lcom/zendesk/sdk/R$id;->view_request_comment_attachment_bth:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentButton:Landroid/widget/Button;

    .line 163
    sget v2, Lcom/zendesk/sdk/R$id;->view_request_fragment_progress:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 164
    sget v2, Lcom/zendesk/sdk/R$id;->view_request_comment_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListView:Landroid/widget/ListView;

    .line 165
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/zendesk/sdk/requests/ViewRequestFragment$1;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$1;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 173
    sget v2, Lcom/zendesk/sdk/R$id;->view_request_attachment_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iput-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    .line 174
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    sget v3, Lcom/zendesk/sdk/R$id;->view_request_comment_attachment_container:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setParent(Landroid/view/View;)V

    .line 175
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v2, v3}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setState(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)V

    .line 176
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    invoke-virtual {v2, v3}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentContainerListener(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;)V

    .line 179
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    new-instance v3, Lcom/zendesk/sdk/requests/ViewRequestFragment$2;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$2;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    invoke-direct {p0, v4}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setSendButtonVisibility(Z)V

    .line 186
    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setHasOptionsMenu(Z)V

    .line 188
    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v1

    .line 189
    .local v1, "storedSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->isAttachmentSupportEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->canGetAttachments()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentButton:Landroid/widget/Button;

    invoke-static {v2, v4}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 192
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-static {v2, v5}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 194
    invoke-direct {p0, v6}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setEditTextMarginRight(Z)V

    .line 196
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentButton:Landroid/widget/Button;

    new-instance v3, Lcom/zendesk/sdk/requests/ViewRequestFragment$3;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment$3;-><init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    :goto_0
    return-object v0

    .line 205
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentButton:Landroid/widget/Button;

    invoke-static {v2, v5}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 206
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSendButton:Landroid/widget/Button;

    invoke-static {v2, v5}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 208
    invoke-direct {p0, v4}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setEditTextMarginRight(Z)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 288
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 289
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->setImageUploadProgressListener(Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;)V

    .line 290
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mImageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->deleteAllAttachmentsBeforeShutdown()V

    .line 291
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 280
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 282
    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mAttachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    .line 283
    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    .line 284
    return-void
.end method

.method onNetworkAvailable()V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->checkSendButtonState()V

    .line 320
    return-void
.end method

.method onNetworkUnavailable()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 323
    invoke-direct {p0, v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setAttachmentButtonVisibility(Z)V

    .line 324
    invoke-direct {p0, v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setSendButtonVisibility(Z)V

    .line 325
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 257
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 258
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->tearDownCallbacks()V

    .line 259
    sget-object v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->INSTANCE:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    invoke-virtual {v0, p0}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->unregisterAction(Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;)V

    .line 260
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 239
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 240
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "requestId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRequestId:Ljava/lang/String;

    .line 241
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "subject"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubject:Ljava/lang/String;

    .line 243
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->setupCallbacks()V

    .line 247
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mCommentListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 249
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->loadRequest()V

    .line 252
    :cond_0
    sget-object v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->INSTANCE:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/zendesk/sdk/deeplinking/actions/Action;

    const/4 v2, 0x0

    new-instance v3, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments;

    iget-object v4, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mRequestId:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments;-><init>(Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinking;->registerAction(Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;[Lcom/zendesk/sdk/deeplinking/actions/Action;)V

    .line 253
    return-void
.end method

.method public refreshComments(Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;)V
    .locals 0
    .param p1, "data"    # Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;

    .prologue
    .line 671
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->loadRequest()V

    .line 672
    return-void
.end method

.method public setSubmissionListener(Lcom/zendesk/sdk/network/SubmissionListener;)V
    .locals 0
    .param p1, "submissionListener"    # Lcom/zendesk/sdk/network/SubmissionListener;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment;->mSubmissionListener:Lcom/zendesk/sdk/network/SubmissionListener;

    .line 316
    return-void
.end method
