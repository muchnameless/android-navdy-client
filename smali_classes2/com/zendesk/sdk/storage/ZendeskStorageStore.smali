.class Lcom/zendesk/sdk/storage/ZendeskStorageStore;
.super Ljava/lang/Object;
.source "ZendeskStorageStore.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/StorageStore;


# instance fields
.field private final helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

.field private final identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

.field private final requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

.field private final sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

.field private final sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/storage/SdkStorage;Lcom/zendesk/sdk/storage/IdentityStorage;Lcom/zendesk/sdk/storage/RequestStorage;Lcom/zendesk/sdk/storage/SdkSettingsStorage;Lcom/zendesk/sdk/storage/HelpCenterSessionCache;)V
    .locals 0
    .param p1, "sdkStorage"    # Lcom/zendesk/sdk/storage/SdkStorage;
    .param p2, "identityStorage"    # Lcom/zendesk/sdk/storage/IdentityStorage;
    .param p3, "requestStorage"    # Lcom/zendesk/sdk/storage/RequestStorage;
    .param p4, "sdkSettingsStorage"    # Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .param p5, "sessionCache"    # Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;

    .line 20
    iput-object p2, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    .line 21
    iput-object p3, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    .line 22
    iput-object p4, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    .line 23
    iput-object p5, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    .line 24
    return-void
.end method


# virtual methods
.method public helpCenterSessionCache()Lcom/zendesk/sdk/storage/HelpCenterSessionCache;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    return-object v0
.end method

.method public identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    return-object v0
.end method

.method public requestStorage()Lcom/zendesk/sdk/storage/RequestStorage;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    return-object v0
.end method

.method public sdkSettingsStorage()Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    return-object v0
.end method

.method public sdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;->sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;

    return-object v0
.end method
