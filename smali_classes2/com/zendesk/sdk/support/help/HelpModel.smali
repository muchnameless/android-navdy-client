.class Lcom/zendesk/sdk/support/help/HelpModel;
.super Ljava/lang/Object;
.source "HelpModel.java"

# interfaces
.implements Lcom/zendesk/sdk/support/help/HelpMvp$Model;


# instance fields
.field private provider:Lcom/zendesk/sdk/network/HelpCenterProvider;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/HelpCenterProvider;)V
    .locals 0
    .param p1, "provider"    # Lcom/zendesk/sdk/network/HelpCenterProvider;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpModel;->provider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    .line 26
    return-void
.end method


# virtual methods
.method public getArticles(Ljava/util/List;Ljava/util/List;[Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p3, "labelNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;[",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "categoryIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p2, "sectionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpModel;->provider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    new-instance v1, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    invoke-direct {v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;-><init>()V

    .line 32
    invoke-virtual {v1, p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->withCategoryIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 33
    invoke-virtual {v1, p2}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->withSectionIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 34
    invoke-virtual {v1, p3}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includeCategories()Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includeSections()Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->build()Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    move-result-object v1

    .line 30
    invoke-interface {v0, v1, p4}, Lcom/zendesk/sdk/network/HelpCenterProvider;->getHelp(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;Lcom/zendesk/service/ZendeskCallback;)V

    .line 39
    return-void
.end method

.method public getArticlesForSection(Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;[Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "section"    # Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    .param p2, "labelNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;",
            "[",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpModel;->provider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    new-instance v1, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    invoke-direct {v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;-><init>()V

    .line 45
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getId()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->withSectionIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 46
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getTotalArticlesCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->withArticlesPerSectionLimit(I)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 47
    invoke-virtual {v1, p2}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 48
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includeSections()Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->build()Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    move-result-object v1

    .line 43
    invoke-interface {v0, v1, p3}, Lcom/zendesk/sdk/network/HelpCenterProvider;->getHelp(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;Lcom/zendesk/service/ZendeskCallback;)V

    .line 51
    return-void
.end method
