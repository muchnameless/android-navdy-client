.class Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;
.super Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;
.source "HelpRecyclerViewAdapter.java"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ArticleViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .line 275
    invoke-direct {p0, p2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;-><init>(Landroid/view/View;)V

    .line 276
    check-cast p2, Landroid/widget/TextView;

    .end local p2    # "itemView":Landroid/view/View;
    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;->textView:Landroid/widget/TextView;

    .line 277
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;I)V
    .locals 3
    .param p1, "item"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .param p2, "position"    # I

    .prologue
    .line 282
    if-nez p1, :cond_0

    .line 283
    const-string v0, "HelpRecyclerViewAdapter"

    const-string v1, "Article item was null, cannot bind"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294
    :goto_0
    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;->textView:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;->textView:Landroid/widget/TextView;

    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder$1;

    invoke-direct {v1, p0, p1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder$1;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
