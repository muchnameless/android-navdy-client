.class public Lcom/zendesk/sdk/support/help/HelpSearchFragment;
.super Landroid/support/v4/app/Fragment;
.source "HelpSearchFragment.java"


# static fields
.field static final ARG_ADD_LIST_PADDING_BOTTOM:Ljava/lang/String; = "arg_add_list_padding_bottom"


# instance fields
.field private adapter:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

.field private query:Ljava/lang/String;

.field private recyclerView:Landroid/support/v7/widget/RecyclerView;

.field private searchArticles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 30
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->searchArticles:Ljava/util/List;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->query:Ljava/lang/String;

    return-void
.end method

.method public static newInstance(Z)Lcom/zendesk/sdk/support/help/HelpSearchFragment;
    .locals 3
    .param p0, "addEndPadding"    # Z

    .prologue
    .line 36
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 37
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "arg_add_list_padding_bottom"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 39
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpSearchFragment;

    invoke-direct {v1}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;-><init>()V

    .line 40
    .local v1, "fragment":Lcom/zendesk/sdk/support/help/HelpSearchFragment;
    invoke-virtual {v1, v0}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method

.method private setupRecyclerView()V
    .locals 5

    .prologue
    .line 90
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    .line 91
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 90
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 93
    new-instance v0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->searchArticles:Ljava/util/List;

    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->query:Ljava/lang/String;

    .line 94
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "arg_add_list_padding_bottom"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;-><init>(Ljava/util/List;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    .line 96
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 97
    return-void
.end method


# virtual methods
.method public clearResults()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->clearResults()V

    .line 87
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->setRetainInstance(Z)V

    .line 49
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 55
    sget v1, Lcom/zendesk/sdk/R$layout;->fragment_help:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 57
    .local v0, "view":Landroid/view/View;
    sget v1, Lcom/zendesk/sdk/R$id;->recyclerView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 59
    invoke-direct {p0}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->setupRecyclerView()V

    .line 61
    return-object v0
.end method

.method public updateResults(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .param p2, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "searchArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;"
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->searchArticles:Ljava/util/List;

    .line 72
    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->query:Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->update(Ljava/util/List;Ljava/lang/String;)V

    .line 78
    :cond_0
    return-void
.end method
