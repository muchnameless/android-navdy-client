.class public Lcom/zendesk/sdk/ui/ZendeskDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ZendeskDialog.java"

# interfaces
.implements Ljava/io/Serializable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/zendesk/sdk/ui/ZendeskDialog;->getTheme()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/zendesk/sdk/ui/ZendeskDialog;->setStyle(II)V

    .line 22
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 26
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStart()V

    .line 28
    invoke-virtual {p0}, Lcom/zendesk/sdk/ui/ZendeskDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 29
    .local v0, "dialog":Landroid/app/Dialog;
    const v1, 0x3f28f5c3    # 0.66f

    invoke-static {v0, v1}, Lcom/zendesk/sdk/util/UiUtils;->sizeDialogWidthForTablets(Landroid/app/Dialog;F)V

    .line 30
    return-void
.end method
