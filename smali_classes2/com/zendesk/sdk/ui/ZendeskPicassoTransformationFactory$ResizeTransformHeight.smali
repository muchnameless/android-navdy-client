.class Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;
.super Ljava/lang/Object;
.source "ZendeskPicassoTransformationFactory.java"

# interfaces
.implements Lcom/squareup/picasso/Transformation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResizeTransformHeight"
.end annotation


# instance fields
.field private final maxHeight:I

.field final synthetic this$0:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;I)V
    .locals 0
    .param p2, "maxHeight"    # I

    .prologue
    .line 83
    iput-object p1, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;->this$0:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput p2, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;->maxHeight:I

    .line 85
    return-void
.end method


# virtual methods
.method public key()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "max-height-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;->maxHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "source"    # Landroid/graphics/Bitmap;

    .prologue
    .line 92
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;->maxHeight:I

    if-le v5, v6, :cond_1

    .line 93
    iget v3, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;->maxHeight:I

    .line 94
    .local v3, "targetHeight":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-double v6, v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-double v8, v5

    div-double v0, v6, v8

    .line 95
    .local v0, "aspectRatio":D
    int-to-double v6, v3

    mul-double/2addr v6, v0

    double-to-int v4, v6

    .line 101
    .end local v0    # "aspectRatio":D
    .local v4, "targetWidth":I
    :goto_0
    const/4 v5, 0x0

    invoke-static {p1, v4, v3, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 103
    .local v2, "result":Landroid/graphics/Bitmap;
    if-eq v2, p1, :cond_0

    .line 104
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 107
    :cond_0
    return-object v2

    .line 97
    .end local v2    # "result":Landroid/graphics/Bitmap;
    .end local v3    # "targetHeight":I
    .end local v4    # "targetWidth":I
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 98
    .restart local v3    # "targetHeight":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .restart local v4    # "targetWidth":I
    goto :goto_0
.end method
