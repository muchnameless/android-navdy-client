.class public Lcom/zendesk/util/RegexUtils;
.super Ljava/lang/Object;
.source "RegexUtils.java"


# static fields
.field private static final QUANTIFIER_PATTERN:Ljava/util/regex/Pattern;

.field private static final QUANTIFIER_VALIDATION_REGEX:Ljava/lang/String; = "^\\{\\d{1,},?\\d*\\}"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "^\\{\\d{1,},?\\d*\\}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/zendesk/util/RegexUtils;->QUANTIFIER_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static escape(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "regex"    # Ljava/lang/String;

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    const-string v6, ""

    .line 54
    :goto_0
    return-object v6

    .line 34
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 37
    .local v2, "chars":[C
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v6, v2

    if-ge v4, v6, :cond_3

    .line 38
    aget-char v1, v2, v4

    .line 39
    .local v1, "ch":C
    const/16 v6, 0x7b

    if-ne v1, v6, :cond_2

    .line 40
    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 41
    .local v5, "unprocessed":Ljava/lang/String;
    invoke-static {v5}, Lcom/zendesk/util/RegexUtils;->validateQuantifierExpression(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 43
    .local v3, "expression":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 44
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    add-int/2addr v4, v6

    .line 37
    .end local v3    # "expression":Ljava/lang/String;
    .end local v5    # "unprocessed":Ljava/lang/String;
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 47
    .restart local v3    # "expression":Ljava/lang/String;
    .restart local v5    # "unprocessed":Ljava/lang/String;
    :cond_1
    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 50
    .end local v3    # "expression":Ljava/lang/String;
    .end local v5    # "unprocessed":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 54
    .end local v1    # "ch":C
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method static validateQuantifierExpression(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "regex"    # Ljava/lang/String;

    .prologue
    .line 69
    sget-object v1, Lcom/zendesk/util/RegexUtils;->QUANTIFIER_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 70
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
