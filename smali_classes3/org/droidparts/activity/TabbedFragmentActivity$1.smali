.class Lorg/droidparts/activity/TabbedFragmentActivity$1;
.super Ljava/lang/Object;
.source "TabbedFragmentActivity.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/activity/TabbedFragmentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/droidparts/activity/TabbedFragmentActivity;


# direct methods
.method constructor <init>(Lorg/droidparts/activity/TabbedFragmentActivity;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lorg/droidparts/activity/TabbedFragmentActivity$1;->this$0:Lorg/droidparts/activity/TabbedFragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 54
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 42
    iget-object v0, p0, Lorg/droidparts/activity/TabbedFragmentActivity$1;->this$0:Lorg/droidparts/activity/TabbedFragmentActivity;

    invoke-static {v0, p2}, Lorg/droidparts/activity/TabbedFragmentActivity;->access$000(Lorg/droidparts/activity/TabbedFragmentActivity;Landroid/app/FragmentTransaction;)V

    .line 43
    iget-object v0, p0, Lorg/droidparts/activity/TabbedFragmentActivity$1;->this$0:Lorg/droidparts/activity/TabbedFragmentActivity;

    iget-object v1, p0, Lorg/droidparts/activity/TabbedFragmentActivity$1;->this$0:Lorg/droidparts/activity/TabbedFragmentActivity;

    invoke-virtual {v1}, Lorg/droidparts/activity/TabbedFragmentActivity;->getCurrentTab()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/droidparts/activity/TabbedFragmentActivity;->onTabChanged(I)V

    .line 44
    return-void
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 49
    return-void
.end method
