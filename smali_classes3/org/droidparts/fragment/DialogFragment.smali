.class public Lorg/droidparts/fragment/DialogFragment;
.super Landroid/app/DialogFragment;
.source "DialogFragment.java"


# instance fields
.field private injected:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final isInjected()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lorg/droidparts/fragment/DialogFragment;->injected:Z

    return v0
.end method

.method public onCreateView(Landroid/os/Bundle;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 46
    invoke-super {p0, p2, p3, p1}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-virtual {p0, p3, p1, p2}, Lorg/droidparts/fragment/DialogFragment;->onCreateView(Landroid/os/Bundle;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 35
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 36
    invoke-static {v0, p0}, Lorg/droidparts/Injector;->inject(Landroid/view/View;Ljava/lang/Object;)V

    .line 37
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/droidparts/fragment/DialogFragment;->injected:Z

    .line 41
    :goto_0
    return-object v0

    .line 39
    :cond_0
    invoke-virtual {p0}, Lorg/droidparts/fragment/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-static {v1, p0}, Lorg/droidparts/Injector;->inject(Landroid/app/Dialog;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public show(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 54
    invoke-static {p1, p0}, Lorg/droidparts/inner/fragments/SecretFragmentsStockUtil;->dialogFragmentShowDialogFragment(Landroid/app/Activity;Landroid/app/DialogFragment;)V

    .line 56
    return-void
.end method
