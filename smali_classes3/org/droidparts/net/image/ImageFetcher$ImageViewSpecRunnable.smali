.class abstract Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;
.super Ljava/lang/Object;
.source "ImageFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/net/image/ImageFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "ImageViewSpecRunnable"
.end annotation


# instance fields
.field final spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

.field final submitted:J

.field final synthetic this$0:Lorg/droidparts/net/image/ImageFetcher;


# direct methods
.method public constructor <init>(Lorg/droidparts/net/image/ImageFetcher;Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;J)V
    .locals 1
    .param p2, "spec"    # Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    .param p3, "submitted"    # J

    .prologue
    .line 405
    iput-object p1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406
    iput-object p2, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;->spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    .line 407
    iput-wide p3, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;->submitted:J

    .line 408
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 412
    const/4 v0, 0x0

    .line 413
    .local v0, "eq":Z
    if-ne p0, p1, :cond_1

    .line 414
    const/4 v0, 0x1

    .line 418
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 415
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;

    if-eqz v1, :cond_0

    .line 416
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;->spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    check-cast p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v2, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;->spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    invoke-virtual {v1, v2}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;->spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    invoke-virtual {v0}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->hashCode()I

    move-result v0

    return v0
.end method
